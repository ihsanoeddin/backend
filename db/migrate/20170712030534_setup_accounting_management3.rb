class SetupAccountingManagement3 < ActiveRecord::Migration[5.0]
  def self.up
    add_column :main_accounting_management_balances, :store_id, :integer, index: { name: "main_accounting_management_balances_store_id" }
  end
  def self.down
    remove_column :main_accounting_management_balances, :store_id
  end
end
