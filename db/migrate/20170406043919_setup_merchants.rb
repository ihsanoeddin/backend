class SetupMerchants < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # create companies table
    #
    create_table :tenants_merchants_companies do |t|
      t.string :type
      t.string :domain, index: {name: 'companies_domain'}
      t.string :slug, unique: true
      t.string :name
      t.string :short_name
      t.string :logo
      t.string :business_registration_number
      t.string :gst_registration_number
      t.string :website
      t.string :email
      t.string :phone_number
      t.string :currency
      t.string :time_zone
      t.string :state
      t.string :owner_name
      t.references :business_field, index: { name: "companies_busines_field_id" }
      t.datetime :deleted_at, index: { name: "companies_deleted_at" }
      t.timestamps
    end

    #create business fields table
    create_table :tenants_merchants_business_fields do |t|
      t.string :name
      t.text :keywords
      t.datetime :deleted_at, index: { name: "tenants_business_fields_deleted_at" }
      t.timestamps
    end

    #
    # create device imei table
    #
    create_table :tenants_merchants_device_imei do |t|
      t.string :type
      t.string :number
      t.references :company
      t.string :state
      t.datetime :deleted_at, index: { name: "device_imei_deleted_at" }
      t.timestamps
    end

    # create table addresses
    create_table :tenants_addressable_addresses do |t|
      t.string :type
      t.string :name
      t.string :address
      t.string :city
      t.string :province
      t.string :country
      t.string :zip_code
      t.float :latitude
      t.float :longitude
      t.references :addressable, polymorphic: true, index: { name: 'addresses_addressable_id_and_addressable_type' }
      t.datetime :deleted_at, index: { name: "addresses_deleted_at" }
      t.timestamps
    end

  end

  def self.down
    remove_index :tenants_merchants_companies, "companies_domain" if index_exists?(:tenants_merchants_companies, "companies_domain")
    remove_index :tenants_merchants_companies, "companies_deleted_at" if index_exists?(:tenants_merchants_companies, "companies_domain")
    remove_index :tenants_merchants_device_imei, "device_imei_deleted_at" if index_exists?(:tenants_merchants_device_imei, "device_imei_deleted_at")
    remove_index :tenants_addressable_addresses, "addresses_deleted_at" if index_exists?(:tenants_addressable_addresses, "addresses_deleted_at")
    remove_index :tenants_addressable_addresses, "addresses_addressable_id_and_addressable_type" if index_exists?(:tenants_addressable_addresses, "addresses_addressable_id_and_addressable_type")
    drop_table :tenants_merchants_companies
    drop_table :tenants_merchants_business_fields
    drop_table :tenants_merchants_device_imei
    drop_table :tenants_addressable_addresses
  end

end
