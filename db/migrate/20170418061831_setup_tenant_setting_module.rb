class SetupTenantSettingModule < ActiveRecord::Migration[5.0]
  
  def self.up

    # create table tenant setting
    create_table :tenants_settingable_settings do |t|
      t.string :name
      t.references :settingable, polymorphic: true, index: { name: "tenants_settings_settingable_type_and_setingable_id" }
      t.text :options
      t.boolean :enable, default: true
      t.datetime :deleted_at, index: { name: "tenants_settings_deleted_at" }
      t.timestamps
    end

  end

  def self.down
    drop_table :tenants_settingable_settings
  end

end
