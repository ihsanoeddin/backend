class SetupImageable < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # create imageable_images table
    #
    create_table :supports_imageable_images do |t|
      t.string :type
      t.references :imageable, polymorphic: true, index: { name: "images_imageable_id_and_imageable_type" }
      t.string :attachment
      t.integer :position
      t.string :caption
      t.datetime :deleted_at, index: { name: "images_deleted_at" }
      t.timestamps
    end

    #
    # create imageable_image_groups table
    #
    create_table :supports_imageable_image_groups do |t|
      t.string :type
      t.string :name
      t.references :contextable, polymorphic: true, index: { name: "image_groups_contextable_id_and_contextable_type" }
      t.datetime :deleted_at, index: { name: "image_groups_deleted_at" }
      t.integer :images_count, default: 0
      t.integer :variants_count, default: 0
      t.timestamps
    end

  end

  def self.down
    
    remove_index :supports_imageable_images, "images_imageable_id_and_imageable_type" if index_exists?(:supports_imageable_images, "images_imageable_id_and_imageable_type")
    remove_index :supports_imageable_images, "image_groups_deleted_at" if index_exists?(:supports_imageable_images, "images_deleted_at")
    remove_index :supports_imageable_image_groups, "images_deleted_at" if index_exists?(:supports_imageable_image_groups, "image_groups_deleted_at")
    remove_index :supports_imageable_image_groups, "image_groups_contextable_id_and_contextable_type" if index_exists?(:supports_imageable_image_groups, "image_groups_contextable_id_and_contextable_type")
    drop_table :supports_imageable_image_groups
    drop_table :supports_imageable_images
  
  end

end
