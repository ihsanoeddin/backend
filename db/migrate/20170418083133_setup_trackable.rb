class SetupTrackable < ActiveRecord::Migration[5.0]
  
  def self.up

    # create table tenant setting
    create_table :supports_trackable_tracks do |t|
      t.references :trackable, polymorphic: true, index: { name: "supports_tracks_trackable_type_and_trackable_id" }
      t.references :editor, polymorphic: true, index: { name: "supports_tracks_editor_type_and_editor_id" }
      t.text :previous_record_attributes
      t.string :message
      t.string :tracked_state
      t.datetime :deleted_at, index: { name: "supports_trackable_tracks_deleted_at" }
      t.timestamps
    end

  end

  def self.down
    drop_table :supports_trackable_tracks
  end

end
