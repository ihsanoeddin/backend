class SetupUsersManagement < ActiveRecord::Migration[5.0]
  
  def self.up
      #
      # create users table
      #
     create_table :main_users_management_users do |t|
      ## Database authenticatable
      t.string :username
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Merchant tag
      t.boolean :merchant, default: false

      ## Lockable
      t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      t.string   :unlock_token # Only if unlock strategy is :email or :both
      t.datetime :locked_at

      t.datetime :deleted_at, index: true


      t.timestamps null: false
    end

    #add_index :main_users_management_users, :email,                unique: true
    add_index :main_users_management_users, :username,                unique: true
    add_index :main_users_management_users, :reset_password_token, unique: true
    add_index :main_users_management_users, :confirmation_token,   unique: true
    add_index :main_users_management_users, :unlock_token,         unique: true

    #
    # create roles table
    #
    create_table(:main_users_management_roles) do |t|
      t.string :name
      t.references :resource, :polymorphic => true, index: { name: 'resource_id_and_resource_type' }
      t.datetime :deleted_at, index: true
      t.timestamps
    end

    #
    # create usersroles table
    #
    create_table(:main_users_management_users_roles, :id => false) do |t|
      t.references :user, index: { name: 'users_roles_user_id' }
      t.references :role, index: { name: 'users_roles_role_id' }
    end

    #
    # create accesses table
    #
    create_table :main_users_management_accesses do |t|
      t.string :name
      t.string :key
      t.boolean :active, default: true
      t.references :parent, index: { name: 'accesses_parent_id' }
      t.string :type
      t.datetime :deleted_at, index: true
      t.timestamps
    end

    #
    # create rolesaccesses table
    #
    create_table :main_users_management_roles_accesses, id: false do |t|
      t.references :role, index: { name: 'roles_accesses_role_id' }
      t.references :access, index: { name: 'roles_accesses_access_id' }
    end

  end

  def self.down
    drop_table :main_users_management_users
    drop_table :main_users_management_roles
    drop_table :main_users_management_users_roles
    drop_table :main_users_management_accesses
    drop_table :main_users_management_roles_accesses
  end

end
