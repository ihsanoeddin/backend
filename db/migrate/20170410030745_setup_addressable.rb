class SetupAddressable < ActiveRecord::Migration[5.0]
  def self.up
    create_table :supports_addressable_addresses do |t|
      t.string :type
      t.string :name
      t.string :address
      t.string :city
      t.string :province
      t.string :country
      t.string :zip_code
      t.float :latitude
      t.float :longitude
      t.references :addressable, polymorphic: true, index: { name: 'addresses_addressable_id_and_addressable_type' }
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end

  def self.down
    drop_table :supports_addressable_addresses    
  end
end
