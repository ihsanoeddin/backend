class SetupReturnsManagement < ActiveRecord::Migration[5.0]
  
  def self.up
    #drop_table :main_returns_management_returns, if_exists: true
    #drop_table :main_returns_management_refunds, if_exists: true
    #drop_table :main_returns_management_exchanges, if_exists: true
    #drop_table :main_returns_management_voids, if_exists: true
    
    #
    # create returns table
    #
    #create_table :main_returns_management_returns do |t|
    #  t.references :store, index: { name: "main_returns_management_returns_store_id" }
    #  t.references :transaction, index: { name: "main_returns_management_returns_transaction_id" }
    #  t.string :state
    #  t.datetime :time
    #  t.datetime :deleted_at, index: true
    #  t.timestamps null: false
    #end

    create_table :main_returns_management_voids do |t|

      t.references :store, index: { name: "main_returns_management_voids_store_id" }
      t.references :transaction, index: { name: "main_returns_management_voids_transaction_id" }
      #t.references :return, index: { name: "main_returns_management_refunds_return_id" }
      #t.references :variant, index: { name: "main_returns_management_refunds_variant_id" }
      t.string :state
      #t.integer :quantity, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.text :notes
      t.datetime :time

      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end


    #
    # create refunds table
    #
    create_table :main_returns_management_refunds do |t|

      t.references :store, index: { name: "main_returns_management_refunds_store_id" }
      t.references :transaction, index: { name: "main_returns_management_refunds_transaction_id" }
      #t.references :return, index: { name: "main_returns_management_refunds_return_id" }
      t.references :variant, index: { name: "main_returns_management_refunds_variant_id" }
      t.string :state
      t.integer :quantity, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.text :notes
      t.datetime :time

      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #
    # create returns table
    #
    create_table :main_returns_management_returns do |t|

      t.references :store, index: { name: "main_returns_management_refunds_store_id" }
      t.references :transaction, index: { name: "main_returns_management_refunds_transaction_id" }
      #t.references :return, index: { name: "main_returns_management_refunds_return_id" }
      t.references :variant, index: { name: "main_returns_management_refunds_variant_id" }
      t.string :state
      t.integer :quantity, default: 0
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2

      t.references :exchange_variant, index: { name: "main_returns_management_refunds_exchange_variant_id" }
      t.integer :exchange_quantity, default: 0
      t.decimal :exchange_amount, :default => 0.0, :precision => 15, :scale => 2

      t.text :notes
      t.datetime :time

      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

  end

  def self.down
    drop_table :main_returns_management_returns, if_exists: true
    drop_table :main_returns_management_refunds, if_exists: true
    drop_table :main_returns_management_exchanges, if_exists: true
    drop_table :main_returns_management_voids, if_exists: true
  end

end
