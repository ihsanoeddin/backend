class SetupProcurementsManagement < ActiveRecord::Migration[5.0]
  
  def self.up
    #drop_table :main_procurements_management_suppliers, if_exists: true  
    #
    # create suppliers table
    #
    create_table :main_procurements_management_suppliers do |t|
      t.string :name
      t.string :email
      t.string :phone_number
      t.string :code
      t.string :mode
      t.string :state
      t.integer :max_purchase_orders_qty, default: 0
      t.integer :min_purchase_orders_qty, default: 0      
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end
    #drop_table :main_procurements_management_contacts, if_exists: true
    #
    # create table contacts
    #
    create_table :main_procurements_management_contacts do |t|
      t.references :supplier, index: { name: "main_procurements_management_contacts_supplier_id" }
      t.string :name
      t.string :email
      t.string :phone_number      
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

    #drop_table :main_procurements_management_suppliers_variants, if_exists: true
    #
    # create suppliers variants
    #
    create_table :main_procurements_management_suppliers_variants do |t|
      t.references :variant, index: { name: "main_purchase_orders_management_suppliers_variants_variant_id" }
      t.references :supplier, index: { name: "main_purchase_orders_management_suppliers_variants_supplier_id" }
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.datetime :deleted_at, index: { name: "procurements_man_suppliers_variants_deleted_at"}
      t.timestamps null: false
    end

    #
    # create orders table
    #
    #drop_table :main_procurements_management_purchase_orders, if_exists: true
    create_table :main_procurements_management_purchase_orders do |t|
      t.references :supplier, index: { name: "main_purchase_orders_management_purchase_orders_supplier_id" }
      t.references :store, index: { name: "main_purchase_orders_management_purchase_orders_store_id" }
      t.string :number
      t.date :order_date
      t.date :expected_delivery_date
      t.date :delivery_date
      t.datetime :completed_date
      t.text :shipping_address
      t.text :notes
      t.string :state
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.text :notified_with
      t.datetime :deleted_at, index: { name: "procurements_man_purchase_orders_deleted_at"}
      t.timestamps null: false
    end

    #
    # create order details table
    #
    #drop_table :main_procurements_management_purchase_order_details, if_exists: true
    create_table :main_procurements_management_purchase_order_details do |t|
      t.references :purchase_order, index: { name: "main_purchase_orders_management_purchase_details_order_id" }
      t.references :variant, index: { name: "main_purchase_orders_management_purchase_details_variant_id" }
      t.string :name
      t.string :number
      t.integer :quantity, default: 0
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :state
      t.datetime :deleted_at, index: { name: "procurements_man_purchase_orders_details_deleted_at"}
      t.timestamps null: false
    end

    #
    # create invoices table
    #
    #drop_table :main_procurements_management_invoices, if_exists: true
    create_table :main_procurements_management_invoices do |t|
      t.references :purchase_order, index: { name: "main_purchase_orders_management_invoices_purchase_order_id" }
      t.references :store, index: { name: "main_purchase_orders_management_invoices_store_id" }
      t.string :number
      t.string :supplier_delivery_order_number
      t.date :date
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :state
      t.float :tax_rate, default: 0
      t.float :discount_rate, default: 0 
      t.text :notes
      t.datetime :deleted_at, index: { name: "procurements_man_invoices_deleted_at"}
      t.timestamps null: false
    end

    #
    # create invoice details table
    #
    #drop_table :main_procurements_management_invoice_details, if_exists: true
    create_table :main_procurements_management_invoice_details do |t|
      t.references :invoice, index: { name: "main_purchase_orders_management_invoice_details_invoice_id" }
      t.references :variant, index: { name: "main_purchase_orders_management_invoice_details_variant_id" }
      t.integer :received_quantity, default: 0
      t.integer :expected_quantity, default: 0
      t.string :name
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :state
      t.float :tax_rate, default: 0
      t.float :discount_rate, default: 0 
      t.datetime :deleted_at, index: { name: "procurements_man_invoice_details_deleted_at"}
      t.timestamps null: false
    end

    #
    # create returns table
    #
    #drop_table :main_procurements_management_returns, if_exists: true
    create_table :main_procurements_management_returns do |t|
      t.references :invoice, index: { name: "main_purchase_orders_management_returns_invoice_id" }
      t.references :supplier, index: { name: "main_purchase_orders_management_returns_supplier_id" }
      t.references :variant, index: { name: "main_purchase_orders_management_returns_variant_id" }
      t.string :variant_name
      t.integer :quantity, default: 0
      t.string :name
      t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2

      t.references :exchange_variant, index: { name: "main_purchase_orders_management_returns_exchange_variant_id" } 
      t.string :exchange_variant_name
      t.integer :exchange_quantity
      t.string :exchange_name
      t.decimal :exchange_price, :default => 0.0, :precision => 15, :scale => 2
      t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
      t.string :state
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end

  end

  def self.down  
    [
      :main_procurements_management_suppliers,
      :main_procurements_management_returns,
      :main_procurements_management_contacts,
      :main_procurements_management_invoices,
      :main_procurements_management_invoice_details,
      :main_procurements_management_purchase_orders,
      :main_procurements_management_suppliers_variants,
      :main_procurements_management_purchase_order_details
    ].each{ |tabl|  drop_table tabl }
  end

end
