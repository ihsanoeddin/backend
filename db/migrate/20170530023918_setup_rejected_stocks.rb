class SetupRejectedStocks < ActiveRecord::Migration[5.0]
  def self.up
    add_column :main_inventory_management_stocks, :type, :string
    add_column :main_inventory_management_stock_adjustments, :stock_type, :string
  end

  def self.down
    remove_column :main_inventory_management_stocks, :type
    remove_column :main_inventory_management_stock_adjustments, :stock_type
  end
end
