class SetupAccountingManagement2 < ActiveRecord::Migration[5.0]
  def self.up
    #
    # add column group_id to accounts tab;e
    #
    add_column :main_accounting_management_accounts, :group_id, :integer, index: { name: "main_accounting_management_accounts_group_id" }
  end
  def self.down
    remove_index :main_accounting_management_accounts, name: "main_accounting_management_accounts_group_id"
    remove_column :main_accounting_management_accounts, :group_id
  end
end
