class SetupStores < ActiveRecord::Migration[5.0]
  
  def self.up
    #
    # create stores table
    #
    create_table :main_stores_management_stores do |t|
      t.string :type
      t.references :branch, index: { name: "stores_branch" }
      t.string :name
      t.string :short_name
      t.string :state
      t.datetime :deleted_at, index: true
      t.integer :transactions_count
      t.timestamps
    end

  end

  def self.down
    drop_table :main_stores_management_stores
  end

end
