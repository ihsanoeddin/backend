# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170710032404) do

  create_table "friendly_id_slugs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, length: { slug: 70, scope: 70 }, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", length: { slug: 140 }, using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "main_accounting_management_accounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "identifing_name"
    t.string   "code"
    t.boolean  "active",                        default: true
    t.boolean  "static",                        default: false
    t.text     "description",     limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "group_id"
    t.index ["deleted_at"], name: "index_main_accounting_management_accounts_on_deleted_at", using: :btree
    t.index ["identifing_name"], name: "index_main_accounting_management_accounts_on_identifing_name", using: :btree
  end

  create_table "main_accounting_management_balances", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "account_id"
    t.integer  "group_id"
    t.string   "reference_type"
    t.integer  "reference_id"
    t.decimal  "amount",                       precision: 15, scale: 2, default: "0.0"
    t.string   "type"
    t.text     "notes",          limit: 65535
    t.datetime "time"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.index ["account_id"], name: "main_accounting_management_balances_account_id", using: :btree
    t.index ["deleted_at"], name: "index_main_accounting_management_balances_on_deleted_at", using: :btree
    t.index ["group_id"], name: "main_accounting_management_balances_group_id", using: :btree
    t.index ["reference_type", "reference_id"], name: "main_accounting_management_balances_reference_type_and_id", using: :btree
  end

  create_table "main_accounting_management_journals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "identifing_name"
    t.text     "description",     limit: 65535
    t.boolean  "static",                        default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["deleted_at"], name: "index_main_accounting_management_journals_on_deleted_at", using: :btree
    t.index ["identifing_name"], name: "index_main_accounting_management_journals_on_identifing_name", using: :btree
  end

  create_table "main_accounting_management_journals_balances", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "journal_id"
    t.integer  "balance_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["balance_id"], name: "main_accounting_management_journals_balances_balance_id", using: :btree
    t.index ["deleted_at"], name: "index_main_accounting_management_journals_balances_on_deleted_at", using: :btree
    t.index ["journal_id"], name: "main_accounting_management_journals_balances_journal_id", using: :btree
  end

  create_table "main_inventory_management_stock_adjustments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "stock_id"
    t.string   "reference_type"
    t.integer  "reference_id"
    t.integer  "group_id"
    t.integer  "count",                        default: 0
    t.string   "on"
    t.string   "type"
    t.string   "state"
    t.text     "notes",          limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "stock_type"
    t.string   "direction",                    default: "in"
    t.index ["deleted_at"], name: "index_main_inventory_management_stock_adjustments_on_deleted_at", using: :btree
    t.index ["group_id"], name: "main_inventory_management_stock_adjustments_group", using: :btree
    t.index ["reference_type", "reference_id"], name: "main_inventory_management_stock_adjustments_reference", using: :btree
    t.index ["stock_id"], name: "main_inventory_management_stock_adjustments_stock_id", using: :btree
  end

  create_table "main_inventory_management_stocks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "variant_id"
    t.integer  "count_on_store",              default: 0
    t.integer  "count_pending_to_customer",   default: 0
    t.integer  "count_pending_from_supplier", default: 0
    t.datetime "deleted_at"
    t.string   "type"
    t.index ["deleted_at"], name: "index_main_inventory_management_stocks_on_deleted_at", using: :btree
    t.index ["store_id"], name: "main_inventory_management_stocks_store_id", using: :btree
    t.index ["variant_id"], name: "main_inventory_management_stocks_variant_id", using: :btree
  end

  create_table "main_procurements_management_contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "supplier_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["deleted_at"], name: "index_main_procurements_management_contacts_on_deleted_at", using: :btree
    t.index ["supplier_id"], name: "main_procurements_management_contacts_supplier_id", using: :btree
  end

  create_table "main_procurements_management_invoice_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "invoice_id"
    t.integer  "variant_id"
    t.integer  "received_quantity",                                     default: 0
    t.integer  "expected_quantity",                                     default: 0
    t.string   "name"
    t.decimal  "price",                        precision: 15, scale: 2, default: "0.0"
    t.decimal  "amount",                       precision: 15, scale: 2, default: "0.0"
    t.string   "state"
    t.float    "tax_rate",          limit: 24,                          default: 0.0
    t.float    "discount_rate",     limit: 24,                          default: 0.0
    t.datetime "deleted_at"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.index ["deleted_at"], name: "procurements_man_invoice_details_deleted_at", using: :btree
    t.index ["invoice_id"], name: "main_purchase_orders_management_invoice_details_invoice_id", using: :btree
    t.index ["variant_id"], name: "main_purchase_orders_management_invoice_details_variant_id", using: :btree
  end

  create_table "main_procurements_management_invoices", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "purchase_order_id"
    t.integer  "store_id"
    t.string   "number"
    t.string   "supplier_delivery_order_number"
    t.date     "date"
    t.decimal  "amount",                                       precision: 15, scale: 2, default: "0.0"
    t.string   "state"
    t.float    "tax_rate",                       limit: 24,                             default: 0.0
    t.float    "discount_rate",                  limit: 24,                             default: 0.0
    t.text     "notes",                          limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                            null: false
    t.datetime "updated_at",                                                                            null: false
    t.index ["deleted_at"], name: "procurements_man_invoices_deleted_at", using: :btree
    t.index ["purchase_order_id"], name: "main_purchase_orders_management_invoices_purchase_order_id", using: :btree
    t.index ["store_id"], name: "main_purchase_orders_management_invoices_store_id", using: :btree
  end

  create_table "main_procurements_management_purchase_order_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "purchase_order_id"
    t.integer  "variant_id"
    t.string   "name"
    t.string   "number"
    t.integer  "quantity",                                   default: 0
    t.decimal  "price",             precision: 15, scale: 2, default: "0.0"
    t.decimal  "amount",            precision: 15, scale: 2, default: "0.0"
    t.string   "state"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.index ["deleted_at"], name: "procurements_man_purchase_orders_details_deleted_at", using: :btree
    t.index ["purchase_order_id"], name: "main_purchase_orders_management_purchase_details_order_id", using: :btree
    t.index ["variant_id"], name: "main_purchase_orders_management_purchase_details_variant_id", using: :btree
  end

  create_table "main_procurements_management_purchase_orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "supplier_id"
    t.integer  "store_id"
    t.string   "number"
    t.date     "order_date"
    t.date     "expected_delivery_date"
    t.date     "delivery_date"
    t.datetime "completed_date"
    t.text     "shipping_address",       limit: 65535
    t.text     "notes",                  limit: 65535
    t.string   "state"
    t.decimal  "amount",                               precision: 15, scale: 2, default: "0.0"
    t.text     "notified_with",          limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                    null: false
    t.datetime "updated_at",                                                                    null: false
    t.index ["deleted_at"], name: "procurements_man_purchase_orders_deleted_at", using: :btree
    t.index ["store_id"], name: "main_purchase_orders_management_purchase_orders_store_id", using: :btree
    t.index ["supplier_id"], name: "main_purchase_orders_management_purchase_orders_supplier_id", using: :btree
  end

  create_table "main_procurements_management_returns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "invoice_id"
    t.integer  "supplier_id"
    t.integer  "variant_id"
    t.string   "variant_name"
    t.integer  "quantity",                                       default: 0
    t.string   "name"
    t.decimal  "price",                 precision: 15, scale: 2, default: "0.0"
    t.decimal  "amount",                precision: 15, scale: 2, default: "0.0"
    t.integer  "exchange_variant_id"
    t.string   "exchange_variant_name"
    t.integer  "exchange_quantity"
    t.string   "exchange_name"
    t.decimal  "exchange_price",        precision: 15, scale: 2, default: "0.0"
    t.string   "state"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.index ["deleted_at"], name: "index_main_procurements_management_returns_on_deleted_at", using: :btree
    t.index ["exchange_variant_id"], name: "main_purchase_orders_management_returns_exchange_variant_id", using: :btree
    t.index ["invoice_id"], name: "main_purchase_orders_management_returns_invoice_id", using: :btree
    t.index ["supplier_id"], name: "main_purchase_orders_management_returns_supplier_id", using: :btree
    t.index ["variant_id"], name: "main_purchase_orders_management_returns_variant_id", using: :btree
  end

  create_table "main_procurements_management_suppliers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone_number"
    t.string   "code"
    t.string   "mode"
    t.string   "state"
    t.integer  "max_purchase_orders_qty", default: 0
    t.integer  "min_purchase_orders_qty", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["deleted_at"], name: "index_main_procurements_management_suppliers_on_deleted_at", using: :btree
  end

  create_table "main_procurements_management_suppliers_variants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "variant_id"
    t.integer  "supplier_id"
    t.decimal  "price",       precision: 15, scale: 2, default: "0.0"
    t.datetime "deleted_at"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["deleted_at"], name: "procurements_man_suppliers_variants_deleted_at", using: :btree
    t.index ["supplier_id"], name: "main_purchase_orders_management_suppliers_variants_supplier_id", using: :btree
    t.index ["variant_id"], name: "main_purchase_orders_management_suppliers_variants_variant_id", using: :btree
  end

  create_table "main_products_management_entity_properties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "entity_type"
    t.integer  "entity_id"
    t.integer  "property_id"
    t.integer  "position"
    t.float    "value",       limit: 24, default: 0.0
    t.string   "unit_value"
    t.datetime "deleted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["deleted_at"], name: "products_management_entity_properties_deleted_at", using: :btree
    t.index ["entity_type", "entity_id"], name: "products_management_entity_type_and_entity_id", using: :btree
    t.index ["property_id"], name: "products_management_entity_property_id", using: :btree
  end

  create_table "main_products_management_product_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "permalink"
    t.text     "description",    limit: 65535
    t.datetime "deleted_at"
    t.integer  "products_count",               default: 0
    t.integer  "integer",                      default: 0
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["deleted_at"], name: "products_management_products_type_deleted_at", using: :btree
  end

  create_table "main_products_management_products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "state"
    t.string   "brand"
    t.string   "type"
    t.text     "keywords",                limit: 65535
    t.string   "permalink"
    t.decimal  "cost",                                  precision: 15, scale: 2, default: "0.0"
    t.decimal  "price",                                 precision: 15, scale: 2, default: "0.0"
    t.string   "short_description"
    t.text     "description",             limit: 65535
    t.boolean  "featured",                                                       default: false
    t.integer  "product_type_id"
    t.integer  "tax_rate_id"
    t.datetime "deleted_at"
    t.integer  "variants_count",                                                 default: 0
    t.integer  "images_count",                                                   default: 0
    t.integer  "entity_properties_count",                                        default: 1
    t.integer  "integer",                                                        default: 1
    t.string   "barcode"
    t.datetime "created_at",                                                                     null: false
    t.datetime "updated_at",                                                                     null: false
    t.integer  "business_model_id"
    t.index ["deleted_at"], name: "products_management_products_deleted_at", using: :btree
    t.index ["product_type_id"], name: "products_management_products_product_type_id", using: :btree
    t.index ["tax_rate_id"], name: "products_management_products_tax_rate_id", using: :btree
  end

  create_table "main_products_management_properties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "identifing_name"
    t.string   "display_name"
    t.text     "units",                   limit: 65535
    t.boolean  "active",                                default: true
    t.string   "description"
    t.datetime "deleted_at"
    t.integer  "entity_properties_count",               default: 1
    t.integer  "integer",                               default: 1
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["deleted_at"], name: "products_management_properties_deleted_at", using: :btree
  end

  create_table "main_products_management_tax_rates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "abbr"
    t.decimal  "rate",                         precision: 8, scale: 2, default: "0.0"
    t.text     "country_ids",    limit: 65535
    t.boolean  "static",                                               default: false
    t.datetime "deleted_at"
    t.integer  "products_count",                                       default: 0
    t.integer  "integer",                                              default: 0
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
    t.index ["deleted_at"], name: "products_management_tax_rates_deleted_at", using: :btree
  end

  create_table "main_products_management_variants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "product_id"
    t.string   "name"
    t.string   "sku"
    t.string   "state"
    t.string   "type"
    t.text     "keywords",                  limit: 65535
    t.string   "permalink"
    t.decimal  "cost",                                    precision: 15, scale: 2, default: "0.0"
    t.decimal  "price",                                   precision: 15, scale: 2, default: "0.0"
    t.string   "short_description"
    t.boolean  "master",                                                           default: false
    t.boolean  "featured",                                                         default: false
    t.datetime "deleted_at"
    t.integer  "entity_properties_count",                                          default: 0
    t.integer  "integer",                                                          default: 0
    t.boolean  "active",                                                           default: true
    t.integer  "transaction_details_count",                                        default: 0
    t.integer  "image_group_id"
    t.string   "barcode"
    t.datetime "created_at",                                                                       null: false
    t.datetime "updated_at",                                                                       null: false
    t.integer  "business_model_id"
    t.boolean  "bundled",                                                          default: false
    t.index ["deleted_at"], name: "products_management_variants_deleted_at", using: :btree
    t.index ["image_group_id"], name: "products_management_variants_image_group_id", using: :btree
    t.index ["product_id"], name: "products_management_variants_product_id", using: :btree
  end

  create_table "main_products_management_variants_bundles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "bundle_id"
    t.integer  "variant_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bundle_id"], name: "main_products_management_variants_bundles_bundle_id", using: :btree
    t.index ["deleted_at"], name: "index_main_products_management_variants_bundles_on_deleted_at", using: :btree
    t.index ["variant_id"], name: "main_products_management_variants_bundles_variant_id", using: :btree
  end

  create_table "main_returns_management_refunds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "transaction_id"
    t.integer  "variant_id"
    t.string   "state"
    t.integer  "quantity",                                              default: 0
    t.decimal  "amount",                       precision: 15, scale: 2, default: "0.0"
    t.text     "notes",          limit: 65535
    t.datetime "time"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.integer  "operator_id"
    t.index ["deleted_at"], name: "index_main_returns_management_refunds_on_deleted_at", using: :btree
    t.index ["store_id"], name: "main_returns_management_refunds_store_id", using: :btree
    t.index ["transaction_id"], name: "main_returns_management_refunds_transaction_id", using: :btree
    t.index ["variant_id"], name: "main_returns_management_refunds_variant_id", using: :btree
  end

  create_table "main_returns_management_returns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "transaction_id"
    t.integer  "variant_id"
    t.string   "state"
    t.integer  "quantity",                                                   default: 0
    t.decimal  "amount",                            precision: 15, scale: 2, default: "0.0"
    t.integer  "exchange_variant_id"
    t.integer  "exchange_quantity",                                          default: 0
    t.decimal  "exchange_amount",                   precision: 15, scale: 2, default: "0.0"
    t.text     "notes",               limit: 65535
    t.datetime "time"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                 null: false
    t.datetime "updated_at",                                                                 null: false
    t.integer  "operator_id"
    t.index ["deleted_at"], name: "index_main_returns_management_returns_on_deleted_at", using: :btree
    t.index ["exchange_variant_id"], name: "main_returns_management_refunds_exchange_variant_id", using: :btree
    t.index ["store_id"], name: "main_returns_management_refunds_store_id", using: :btree
    t.index ["transaction_id"], name: "main_returns_management_refunds_transaction_id", using: :btree
    t.index ["variant_id"], name: "main_returns_management_refunds_variant_id", using: :btree
  end

  create_table "main_returns_management_voids", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "transaction_id"
    t.string   "state"
    t.decimal  "amount",                       precision: 15, scale: 2, default: "0.0"
    t.text     "notes",          limit: 65535
    t.datetime "time"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.integer  "operator_id"
    t.index ["deleted_at"], name: "index_main_returns_management_voids_on_deleted_at", using: :btree
    t.index ["store_id"], name: "main_returns_management_voids_store_id", using: :btree
    t.index ["transaction_id"], name: "main_returns_management_voids_transaction_id", using: :btree
  end

  create_table "main_sales_management_customers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "type"
    t.string   "number"
    t.string   "phone_number"
    t.string   "id_number"
    t.datetime "deleted_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["deleted_at"], name: "index_main_sales_management_customers_on_deleted_at", using: :btree
  end

  create_table "main_sales_management_discount_redemptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "discount_id"
    t.string   "redeemable_type"
    t.integer  "redeemable_id"
    t.string   "state"
    t.datetime "deleted_at"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["deleted_at"], name: "index_main_sales_management_discount_redemptions_on_deleted_at", using: :btree
    t.index ["discount_id"], name: "main_sales_management_discount_redemptions_discount_id", using: :btree
    t.index ["redeemable_type", "redeemable_id"], name: "discount_redemptions_redeemable_type_and_redeemable_id", using: :btree
  end

  create_table "main_sales_management_discounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "code"
    t.string   "name"
    t.text     "description",                 limit: 65535
    t.date     "valid_from"
    t.date     "valid_until"
    t.integer  "redemption_limit",                                                   default: 0
    t.integer  "discount_redemptions_count",                                         default: 0
    t.decimal  "amount",                                    precision: 15, scale: 2, default: "0.0"
    t.string   "based_on"
    t.string   "discount_orientation"
    t.decimal  "discount_orientation_amount",               precision: 15, scale: 2, default: "0.0"
    t.string   "type"
    t.boolean  "concurrent",                                                         default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                         null: false
    t.datetime "updated_at",                                                                         null: false
    t.index ["deleted_at"], name: "index_main_sales_management_discounts_on_deleted_at", using: :btree
    t.index ["store_id"], name: "main_sales_management_discounts_store_id", using: :btree
  end

  create_table "main_sales_management_payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.decimal  "amount",                       precision: 15, scale: 2, default: "0.0"
    t.integer  "transaction_id"
    t.string   "state"
    t.text     "details",        limit: 65535
    t.string   "type"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.index ["deleted_at"], name: "index_main_sales_management_payments_on_deleted_at", using: :btree
    t.index ["transaction_id"], name: "main_sales_management_payment_transaction_id", using: :btree
  end

  create_table "main_sales_management_sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "name"
    t.string   "state"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer  "transactions_count", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "operator_id"
    t.index ["deleted_at"], name: "index_main_sales_management_sales_on_deleted_at", using: :btree
    t.index ["store_id"], name: "main_sales_management_sales_store_id", using: :btree
  end

  create_table "main_sales_management_transaction_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "transaction_id"
    t.integer  "variant_id"
    t.string   "state"
    t.string   "name"
    t.integer  "quantity",                                               default: 0
    t.decimal  "amount",                        precision: 15, scale: 2, default: "0.0"
    t.decimal  "cost",                          precision: 15, scale: 2, default: "0.0"
    t.decimal  "discount_amount",               precision: 15, scale: 2, default: "0.0"
    t.decimal  "tax_rate",                      precision: 8,  scale: 2, default: "0.0"
    t.text     "notes",           limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.index ["deleted_at"], name: "index_main_sales_management_transaction_details_on_deleted_at", using: :btree
    t.index ["transaction_id"], name: "main_sales_management_transaction_details_transaction_id", using: :btree
    t.index ["variant_id"], name: "main_sales_management_transaction_details_variant_id", using: :btree
  end

  create_table "main_sales_management_transactions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.integer  "sale_id"
    t.integer  "customer_id"
    t.string   "state"
    t.string   "number"
    t.string   "barcode"
    t.string   "type"
    t.text     "notes",                     limit: 65535
    t.integer  "transaction_details_count",                                        default: 0
    t.decimal  "amount",                                  precision: 15, scale: 2, default: "0.0"
    t.decimal  "cost",                                    precision: 15, scale: 2, default: "0.0"
    t.decimal  "tax_rate",                                precision: 8,  scale: 2, default: "0.0"
    t.boolean  "exclude_tax",                                                      default: false
    t.decimal  "discount_amount",                         precision: 15, scale: 2, default: "0.0"
    t.datetime "time"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                       null: false
    t.datetime "updated_at",                                                                       null: false
    t.index ["customer_id"], name: "main_sales_management_transactions_customer_id", using: :btree
    t.index ["deleted_at"], name: "index_main_sales_management_transactions_on_deleted_at", using: :btree
    t.index ["sale_id"], name: "main_sales_management_transactions_sale_id", using: :btree
    t.index ["store_id"], name: "main_sales_management_transactions_store_id", using: :btree
  end

  create_table "main_sales_management_variants_discounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "discount_id"
    t.integer  "variant_id"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["deleted_at"], name: "index_main_sales_management_variants_discounts_on_deleted_at", using: :btree
    t.index ["discount_id"], name: "main_sales_management_discount_variants_discount_id", using: :btree
    t.index ["variant_id"], name: "main_sales_management_discount_variants_variant_id", using: :btree
  end

  create_table "main_stores_management_business_models", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "identifing_name"
    t.boolean  "static",          default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["deleted_at"], name: "index_main_stores_management_business_models_on_deleted_at", using: :btree
    t.index ["identifing_name"], name: "index_main_stores_management_business_models_on_identifing_name", using: :btree
  end

  create_table "main_stores_management_stores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.integer  "branch_id"
    t.string   "name"
    t.string   "short_name"
    t.string   "state"
    t.datetime "deleted_at"
    t.integer  "transactions_count"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "business_model_id"
    t.index ["branch_id"], name: "stores_branch", using: :btree
    t.index ["deleted_at"], name: "index_main_stores_management_stores_on_deleted_at", using: :btree
  end

  create_table "main_users_management_accesses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "key",         limit: 65535
    t.boolean  "active",                    default: true
    t.integer  "parent_id"
    t.string   "type"
    t.datetime "deleted_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.text     "context",     limit: 65535
    t.text     "routes",      limit: 65535
    t.text     "categories",  limit: 65535
    t.text     "description", limit: 65535
    t.index ["deleted_at"], name: "index_main_users_management_accesses_on_deleted_at", using: :btree
    t.index ["parent_id"], name: "accesses_parent_id", using: :btree
  end

  create_table "main_users_management_roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "deleted_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["deleted_at"], name: "index_main_users_management_roles_on_deleted_at", using: :btree
    t.index ["resource_type", "resource_id"], name: "resource_id_and_resource_type", using: :btree
  end

  create_table "main_users_management_roles_accesses", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "role_id"
    t.integer "access_id"
    t.index ["access_id"], name: "roles_accesses_access_id", using: :btree
    t.index ["role_id"], name: "roles_accesses_role_id", using: :btree
  end

  create_table "main_users_management_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username"
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.boolean  "merchant",               default: false
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "deleted_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["confirmation_token"], name: "index_main_users_management_users_on_confirmation_token", unique: true, using: :btree
    t.index ["deleted_at"], name: "index_main_users_management_users_on_deleted_at", using: :btree
    t.index ["reset_password_token"], name: "index_main_users_management_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_main_users_management_users_on_unlock_token", unique: true, using: :btree
    t.index ["username"], name: "index_main_users_management_users_on_username", unique: true, using: :btree
  end

  create_table "main_users_management_users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "users_roles_role_id", using: :btree
    t.index ["user_id"], name: "users_roles_user_id", using: :btree
  end

  create_table "oauth_access_grants", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resource_owner_id",               null: false
    t.integer  "application_id",                  null: false
    t.string   "token",                           null: false
    t.integer  "expires_in",                      null: false
    t.text     "redirect_uri",      limit: 65535, null: false
    t.datetime "created_at",                      null: false
    t.datetime "revoked_at"
    t.string   "scopes"
    t.index ["application_id"], name: "fk_rails_b4b53e07b8", using: :btree
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree
  end

  create_table "oauth_access_tokens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",                               null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",                          null: false
    t.string   "scopes"
    t.string   "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "fk_rails_732cb83ab7", using: :btree
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree
  end

  create_table "oauth_applications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                                    null: false
    t.string   "uid",                                     null: false
    t.string   "secret",                                  null: false
    t.text     "redirect_uri", limit: 65535,              null: false
    t.string   "scopes",                     default: "", null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree
  end

  create_table "supports_addressable_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "zip_code"
    t.float    "latitude",         limit: 24
    t.float    "longitude",        limit: 24
    t.string   "addressable_type"
    t.integer  "addressable_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["addressable_type", "addressable_id"], name: "addresses_addressable_id_and_addressable_type", using: :btree
    t.index ["deleted_at"], name: "index_supports_addressable_addresses_on_deleted_at", using: :btree
  end

  create_table "supports_imageable_image_groups", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "name"
    t.string   "contextable_type"
    t.integer  "contextable_id"
    t.datetime "deleted_at"
    t.integer  "images_count",     default: 0
    t.integer  "variants_count",   default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["contextable_type", "contextable_id"], name: "image_groups_contextable_id_and_contextable_type", using: :btree
    t.index ["deleted_at"], name: "image_groups_deleted_at", using: :btree
  end

  create_table "supports_imageable_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.string   "attachment"
    t.integer  "position"
    t.string   "caption"
    t.datetime "deleted_at"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["deleted_at"], name: "images_deleted_at", using: :btree
    t.index ["imageable_type", "imageable_id"], name: "images_imageable_id_and_imageable_type", using: :btree
  end

  create_table "supports_importers_files", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "import_id"
    t.string   "syntax"
    t.string   "attachment"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deleted_at"], name: "index_supports_importers_files_on_deleted_at", using: :btree
    t.index ["import_id"], name: "supports_importers_attachments_import_id", using: :btree
  end

  create_table "supports_importers_imports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "store_id"
    t.string   "name"
    t.string   "state"
    t.text     "details",      limit: 65535
    t.datetime "started_at"
    t.datetime "finished_at"
    t.integer  "total_rows",                 default: 0
    t.integer  "current_row",                default: 0
    t.integer  "skipped_rows",               default: 0
    t.string   "import_class"
    t.text     "results",      limit: 65535
    t.string   "type"
    t.datetime "deleted_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["deleted_at"], name: "index_supports_importers_imports_on_deleted_at", using: :btree
    t.index ["store_id"], name: "supports_importers_imports_store_id", using: :btree
  end

  create_table "supports_trackable_tracks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "trackable_type"
    t.integer  "trackable_id"
    t.string   "editor_type"
    t.integer  "editor_id"
    t.text     "previous_record_attributes", limit: 65535
    t.string   "message"
    t.string   "tracked_state"
    t.datetime "deleted_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["deleted_at"], name: "supports_trackable_tracks_deleted_at", using: :btree
    t.index ["editor_type", "editor_id"], name: "supports_tracks_editor_type_and_editor_id", using: :btree
    t.index ["trackable_type", "trackable_id"], name: "supports_tracks_trackable_type_and_trackable_id", using: :btree
  end

  create_table "tenants_addressable_addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.string   "country"
    t.string   "zip_code"
    t.float    "latitude",         limit: 24
    t.float    "longitude",        limit: 24
    t.string   "addressable_type"
    t.integer  "addressable_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["addressable_type", "addressable_id"], name: "addresses_addressable_id_and_addressable_type", using: :btree
    t.index ["deleted_at"], name: "addresses_deleted_at", using: :btree
  end

  create_table "tenants_merchants_business_fields", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "keywords",   limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["deleted_at"], name: "tenants_business_fields_deleted_at", using: :btree
  end

  create_table "tenants_merchants_companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "domain"
    t.string   "slug"
    t.string   "name"
    t.string   "short_name"
    t.string   "logo"
    t.string   "business_registration_number"
    t.string   "gst_registration_number"
    t.string   "website"
    t.string   "email"
    t.string   "phone_number"
    t.string   "currency"
    t.string   "time_zone"
    t.string   "state"
    t.string   "owner_name"
    t.integer  "business_field_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["business_field_id"], name: "companies_busines_field_id", using: :btree
    t.index ["deleted_at"], name: "companies_deleted_at", using: :btree
    t.index ["domain"], name: "companies_domain", using: :btree
  end

  create_table "tenants_merchants_device_imei", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.string   "number"
    t.integer  "company_id"
    t.string   "state"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_tenants_merchants_device_imei_on_company_id", using: :btree
    t.index ["deleted_at"], name: "device_imei_deleted_at", using: :btree
  end

  create_table "tenants_settingable_settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "settingable_type"
    t.integer  "settingable_id"
    t.text     "options",          limit: 65535
    t.boolean  "enable",                         default: true
    t.datetime "deleted_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["deleted_at"], name: "tenants_settings_deleted_at", using: :btree
    t.index ["settingable_type", "settingable_id"], name: "tenants_settings_settingable_type_and_setingable_id", using: :btree
  end

  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
end
