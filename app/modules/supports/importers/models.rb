module Supports::Importers::Models
  def self.table_name_prefix
    'supports_importers_'
  end
end