#
# schema
#create_table :supports_importers_imports do |t|
#      
#  t.references :store, index: { name: "supports_importers_imports_store_id" }
#  t.string :name
#  t.string :state
#  t.text :details
#  t.datetime :started_at
#  t.datetime :finished_at
#  t.integer :total_rows
#  t.integer :current_row
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Supports
  module Importers
    module Models
      class Import < ::ApplicationRecord

        class_attribute :csv_headers
        class_attribute :process_reloading_every
        self.csv_headers= []
        self.process_reloading_every= 20

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        #belongs_to :store, class_name: ""
        has_one :file, class_name: "::Supports::Importers::Models::File", inverse_of: :import, foreign_key: :import_id, dependent: :destroy
        accepts_nested_attributes_for :file, allow_destroy: true, reject_if: :all_blank

        #
        # cache relationships
        #
        cache_has_one :file, inverse_name: :import

        #
        # serialize results to hash
        #
        serialize :results, Hash

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do

          state :new, initial: true
          state :in_progress
          state :paused
          state :completed
          state :error

          event :process, before: :pre_processing, after: :delay_processing!, guard: :csv_present_and_csv_header_valid do
            transitions to: :in_progress, from: [:new, :error, :paused]
          end
          event :pause, after: :after_pause do
            transitions to: :paused, from: :in_progress
          end
          event :finish, after: :after_finish do
            transitions to: :completed, from: [:in_progress, :paused]
          end
          event :error_happen, after: :after_error do
            transitions to: :error, from: :in_progress
          end
        end

        #
        # callbacks
        #
        after_initialize :initialize_results

        def import_class_constant
          @import_class_constant||= self.import_class.constantize rescue nil
        end

        #
        # set default details value
        #
        def initialize_results
          self.results||= { paused: [], errors: [], skipped: [], completed: [] }
        end

        #
        # to insert row data to database 
        # update current row based on counter every 20 count
        # should pause or continue every 20 times
        # Override if necessary
        #
        def processing!
          reloading_every= self.class.process_reloading_every
          file.open_attachment do |csv|
            header= csv.shift
            reloading_every= 1 if file.total_rows <= 20
            counter= 0
            # skipped to current row if import was paused
            skip= true
            while row= csv.shift
              # start from current row + 1
              if skip
                if counter < current_row
                  counter += 1
                else
                  skip= false
                end
              end
              next if skip
              check_whether_stop_or_continue_processing(reloading= counter % reloading_every == 0) do
                row_h= file.row_with_header(header, row)
                save_a_row(row_number=counter, params=row_h)
                counter+= 1
                update_row_info(self.total_rows, counter) #if counter % reloading_every == 0
              end
            end
            finish! if (total_rows == current_row)  && may_finish?             
          end
        end
        
        #
        # save a row to class constant table
        # Override if necessary
        #
        def save_a_row(row_number=0, params={})
          self.import_class_constant.create params
        end

        #
        # block method to check whether process import is paused or not
        #
        def check_whether_stop_or_continue_processing(reloading= false &block)
          self.reload if reloading
          unless self.paused?
            yield
          end
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res= cached_collection

            if params[:state].present?
              res = res.where state: params[:state]
            end

            if params[:common].present?
              res = res.search(params[:common])
            end

            if params[:type].present?
              import_type=  case params[:type]
                            when 'variant'
                              "Main::ProductsManagement::Importers::Variant"
                            end
              res = res.where(type: import_type)
            end

            res
          end

        end

        protected

          #
          # BEGIN aasm transitions callbacks
          #

          def csv_present_and_csv_header_valid
            if file.blank?
              self.errors.add(:state, "No csv file is presented") if file.blank?
              return false
            else
              unless file.valid_header?
                self.errors.add(:state, "Header csv file is not valid") 
                return false
              end
              return true
            end
          end

          def pre_processing
            readonlyfalse
            update_attribute(:started_at, DateTime.now) if started_at.blank?
            update_row_info(total= file.total_rows ) if total_rows == 0
          end

          #
          # send process to worker
          #
          def delay_processing!
            #processing!
            ::Supports::Importers::Workers::Import.perform_async(self.class.name, self.id)
          end

          def update_row_info(total=nil,current=nil)
            self.update_attribute(:total_rows, total) if total
            self.update_attribute(:current_row, current) if current
          end

          def after_finish
            update_attribute :finished_at, DateTime.now
            results[:completed]||= []
            results[:completed] << "Completed at #{DateTime.now.strftime("%d-%m-%Y %H:%M")}" 
            save
          end

          def after_error
            
          end

          def after_pause
            results[:paused]||= []
            results[:paused] << "Paused at #{DateTime.now.strftime("%d-%m-%Y %H:%M")}"
            save
          end

          #
          # END aasm transitions callbacks
          #

      end
    end
  end
end
