#
# Typically a product has many variants.  (Variant ~= specific size of a given shoe)
#
# If you have many variants with the same image don't bother with an image group,
#  just use the "products.images".
#
# Use ImageGroups for something like shoes. Lets say you have 3 colors, and each color has 10 sizes.
#  You would create 3 images groups (one for each color). The image for each size would be the same and
#  hence each variant would be associated to the same image_group for a given color.
#
# schema
#    create_table :supports_imageable_image_groups do |t|
#  t.string :type
#  t.string :name
#  t.references :contextable, polymorphic: true, index: { name: "image_groups_contextable_id_and_contextable_type" }
#  t.datetime :deleted_at, index: { name: "image_groups_deleted_at" }
#  t.integer :images_count, default: 0
#  t.timestamps
#end
#

module Supports
  module Imageable
    module Models
      class ImageGroup < ::ApplicationRecord

        #
        # simplify contextable_type class
        #
        class_attribute :simplify_contextable_type_classes
        self.simplify_contextable_type_classes=   { 
                                                    "product" => "Main::ProductsManagement::Models::Product"
                                                  }

        #
        # define relationships
        #
        belongs_to :contextable, polymorphic: true, optional: true
        include ::Supports::Imageable::Interfaces::HasImages

        #
        # validations
        #
        validates :name, presence: true, uniqueness: { allow_blank: true, scope: [:contextable_type, :contextable_id] }

        #
        # callbacks
        #
        before_validation :convert_simplified_contextable_type

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable
        cache_has_many :images, inverse_name: :imageable
        cache_belongs_to :contextable

        #
        # include has many variants
        #
        include ::Main::ProductsManagement::Interfaces::HasManyVariants
        cache_has_many :variants, :inverse_name =>  :image_group

        #
        # include search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        #
        # get all images url
        #
        def image_urls image_size= :thumb
          Rails.cache.fetch("#{self.class.try(:cached_name)}-image-group-#{self.name}-image_urls-#{self}-#{image_size}", :expires_in => 3.hours) do
            fetch_images.empty? ? contextable.try(:image_urls, image_size) : fetch_images.map{|i| "#{ENV["base_url"]}#{i.attachment.url(image_size)}"}
          end
        end

        protected

          def convert_simplified_contextable_type
            self.contextable_type= self.class.simplify_contextable_type_classes[self.contextable_type] if self.class.simplify_contextable_type_classes.keys.include?(self.contextable_type)
          end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection.where(nil)
            if params[:common]
              res = res.search(params[:common])
            end
            res 
          end

        end

      end
    end
  end
end