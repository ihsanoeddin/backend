module Supports
  module Imageable
    module Interfaces
      module HasAvatar
        extend ::ActiveSupport::Concern
      
        included do 
          has_one :avatar, as: :imageable, class_name: "::Supports::Imageable::Models::Image", dependent: :destroy
          accepts_nested_attributes_for :avatar, allow_destroy: true, reject_if: :all_blank
        end

      end
    end
  end
end