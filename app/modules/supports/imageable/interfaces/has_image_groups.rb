module Supports
  module Imageable
    module Interfaces
      module HasImageGroups
        extend ::ActiveSupport::Concern
      
        included do 
          has_many :image_groups, as: :contextable, class_name: "::Supports::Imageable::Models::ImageGroup"
        end

      end
    end
  end
end