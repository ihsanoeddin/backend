
##
# PDF printer
##
module Supports
  module Printers
    class PDF

      require 'securerandom'

      attr_accessor :templates, :locals, :styles, :cache, :generated_token, :token_prefix, :pdf_header, :pdf_footer

      def initialize options={ templates_folder: nil, templates: nil, locals: nil, cache: true, token_prefix: nil}
        options.each do |k, v|
          self.send("#{k}=", v) if respond_to?(k)
        end
        renderer(options)
        default_styles
        valid_templates
      end

      def file(token=nil)
        render(token)
      end

      class << self

        def print(options= {token: nil})
          printer= self.new(options)
          printer.file(options[:token])
        end

      end

      protected

        def render(token= nil)
          token ||= generate_token(token_prefix)
          rendered = Rails.cache.fetch(token, expires_in: expiry_time) do
            engine.pdf_from_string *pdf_render_options
          end
        end

        def pdf_render_options
          [
            pdf_render_body_options,
            { 
              disposition: 'inline', 
              margin: { 
                top: self.styles[:top], 
                bottom: self.styles[:bottom] 
              },
              header: pdf_render_header_options,
              footer: pdf_render_footer_options 
            }
          ]
        end

        def pdf_render_body_options
          renderer.render_to_string(self.templates[:body], layout: self.templates[:layout], locals: self.locals || {} )
        end

        def pdf_render_header_options
          if self.pdf_header
            {
              :content => renderer.render_to_string(
                :template => self.pdf_header,
                :locals => self.locals
              ),
              :spacing => self.styles[:top] - 12
            }
          else
            {}
          end
        end

        def pdf_render_footer_options
          if self.pdf_footer
            {
             :footer => {
                    :content => renderer.render_to_string(
                    :template => self.pdf_footer
                )
              }
            }
          else
            {}
          end
        end

        def renderer(options={})
          return @renderer if @renderer
          @renderer ||= ::Printers::Renderer.new()
          @renderer.append_view_path(options[:templates_folder]) if options[:templates_folder]
        end

        def engine
          ::WickedPdf.new
        end

        def redis
          $redis
        end

        def default_styles
          if self.styles.is_a?(Hash)
            self.styles= { top: 1, bottom: 1 }.merge!(self.styles)
          else
            self.styles= { top: 1, bottom: 1 }
          end
        end

        def generate_token(prefix=nil)
          loop do
            token = SecureRandom.urlsafe_base64(8)
            key = "#{prefix}#{token}"
            self.generated_token= key
            break unless Rails.cache.exist?(key)
          end
          self.generated_token
        end

        def expiry_time
          Rails.env.development?? 1.minutes : 1.hour
        end

        def valid_templates
          unless self.templates.is_a?(Hash)
            self.templates= { body: self.templates.to_s }
          end
        end

    end
  end
end