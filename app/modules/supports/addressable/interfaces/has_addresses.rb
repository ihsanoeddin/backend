#
# this module is used for model to implement address relation
#
module Supports
  module Addressable
    module Interfaces
      module HasAddresses
        extend ::ActiveSupport::Concern

        included do 
          has_many :addresses, as: :addressable, class_name: '::Supports::Addressable::Models::Address', :dependent => :destroy
          accepts_nested_attributes_for :addresses, allow_destroy: true, reject_if: :all_blank
        end

      end
    end
  end
end
