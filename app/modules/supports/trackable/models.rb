module Supports::Trackable::Models
  def self.table_name_prefix
    'supports_trackable_'
  end
end