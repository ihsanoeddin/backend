#
# TODO : complete the code
# guard module provide mechanism to insert or update object attributes with specific rules
# this module will :
# - define `guard` attribute
# - 
#
module Supports
  module Guards
    module Protect
      extend ::ActiveSupport::Concern

      included do
        attr_accessor :guardian # will be set to true 
      end

    end
  end
end