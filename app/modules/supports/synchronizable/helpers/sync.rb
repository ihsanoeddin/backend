module Supports
  module Synchronizable
    module Helpers
      module Sync
        extend ActiveSupport::Concern

        included do 

          scope :updated_above, -> (time) { where("#{self.table_name}.updated_at > ?", time).order(:updated_at, :id) }
          scope :updated_below, -> (time) {where("#{self.table_name}.updated_at < ?", time) }

        end

        #
        # check whether object is updated by the time sync process is performed
        #
        def resource_status(last_sync)
          last_sync||= created_at
          return nil unless self.respond_to?(:created_at) && self.respond_to?(:updated_at)
          return 'deleted' if self.respond_to?(:deleted?) && self.deleted?
          if last_sync < created_at
            (created_at == updated_at) ? 'new' : 'updated'
          else
            (last_sync == updated_at) ? 'new' : 'updated'
          end
        end

        module ClassMethods

          #
          # get collection of synchronized object
          #
          def synchronize token, params={}
            ::Supports::Synchronizable::Helpers::Synchronize.new(self, token, params).synchronize
          end

        end

      end
    end
  end
end