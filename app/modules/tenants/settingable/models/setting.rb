#
# schema
#create_table :tenants_settingable_settings do |t|
#  t.string :name
#  t.references :settingable, polymorphic: true, index: { name: "tenants_settings_settingable_type_and_setingable_id" }
#  t.text :options
#  t.boolean :enable, default: true
#  t.datetime :deleted_at, index: { name: "tenants_settings_deleted_at" }
#  t.timestamps
#end
#

module Tenants
  module Settingable
    module Models
      class Setting < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        #
        # serialize options attributes as hash
        #
        serialize :options, Hash

        #
        # define relationships
        #
        belongs_to :settingable, polymorphic: true
        cache_belongs_to :settingable

        #
        # clear cached object
        #
        #after_commit :clear_cached_settingable!

        protected

          def clear_cached_settingable
            Rails.cache.delete("#{::Apartment::Tenant.current}-#{self.settingable_type.to_s.demodulize.parameterize}-#{self.settingable_id}-setting")
          end

      end
    end
  end
end