#
# this module is included to class that has setting feature
#

module Tenants
  module Settingable
    module Interfaces
      module HasSetting
        extend ::ActiveSupport::Concern
          #
          #include identity cache methods
          #
          

        included do 
          has_one :setting, :as => :settingable, class_name: "::Tenants::Settingable::Models::Setting"
          cache_has_one :setting, :inverse_name => :settingable, :embed => true
          accepts_nested_attributes_for :setting, allow_destroy: false, reject_if: :all_blank

          after_create :initialize_setting

        end

        #
        # cached setting object
        #
        def cached_setting
          Rails.cache.fetch("#{::Apartment::Tenant.current}-#{self.class.name.demodulize.parameterize}-#{self.id}-setting", expires_in: 1.day) do 
            self.initialize_setting
          end
        end

        protected

          #
          # init setting object
          # override on context class if necessary
          #
          def initialize_setting(options={})
            if self.setting.blank?
              self.setting= ::Tenants::Settingable::Models::Setting.create!(settingable_id: self.id, settingable_type: self.class.name, name: options.delete(:name) || "setting", options:  options)
            end
            self.setting
          end

      end
    end
  end
end