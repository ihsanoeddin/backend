module Tenants::Merchants::Models
  def self.table_name_prefix
    'tenants_merchants_'
  end
end