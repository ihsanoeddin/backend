#
# this worker class is used to create default records for company
# default records are consisted of:
# - accesses
# - account
# - roles
# 
#
module Tenants
  module Merchants
    module Workers

      class DefaultRecordsGenerator
        
        class_attribute :role_class,
                        :access_class,
                        :product_type_class,
                        :product_class,
                        :tax_rate_class,
                        :property_class,
                        :journal_class,
                        :balance_class,
                        :account_class,
                        :business_model_class

        self.role_class= ::Main::UsersManagement::Models::Role
        self.access_class= ::Main::UsersManagement::Models::Access

        self.product_type_class= ::Main::ProductsManagement::Models::ProductType
        self.product_class= ::Main::ProductsManagement::Models::Product
        self.tax_rate_class= ::Main::ProductsManagement::Models::TaxRate

        self.journal_class=  ::Main::Accounting::Models::Journal
        self.balance_class= ::Main::Accounting::Models::Balance
        self.account_class= ::Main::Accounting::Models::Account

        self.business_model_class= ::Main::StoresManagement::Models::BusinessModel

        include Sidekiq::Worker

        def perform(current_tenant)
          begin
            unless ::Apartment::Tenant.current.eql?(current_tenant)
              ::Apartment::Tenant.switch!(current_tenant) 
            end

            #
            # generate business models defaults
            #
            self.business_model_class.create!([{ name: 'Retail', identifing_name: 'retail', static: true }, { name: "Resto/Cafe", identifing_name: 'resto/cafe', static: true }])

            #
            # generate default accesses
            #
            self.access_class.destroy_all
            self.access_class.default_records_templates.each { |access_attributes| self.access_class.create!(access_attributes)  }

            #
            # generate default_roles
            #
            self.role_class.destroy_all
            self.role_class::DEFAULT_ROLE_NAMES.each { |named|  self.role_class.create!(name: named, access_ids: self.access_class.with_category(named).pluck(&:id)) }

            #
            # generate default product_types
            #
            self.product_type_class.destroy_all
            default_product_types= ["Food", "Beverages", "Electronics", "Clothing", "Other"].map{ |nm| self.product_type_class.create(name: nm) }
            
            #
            # default tax rates
            #
            self.tax_rate_class.destroy_all
            default_tax_rates= [{ name: 'Pajak Pertambahan Nilai', abbr: 'PPN', rate: 0.10  }].map{ |tx| self.tax_rate_class.create(tx) }

            #
            # accountings
            #

            #
            # default journal names
            #
            self.journal_class.destroy_all
            default_journals= [ { name: "Cash Revenue", identifing_name: 'cash_revenue' }, 
                                { name: "Inventory Expenditure", identifing_name: 'inventory_expenditure' }, 
                                { name: "Purchase" , identifing_name: 'purchase'},
                                { name: "Return", identifing_name: 'return', static: true },
                                { name: "Refund", identifing_name: "refund", static: true },
                                { name: "Void", identifing_name: "void", static: true },
                                { name: "Equity ShareHolder", identifing_name: "equity_shareholder", static: true }
                              ].map{ |dj| self.journal_class.create(dj) }

            #
            # default account names
            #
            self.account_class.destroy_all
            default_accounts = [ { name: "Cash", code: 'cash', identifing_name: 'cash'}, 
                                 { name: 'Sales', code: 'sales', identifing_name: 'sales' },
                                 { name: 'Cost of Sales', code: 'cost_of_sales', identifing_name: 'cost_of_sales' },
                                 { name: "Inventory", code: 'inventory', identifing_name: 'inventory' },
                                 { name: 'PPN', code: 'ppn', identifing_name: 'ppn' },
                                 { name: "Rejected Inventory", code: "r_inventory", identifing_name: "r_inventory", static: true },
                                 { name: "Refund", code: "refund", identifing_name: "refund", static: true },
                                 { name: "Equity", code: "equity", identifing_name: "equity", static: true }
                               ].map{ |da| self.account_class.create(da)  }

          rescue ::Apartment::TenantNotFound => e
          end

        end
      end

    end
  end
end
