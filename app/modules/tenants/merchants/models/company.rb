# 
# table schema
#create_table :tenants_merchants_companies do |t|
#  t.string :type
#  t.string :domain, index: {name: 'companies_domain'}
#  t.string :name
#  t.string :short_name
#  t.string :logo
#  t.string :business_registration_number
#  t.string :gst_registration_number
#  t.string :website
#  t.string :email
#  t.string :phone_number
#  t.string :currency
#  t.string :time_zone
#  t.string :state
#  t.owner_name :string
#  t.datetime :deleted_at, index: true
#  t.timestamps
#end
#

module Tenants
  module Merchants
    module Models
      class Company < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable
        cache_index :domain

        extend ::FriendlyId
        friendly_id :domain, use: :slugged

        #
        # use carrierwave to handle upload logo
        #
        mount_uploader :logo, ::Tenants::Merchants::Uploaders::LogoUploader

        #
        # define relationships
        #
        belongs_to :business_field, class_name: "::Tenants::Merchants::Models::BusinessField"

        #
        # implement has addresses methods
        #
        include ::Tenants::Addressable::Interfaces::HasAddresses

        #
        # include has setting
        #
        include ::Tenants::Settingable::Interfaces::HasSetting

        #
        # @Override initialize_setting method
        #
        def initialize_setting(options={ name: "Merchant Setting", product_code: { prefix: self.domain, suffix: nil } })
          super
        end

        protected :initialize_setting

        #
        # define states
        #
        include ::AASM
        aasm column: :state do
          state :unconfirmed, initial: true
          state :confirmed

          event :confirm do
            transitions from: :unconfirmed, to: :confirmed
          end
        end

        #
        # company object validations
        #
        validates :domain,    format: { with: /\A[A-Za-z0-9\-\_]+\Z/ }, 
                              presence: true, 
                              uniqueness: { allow_blank: true }, 
                              length: { allow_blank: true, minimum: 3, maximum: 50 }, 
                              exclusion: { in: %w(www admin), allow_blank: true }
        validates :name,      presence: true, 
                              uniqueness: { allow_blank: true },
                              length: { allow_blank: true, minimum: 3, maximum: 50 }
        validates :email,     presence: true, 
                              uniqueness: { allow_blank: true },
                              format: { with: /@/ , allow_blank: true},
                              length: { allow_blank: true, maximum: 255 }
        #
        # TODO : validates with inclusion ruby gem money
        validates :currency,  presence: true
        #
        #
        validates :time_zone, presence: true, 
                              inclusion: { allow_blank: true, in: ::ActiveSupport::TimeZone.all.map(&:to_s) }
        
        before_validation :construct_domain, if: Proc.new{|company| company.domain.blank? && company.name.present? }, on: :create
        
        class << self

          #
          # get all cached companies object
          #
          def cached
             Rails.cache.fetch("cached_companies", expires_in: 12.hours) do
              select(:id, :domain)
            end
          end

        end

        #
        # just to simplify user class name
        #
        def users
          ::Main::UsersManagement::Models::User
        end

        #
        # mysql can not interpret '-'
        #
        def domain_underscore
          domain.underscore
        end

        protected

          #
          # subdomain slug candidates
          #
          def slug_candidates
            [ :domain,[:domain,:id] ]
          end

          #
          # construct domain from name
          #
          def construct_domain
            self.domain= name.gsub(/[^0-9a-z ]/i, '').parameterize if self.domain.blank?         
          end

      end
    end
  end
end