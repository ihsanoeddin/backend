# Schema
#create business fields table
#create_table :tenants_merchants_business_fields do |t|
#  t.string :name
#  t.text :keywords
#  t.datetime :deleted_at
#  t.timestamps
#end
#

#
# this class is used as base for querying product type and product brand
# records of this classes is provided on seeds.rb
#

module Tenants
  module Merchants
    module Models
      class BusinessField < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable
        
        #
        # serialize keywords attributes as array
        #
        serialize :keywords, Array

        #
        # define relationship
        #     
        has_many :companies, class_name: "::Tenants::Merchants::Models::Company"

        #
        # validations
        #
        validates :name, presence: true

      end
    end
  end
end