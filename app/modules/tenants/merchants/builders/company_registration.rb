#
# this class is used for company registration process
#

module Tenants
  module Merchants
    module Builders
      class CompanyRegistration
        
        attr_accessor :params, :errors, :admin, :company, :valid

        def initialize params={}
          self.params= params
        end

        #
        # simulate active record relation save process
        #
        def save
          begin
            #::ActiveRecord::Base.transaction do
              if both_valid?
                begin
                  company.save
                  ::Apartment::Tenant.create(company.domain_underscore)
                rescue ::Apartment::TenantExists, ::ActiveRecord::NoDatabaseError
                ensure ::Apartment::Tenant::switch!(company.domain_underscore)
                  build_admin(newobject=true).save
                  build_admin.add_role(:super_admin)
                  #
                  # generate default records for company
                  #
                  ::Tenants::Merchants::Workers::DefaultRecordsGenerator.perform_in(15.second, company.domain_underscore)
                end
              end
              if errors.blank?
                return true
              else  
                return false
              end
            #end
          rescue => e
            Rails.logger.error e.message
            Rails.logger.error e.backtrace
            raise ActiveRecord::Rollback
            return false;
          end
        end

        protected

          #
          # to check whether the company and admin is valid?
          #
          def both_valid?
            if build_company.valid? and build_admin.valid?
              self.valid= true
            else
              build_admin.valid?
              self.errors= build_company.errors.messages.merge!(build_admin.errors.messages)
              self.valid= false
            end
          end

          #
          # to build user admin
          #
          def build_admin(newobject=false)
            user_params= admin_params.merge(merchant: true)
            if newobject
              self.admin= build_company.users.new(user_params)
            else
              self.admin ||= build_company.users.new(user_params)
            end
          end

          #
          # to build company
          #
          def build_company
            self.company ||= ::Tenants::Merchants::Models::Company.new(company_params)
          end

          #
          # define required params for building company object
          #
          def company_params
            ::ActionController::Parameters.new(params).require(:company).permit(:name, :domain, :owner_name, :email, :business_registration_number, 
                                                                                 :website, :phone_number, :time_zone, :currency, :business_field_id,
                                                                                 :addresses_attributes => [:name, :address, :city, :province, :country])
          end

          #
          # define required params for building user object
          #
          def admin_params
            ::ActionController::Parameters.new(params).require(:company).permit(:username, :email, :password, :password_confirmation)
          end

      end
    end
  end
end