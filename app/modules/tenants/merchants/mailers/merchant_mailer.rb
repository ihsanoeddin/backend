module Tenants
  module Merchants
    module Mailers
      class MerchantMailer < ActionMailer::Base
        
        default :from => 'Qashier <no-reply@qashier.com>'
        default template_path: "merchants"
        append_view_path Rails.root.join('app', 'modules', 'tenants', 'merchants', 'views', 'mailers')
      
      end
    end
  end
end
