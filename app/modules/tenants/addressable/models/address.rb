# Schema
#    create_table :tenants_addressable_addresses do |t|
#      t.string :type
#      t.string :name
#      t.string :address
#      t.string :city
#      t.string :state
#      t.string :country
#      t.string :zip_code
#      t.float :latitude
#      t.float :longitude
#      t.references :addressable, polymorphic: true, index: { name: 'addresses_addressable_id_and_addressable_type' }
#      t.datetime :deleted_at, index: true
#      t.timestamps
#    end
#

module Tenants
  module Addressable
    module Models
      class Address < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        belongs_to :addressable, polymorphic: true

      end
    end
  end
end