module Main
  module ProductsManagement
    module Interfaces
      module HasManyVariants
        extend ::ActiveSupport::Concern
      
        included do 
          has_many :variants, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: "#{self.name.demodulize.underscore}_id"
        end

      end
    end
  end
end