#
# schema
#create_table :main_products_management_product_types do |t|
#  t.string :name
#  t.string :permalink
#  t.text :description
#  t.datetime :deleted_at, index: { name: "products_management_products_type_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class ProductType < ::ApplicationRecord
        
        #
        # define relationships
        #
        has_many :products, class_name: "::Main::ProductsManagement::Models::Product", 
                            foreign_key: :product_type_id, :dependent => :destroy
        has_many :items, class_name: "::Main::ProductsManagement::Models::Types::Item", 
                          foreign_key: :product_type_id, :dependent => :destroy
        has_many :services, class_name: "::Main::ProductsManagement::Models::Types::Service", 
                          foreign_key: :product_type_id, :dependent => :destroy
        
        #
        # cache relationship
        #
        include ::Supports::Cacheable::Cacheable
        cache_has_many :products, :inverse_name => :product_type
        cache_has_many :items, :inverse_name => :product_type
        cache_has_many :services, :inverse_name => :product_type


        #
        # define validations
        #
        validates :name, presence: true, uniqueness: true

        #
        # use permalink as friendly id column
        #
        extend FriendlyId
        friendly_id :name, use: :slugged, slug_column: :permalink

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

      end
    end
  end
end