#
# schema
#create_table :main_products_management_tax_rates do |t|
#  t.string :name
#  t.string :abbr
#  t.decimal :rate, precision: 8, scale: 2
#  t.text :country_ids
#  t.boolean :static, default: false
#  t.datetime :deleted_at, index: { name: "products_management_tax_rates_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class TaxRate < ::ApplicationRecord

        #
        # define relationship
        #
        has_many :products, :class_name => "::Main::ProductsManagement::Models::Product",
                            foreign_key: :tax_rate_id
        has_many :items, :class_name => "::Main::ProductsManagement::Models::Types::Item",
                            foreign_key: :tax_rate_id
        has_many :services, :class_name => "::Main::ProductsManagement::Models::Types::Service",
                            foreign_key: :tax_rate_id

        #
        # cache relationship
        #
        include ::Supports::Cacheable::Cacheable
        self.caches_suffix_list= ['collection', "ppn"]
        cache_has_many :products, inverse_name: :tax_rate
        cache_has_many :items, inverse_name: :tax_rate
        cache_has_many :services, inverse_name: :tax_rate

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :abbr
        end


        #
        # define validations
        #

        validates :name, presence: true, uniqueness: true
        validates :abbr, presence: true
        validates :rate, presence: true, numericality: { allow_blank: true, maximum: 100, minimum: 0 }

        before_validation :generate_abbr, unless: :name

        def rate_percentage
          BigDecimal("#{rate}") / 100
        end

        class << self

          #
          # get ppn rate
          #
          def ppn
            Rails.cache.fetch("#{self.cached_name}-ppn", expires_in: 1.day) do 
              where(static: true, abbr: "PPN").first
            end
          end

        end

        protected

          #
          # generate abbr from name
          #
          def generate_abbr
            self.abbr= name.to_s.split(" ").map {|name| name[0].chr }.join.upcase if name
          end

      end
    end
  end
end