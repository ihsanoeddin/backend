#
#schema
#create_table :main_products_management_products do |t|
#  t.string :name
#  t.string :state
#  t.string :brand
#  t.string :type
#  t.string :keywords
#  t.string :permalink
#  t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.string :short_description
#  t.text :description
#  t.boolean :featured, default: false
#  t.references :product_type, index: { name: "products_management_products_product_type_id" }
#  t.references :tax_rate, index: { name: "products_management_products_tax_rate_id" }
#  t.datetime :deleted_at, index: { name: "products_management_products_deleted_at" }
#  t.timestamps
#end
#

module Main
  module ProductsManagement
    module Models
      class Product < ::ApplicationRecord
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable
        cache_index :permalink
        self.caches_suffix_list= ["collection", "image-urls-product-thumb", "image-urls-product-medium", "image-urls-product-original" ]

        #
        # make keywords attr array
        #
        serialize :keywords, Array

        #
        # define relationships
        #
        belongs_to :product_type, class_name: '::Main::ProductsManagement::Models::ProductType'#, counter_cache: true
        belongs_to :tax_rate, class_name: '::Main::ProductsManagement::Models::TaxRate', optional: true#, counter_cache: true
        belongs_to :business_model, class_name: "::Main::StoresManagement::Models::BusinessModel", foreign_key: :business_model_id
        include ::Supports::Imageable::Interfaces::HasImages
        include ::Supports::Imageable::Interfaces::HasImageGroups
        has_many :variants, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :product_id, inverse_of: :product, dependent: :destroy
        accepts_nested_attributes_for :variants, :allow_destroy => true, reject_if: :all_blank
        has_many :entity_properties, as: :entity, class_name: "::Main::ProductsManagement::Models::EntityProperty"
        accepts_nested_attributes_for :entity_properties, allow_destroy: true, reject_if: :all_blank
        has_many :properties, through: :entity_properties

        #
        # method delegation
        #
        delegate :name, to: :business_model, prefix: true

        #
        # cache relationship
        #
        cache_belongs_to :business_model, inverse_name: :products
        cache_has_many :variants, inverse_name: :product
        cache_has_many :images, inverse_name: :imageable
        cache_has_many :image_groups, inverse_name: :contextable
        cache_belongs_to :product_type
        cache_belongs_to :tax_rate
        cache_has_many :entity_properties, inverse_name: :entity

        #
        # use permalink as friendly id column
        #
        extend FriendlyId
        friendly_id :slug_candidates, use: :slugged, slug_column: :permalink

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :permalink#, :keywords
          attributes :product_type => "product_type.name"
          attributes :tax_rate => "tax_rate.name"
        end

        #
        # validations 
        #
        validates :name, presence: true, uniqueness: { allow_blank: true, scope: [:type] }
        validates :cost, presence: true, numericality: { allow_blank: true, greater_than: 0 }
        validates :price, presence: true, numericality: { allow_blank: true, greater_than: 0 }
        validates :product_type, presence: true
        validates :business_model, presence: true
        #validates :permalink, uniqueness: true

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :active
          state :inactive

          event :activate do
            transitions from: [:inactive, :draft], to: :active
          end
          event :deactivate do 
            transitions from: [:active, :draft], to: :inactive
          end
        end

        #
        # list of cached image url
        # TODO: set default no image name
        #
        def image_urls(image_size = :thumb)
          Rails.cache.fetch("#{self.class.cached_name}-image-urls-#{self.class.name.demodulize.downcase}-#{image_size}", expires_in: 1.day) do
            fetch_images.empty? ? ["no_image_#{image_size.to_s}.jpg"] : fetch_images.map{|i| "#{ENV["base_url"]}#{i.attachment.url(image_size)}" }
          end
        end

        #
        # calculate tax rate
        #
        def tax_rate_cost(input_price)
          input_price||= self.price
          if tax_rate
            BigDecimal("#{input_price}") * (BigDecimal("#{fetch_tax_rate.try(:rate_percentage) || 0}") / 100)
          else
            input_price
          end
        end

        protected

          #
          # permalin slug candidates
          #
          def slug_candidates
            [ :name,[:name,:id] ]
          end

          def should_generate_new_friendly_id?
            permalink.blank?
          end

        class << self

          #
          # filter records
          #
          def filter params={}
            res = cached_collection
            if params[:price_from].present?
              res = res.where("#{self.class.table_name}.price >= ?", params[:price_from])
            end
            if params[:price_to].present?
              res = res.where("#{self.class.table_name}.price <= ?", params[:price_to])
            end
            if params[:cost_from].present?
              res = res.where("#{self.class.table_name}.cost >= ?", params[:cost_from])
            end
            if params[:cost_to].present?
              res = res.where("#{self.class.table_name}.cost <= ?", params[:cost_to])
            end
            if params[:product_type_id].present?
              res = res.where(product_type_id: params[:product_type_id])
            end
            if params[:tax_rate_id].present?
              res = res.where(tax_rate_id: params[:tax_rate_id])
            end
            if params[:common].present?
              res= res.search(params[:common])
            end
            res # return query result
          end

        end

      end
    end
  end
end