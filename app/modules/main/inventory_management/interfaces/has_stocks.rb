module Main
  module InventoryManagement
    module Interfaces
      module HasStocks
     
        extend ::ActiveSupport::Concern

        included do 
          has_many :stocks, class_name: "::Main::InventoryManagement::Models::Stock", foreign_key: "#{self.name.demodulize.underscore}_id"
        end

        module VariantHelper

          extend ::ActiveSupport::Concern
          
          #
          # find product stock
          #
          def stock(store)
            @stock||= stocks.find_or_create_by(variant_id: self.id, store_id: store.id)
          end

          #
          # total_stocks
          #
          def available_stocks
            fetch_stocks.inject(0){|sum, stk| sum + stk.quantity_available }
          end

        end

        module StoreHelper

          extend ::ActiveSupport::Concern

          #
          # find stock of product on a store
          #
          def stock(variant)
            @stock||= stocks.where(variant_id: variant.id).limit(1)
          end

        end
        
      end
    end
  end
end