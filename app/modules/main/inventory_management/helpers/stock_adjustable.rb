module Main
  module InventoryManagement
    module Helpers
      module StockAdjustable
     
        extend ::ActiveSupport::Concern

        protected

          def adjust_stock!
            if stock_adjustment_params.present? 
              ::Main::InventoryManagement::Workers::StockAdjustment.perform_async(stock_adjustment_params.to_json)       
            end
          end

          #
          # check whether changes of attribute(s) trigger stock adjustment
          #
          def stock_adjustable? options= self.stock_adjustment_options
            fields= options[:depend_on_changes_of]
            adjust = false
            if fields.is_a?(String) or fields.is_a?(Symbol)
             fields= String(fields)
             if fields == "all"
               adjust= previous_changes.present?
             else
               adjust= previous_changes[fields].present?
             end
            elsif fields.is_a?(Array)
             fields.each do |field|
               adjust= previous_changes[field]
               break if adjust
             end
            elsif fields.is_a?(Hash)
              fields.each do |key,val|
                if previous_changes[key].present? or (send("#{key}_was") != send(key) )
                  if val.is_a?(Array)
                    adjust= if previous_changes[key].present?
                              previous_changes[key].any?{|change| val.include?(change) }
                            else
                              val.include?(send(key))
                            end
                  else
                    adjust= if previous_changes[key].present?
                                previous_changes[key].include?(val)
                            else
                              val == send(key)
                            end
                  end
                end
                break if adjust
              end
            end
            adjust
          end

          #
          # stock_adjustment hash attributes
          # Override!
          #
          def stock_adjustment_params
            raise "Must be implemented"
          end

        module ClassMethods

          #
          # adjust stock when
          #
          def adjust_stock! options={}
            options= { when: :after_commit, depend_on_changes_of: :all }.merge!(options)
            class_attribute :stock_adjustment_options unless respond_to?(:stock_adjustment_options)
            self.stock_adjustment_options= options
            Array(options[:when]).flatten.compact.each do |callback|
              send(callback, :adjust_stock!, if: :stock_adjustable?)
            end
          end

        end
        
      end
    end
  end
end