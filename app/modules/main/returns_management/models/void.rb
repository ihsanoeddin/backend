#
# schema
#create_table :main_returns_management_voids do |t|
#
#  t.references :store, index: { name: "main_returns_management_refunds_store_id" }
#  t.references :transaction, index: { name: "main_returns_management_refunds_transaction_id" }
#  #t.references :return, index: { name: "main_returns_management_refunds_return_id" }
#  #t.references :variant, index: { name: "main_returns_management_refunds_variant_id" }
#  t.string :state
#  #t.integer :quantity, default: 0
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.text :notes
#  t.datetime :time
#
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ReturnsManagement
    module Models
      class Void < ::ApplicationRecord
        
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # include has balances
        #
        include ::Main::Accounting::Interfaces::HasBalances
        insert_balance! depend_on_changes_of: { state: "accepted" }
        #
        # @Overriding params for balance insertion
        #
        def balance_insertion_params
          [ 
            { 
              journal_identifing_name: 'void', account_identifing_name: "sales", amount: self.transaction_amount_after_discount, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes,
              parts_attributes: [ 
                { journal_identifing_name: 'void', account_identifing_name: 'ppn', amount:  self.transaction_tax_amount, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes },
                { journal_identifing_name: 'void', account_identifing_name: 'cash', amount:  self.transaction_amount_after_tax, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes }
              ]
            },
            {
              journal_identifing_name: 'void', account_identifing_name: "inventory", amount: self.transaction_cost, balance_type: 'debt', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes,
              parts_attributes: [ 
                { journal_identifing_name: 'void', account_identifing_name: 'cost_of_sales', amount:  self.transaction_cost, balance_type: 'credit', reference_type: self.class.name, reference_id: self.id, time: self.time || DateTime.now, notes: self.notes }
              ]
            }    
          ]
        end

        #
        # include stock_adjustable helpers
        #
        include ::Main::InventoryManagement::Helpers::StockAdjustable
        adjust_stock! depend_on_changes_of: { state: "accepted" }#, when: :after_save

        #
        # @Override from ::Main::InventoryManagement::HelpersStockAdjustable
        #
        def stock_adjustment_params
          if self.variant.present? 
            transaksi.details.map{ |detail|
              { reference_type: self.class.name, reference_id: self.id, stock_id: detail.fetch_variant.try(:stock, store).try(:id), state: "published", count: detail.quantity, on: "count_on_store", stock_type: "reject" }
            }
          else 
            {}
          end
        end

        #
        # define relationships
        #
        include ::Main::StoresManagement::Interfaces::BelongsToStore
        belongs_to :transaksi, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :transaction_id#, counter_cache: :transaction_details_count

        #
        # cache relationships
        #
        cache_belongs_to :store, inverse_name: :voids
        cache_belongs_to :transaksi, inverse_name: :voids

        #
        # validations
        #
        validates :store, presence: true
        validates :transaksi, presence: true

        #
        # callbacks
        #
        before_validation :initialize_time
        before_validation :calculate_amount
        #before_update :valid_to_update?, unless: :state_changed?
        before_destroy :valid_to_update?

        #
        # validations
        #

        #
        # initialize time 
        #
        def initialize_time
          self.time||= DateTime.now
        end

        protected :initialize_time

        def calculate_amount
          self.amount = transaksi.amount_after_tax
        end

        def transaction_amount_after_discount
          fetch_transaksi.amount_after_discount rescue 0
        end

        def transaction_amount_after_tax
          fetch_transaksi.amount_after_tax
        end

        def transaction_tax_amount
          fetch_transaksi.total_tax
        end

        def transaction_cost
          begin
          self.transaksi.details.inject(0){ |sum, detail|  sum = sum + detail.cost }
            rescue => e
              Rails.logger.info e.message
              Rails.logger.info e.backtrace
              return 0
          end
        end

        def subtotal
          
        end

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :processed
          state :rejected
          state :accepted
          event :process do 
            transitions from: :draft, to: :processed
          end
          event :reject do 
            transitions from: [:draft, :processed], to: :rejected
          end
          event :accept, after: :after_accept_event do 
            transitions from: [:draft, :processed, :rejected], to: :accepted
          end
        end

        #
        # method to apply on callback to authorize update
        #
        def valid_to_update?
          unless draft?
            errors.add(:state, "can not update or delete")
            throw(:abort)
          end
        end

        class << self

          #
          # to filter records
          #
          def filter(params={})
            res= cached_collection

            if params[:id].present?
              res = res.where(:id => params[:id])
            end
            if params[:store_id].present?
              res= res.where(store_id: params[:store_id])
            end
            if params[:transaction_id]
              res= res.where(transaction_id: params[:transaction_id])
            end
            if params[:state].present?
              res= res.where(state: params[:state])
            end
            if params[:min_amount].present?
              params[:min_amount]= BigDecimal(params[:min_amount]) rescue 0
              res = res.where("amount >= ?", params[:min_amount])
            end
            if params[:max_amount].present?
              params[:max_amount]= BigDecimal(params[:max_amount]) rescue 0
              res = res.where("amount <= ?", params[:max_amount])
            end
            if params[:from_time].present?
              from_time= DateTime.parse(params[:from_time]) rescue nil
              res = res.where("time >= ?", from_time) if from_time
            end
            if params[:to_time].present?
              to_time= DateTime.parse(params[:to_time]) rescue nil
              res = res.where("time <= ?", to_time) if to_time
            end
            res
          end

        end

        protected

          def after_accept_event
            
          end

      end
    end
  end
end