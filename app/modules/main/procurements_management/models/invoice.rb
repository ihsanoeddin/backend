#
# schema
#create_table :main_procurements_management_invoices do |t|
#  t.references :purchase_order, index: { name: "main_purchase_orders_management_invoices_purchase_order_id" }
#  t.references :store, index: { name: "main_purchase_orders_management_invoices_store_id" }
#  t.string :number
#  t.string :supplier_delivery_order_number
#  t.date :date
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.string :state
#  t.float :tax_rate, default: 0
#  t.float :discount_rate, default: 0
#  t.text :notes
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class Invoice < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        belongs_to :purchase_order, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrder", foreign_key: :purchase_order_id
        belongs_to :store, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :store_id
        has_many :details, class_name: "::Main::ProcurementsManagement::Models::InvoiceDetail", foreign_key: :invoice_id, inverse_of: :invoice
        include ::Supports::Imageable::Interfaces::HasImages

        #
        # accepts nested attributes
        #
        accepts_nested_attributes_for :details, allow_destroy: true, reject_if: :all_blank

        #
        # cache relationships
        #
        cache_belongs_to :purchase_order, inverse_name: :invoices
        cache_belongs_to :store, inverse_name: :invoices
        cache_has_many :details, inverse_name: :invoice

        #
        # validations
        #
        validates :store, presence: true
        validates :purchase_order, presence: true
        validates :date, presence: true, date: { allow_blank: true }
        validates :number, presence: true
        validate :valid_purchase_order

        #
        # invoice can be created or updated if purchase order state is `received`
        #
        def valid_purchase_order
          if purchase_order
            errors.add(:purchase_order_id, "can be created or updated if purchase order state is received") unless purchase_order.received?
          end
        end

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :number, :supplier_delivery_order_number
        end

        #
        # attr_accessors
        #
        attr_accessor :force_accepted # to trigger event accept after save

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :accepted
          state :paid
          state :rejected
          event :pay do 
            transitions from: [:accepted], to: :paid
          end
          event :accept do 
            transitions from: [:draft, :rejected], to: :accepted
          end
          event :reject do 
            transitions from: [:draft], to: :rejected
          end
        end

        #
        # callbacks
        #
        before_save :calculate_amount
        before_validation :generate_number, if: :new_record?
        after_create :force_accept, if: :force_accepted

        #
        # generate invoice unique number
        #
        def generate_number
          self.number= "INV-#{self.purchase_order.number}-#{self.purchase_order.invoices.count + 1}" if self.purchase_order.present?
        end

        #
        # accepted after create
        #
        def force_accept
          self.accept! if self.force_accepted
        end

        #
        # calculate amount
        #
        def calculate_amount
          self.amount= self.details.inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.amount }
        end

        class << self

          #
          # filter records
          #
          def filter(params=[])
            res = cached_collection.order("created_at DESC")

            if params[:store_id].present?
              res = res.where(:store_id => params[:store_id])
            end
            if params[:purchase_order_id].present?
              res = res.where(purchase_order_id: params[:purchase_order_id])
            end
            if params[:common].present?
              res = res.search(params[:common]) 
            end
            if params[:state].present?
              res = res.where(state: params[:state])
            end
            res
          end

        end

      end
    end
  end
end