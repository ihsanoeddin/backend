#
# schema
#create_table :main_procurements_management_purchase_order_details do |t|
#  t.references :purchase_order, index: { name: "main_purchase_orders_management_purchase_order_details_purchase_order_id" }
#  t.references :variant, index: { name: "main_purchase_orders_management_purchase_order_details_variant_id" }
#  t.string :name
#  t.string :number
#  t.integer :quantity, default: 0
#  t.string :name
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.string :state
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class PurchaseOrderDetail < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        belongs_to :purchase_order, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrder", foreign_key: :purchase_order_id
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id

        #
        # cache relationships
        #
        cache_belongs_to :purchase_order, inverse_name: :details
        cache_belongs_to :variant, inverse_name: :purchase_order_details

        #
        # validations
        #
        # variant is required if `name` is blank
        validates :variant, presence: true, unless: :name
        # provide name if variant is not presented
        validates :name, presence: true, unless: :variant
        validates :purchase_order, presence: true
        validates :price, presence: true, numericality: { greater_than: 0, allow_blank: true }
        #validates :amount, presence: true, numericality: { allow_blank: true }
        validates :quantity, presence: true, numericality: { greater_than: 0, allow_blank: true }

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name
          attributes :variant => "variant.name"
        end

        #
        # callbacks
        #
        before_validation :generate_number, if: :new_record
        before_validation :set_price
        before_save :calculate_amount
        after_save :calculate_purchase_order_amount
        #before_update :valid_to_update?, if: :state_changed?
        before_destroy :valid_to_update?

        def valid_to_update
          unless purchase_order.draft?
            errors.add(:state, "can not update or delete")
            throw(:abort)
          end
        end

        #
        # set price from variant#cost
        #
        def set_price
          self.price= self.fetch_variant.cost if self.fetch_variant || self.variant 
        end

        #
        # generate number
        #
        def generate_number
          self.number||= "#{self.purchase_order.number}-#{self.purchase_order.with_deleted.count + 1}" if self.purchase_order
        end

        #
        # calculate amount
        #
        def calculate_amount
          self.amount= self.price * self.quantity
        end

        #
        # calculate purchase order amount
        #
        def calculate_purchase_order
          self.purchase_order.update_attribute :amount, self.purchase_order.details.inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.amount }
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection.order("created_at DESC")
            if params[:common].present?
              res= res.search(params[:common])
            end
            if params[:purchase_order_id].present?
              res = res.where(purchase_order_id: params[:purchase_order_id])
            end
            res
          end

        end

      end
    end
  end
end