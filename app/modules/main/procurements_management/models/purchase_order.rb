#
# schema
#create_table :main_procurements_management_purchase_orders do |t|
#  t.references :supplier, index: { name: "main_purchase_orders_management_purchase_orders_supplier_id" }
#  t.references :store, index: { name: "main_purchase_orders_management_purchase_orders_store_id" }
#  t.string :number
#  t.date :order_date
#  t.date :expected_delivery_date
#  t.date :delivery_date
#  t.text :shipping_address
#  t.text :notes
#  t.string :state
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.text :notified_with
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class PurchaseOrder < ::ApplicationRecord

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # serialize notified with
        #
        serialize :notified_with, Array

        #
        # relationships
        #
        belongs_to :supplier, class_name: "::Main::ProcurementsManagement::Models::Supplier", foreign_key: :supplier_id
        belongs_to :store, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :store_id        
        has_many :details, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrderDetail", foreign_key: :purchase_order_id, inverse_of: :purchase_order
        has_many :invoices, class_name:  "::Main::ProcurementsManagement::Models::Invoice", inverse_of: :purchase_order, foreign_key: :purchase_order_id

        accepts_nested_attributes_for :details, allow_destroy: true, reject_if: :all_blank
        accepts_nested_attributes_for :invoices, allow_destroy: true, reject_if: :all_blank

        #
        # cache relationships
        #
        cache_belongs_to :supplier, inverse_name: :purchase_orders
        cache_belongs_to :store, inverse_name: :purchase_orders
        cache_has_many :invoices, reverse_name: :purchase_order

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :number, :notes
        end

        #
        # attr_accessors
        #
        attr_accessor :force_open # to change state to open after create

        #
        # stateful model
        #
        include ::AASM
        aasm column: :state do
          state :draft, initial: true
          state :opened
          state :processing
          state :received
          state :canceled
          state :completed
          event :open, after: :after_open_event, guard: :order_valid? do 
            transitions from: [:draft], to: :opened
          end
          event :process, after: :after_process_event do
            transitions from: [:opened, :draft], to: :processing
          end
          event :receive, after: :after_receive_event, guard: :guard_receive_event do 
            transitions from: [:opened, :processed], to: :received
          end
          event :cancel, after: :after_cancel_event do 
            transitions from: [:opened], to: :canceled
          end
          event :complete, after: :after_complete_event do 
            transitions from: [:processed, :received], to: :completed
          end
        end

        #
        # validations
        #
        validates :number, presence: true, uniqueness: { scope: :deleted_at }
        validates :supplier, presence: true
        validates :store, presence: true
        validates :order_date, date: true
        #expected delivery must be recent than order date
        validates :expected_delivery_date, date: { after: :order_date }
        #delivery date must be recent than order date
        validates :delivery_date, date: { after: :order_date }
        validates :shipping_address, presence: true
        validates :amount, presence: true, numericality: true

        #
        # callbacks
        #
        after_save :open_purchase_order, if: :force_open
        before_validation :generate_number
        before_save :calculate_amount
        before_validation :set_shipping_address
        before_destroy :valid_to_update?
        #before_update :valid_to_update?, unless: :state_changed?

        #
        # valid to destroy or update
        #
        def valid_to_update?
          unless draft?
            errors.add(:state, "can not update or delete")
            throw(:abort)
          end
        end


        #
        # set supplier first address if shipping address blank
        #
        def set_shipping_address
          self.shipping_address||= self.supplier.addresses.first.try(:full_address) if supplier
        end

        #
        # calculate sub total
        #
        def calculate_amount
          self.amount= (self.fetch_details || self.details).inject(0){ |sum, detail| sum += detail.deleted?? 0 : detail.amount } + self.shipping_cost + self.misc_cost
        end

        #
        # generate number
        #
        def generate_number
          if supplier.present? && store.present?
            self.number||= "#{self.store.short_name}-#{self.supplier.code}-#{DateTime.now.to_i}"
          end
        end

        #
        # open purchase order 
        #
        def open_purchase_order
          self.open! if self.force_open && self.new_record? && self.may_open?
        end

        #
        # start aasm callbacks
        #
        
        #
        # after open order event should:
        # - send notification to supplier based on notified_by
        # - another action if defined by requirement
        #
        def after_open_event
          
        end

        #
        # after cancel order event should:
        # - send notification based on notified_by
        #
        def after_cancel_event
          
        end

        #
        # guard receive event to provide invoice
        # - create invoices
        #
        def guard_receive_event(invoices_params=[])
          if self.update(invoices_params)
            return true
          else
            return false
          end
        end

        #
        # after receive order should:
        # - update delivery_date
        #
        def after_receive_event(invoices_params=[])
          update_attribute :delivery_date, Date.today
        end

        #
        # after process order should:
        # - 
        #
        def after_process_event
          
        end

        #
        # after complete event should:
        # - stamp completed at
        #
        def after_complete_event
          update_attribute :completed_at, DateTime.now
        end

        #
        # check whether po is valid
        # po is valid when :
        # - detail(s) is present
        #
        def order_valid?
          fetch_details.present? || details.present?
        end

        #
        # end aasm callbacks
        #

        class << self

            #
            # filter records
            #
            def filter(params={})
              res = cached_collection
              if params[:common].present?
                res = res.search(params[:common])
              end
              if params[:supplier_id].present?
                res = res.where(supplier_id: params[:supplier_id])
              end
              if params[:store_id].present?
                res = res.where(store_id: params[:store_id])
              end
              if params[:state].present?
                res = res.where(state: params[:state])
              end
              if params[:order].present?
                res = case params[:order]
                      when "desc"
                        res.order("updated_at DESC")
                      else
                        res.order("updated_at ASC")
                      end
              end
              res
            end

        end

      end
    end
  end
end