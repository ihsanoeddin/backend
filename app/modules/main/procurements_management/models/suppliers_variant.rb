#
# schema
#create_table :main_procurements_management_suppliers_variants do |t|
#  t.references :variant, index: { name: "main_purchase_orders_management_suppliers_variants_variant_id" }
#  t.references :supplier, index: { name: "main_purchase_orders_management_suppliers_variants_supplier_id" }
#  t.decimal :price, :default => 0.0, :precision => 15, :scale => 2
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module ProcurementsManagement
    module Models
      class SuppliersVariant < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # relationships
        #
        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id, inverse_of: :variant
        belongs_to :supplier, class_name: "::Main::ProductsManagement::Models::Supplier", foreign_key: :supplier_id, inverse_of: :supplier

        #
        # cache relationships
        #
        cache_belongs_to :variant, inverse_name: :suppliers_variants
        cache_belongs_to :supplier, inverse_name: :suppliers_variants

      end
    end
  end
end