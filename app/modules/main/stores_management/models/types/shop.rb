#
# schema
#  create_table :main_stores_managements_store do |t|
#    t.string :type
#    t.references :branch, index: { name: "stores_branch" }
#    t.string :name
#    t.string :short_name
#    t.string :state
#    t.datetime :deleted_at, index: true
#    t.timestamps
#  end
#

module Main
  module StoresManagement
    module Models
      module Types
        class Shop < ::Main::StoresManagement::Models::Store
        
          #
          # include cacheable methods
          #
          include ::Supports::Cacheable::Cacheable

        end
      end
    end
  end
end