#
# schema
#  create_table :main_stores_managements_store do |t|
#    t.string :type
#    t.references :branch, index: { name: "stores_branch" }
#    t.string :name
#    t.string :short_name
#    t.string :state
#    t.integer :business_model_id
#    t.datetime :deleted_at, index: true
#    t.timestamps
#  end
#

module Main
  module StoresManagement
    module Models
      class Store < ::ApplicationRecord

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable
        
        #
        # include this class on rolify resource_types
        #
        resourcify :roles, role_cname: "::Main::UsersManagement::Models::Role"

        #
        # define self relationship
        #
        has_many :branchs, class_name: self.name, foreign_key: :branch_id
        belongs_to :head, class_name: self.name, foreign_key: :branch_id
        belongs_to :business_model, class_name: "::Main::StoresManagement::Models::BusinessModel", foreign_key: :business_model_id
        has_many :sales, class_name: "::Main::SalesManagement::Models::Sale", foreign_key: :store_id
        has_many :transactions, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :store_id
        has_many :discounts, class_name: "::Main::SalesManagement::Models::Discount", foreign_key: :store_id
        has_many :discount_items, class_name: "::Main::SalesManagement::Models::DiscountTypes::Item", foreign_key: :store_id
        has_many :discount_transactions, class_name: "::Main::SalesManagement::Models::DiscountTypes::Transaction", foreign_key: :store_id
        has_many :returns, class_name: "::Main::ReturnsManagement::Models::Return", foreign_key: :store_id, inverse_of: :store
        has_many :refunds, class_name: "::Main::ReturnsManagement::Models::Refund", foreign_key: :store_id, inverse_of: :store
        has_many :voids, class_name: "::Main::ReturnsManagement::Models::Void", foreign_key: :store_id, inverse_of: :store
        has_many :purchase_orders, class_name: "::Main::ProcurementsManagement::Models::PurchaseOrder", foreign_key: :store_id
        has_many :invoices, class_name: "::Main::ProcurementsManagement::Models::Invoice", foreign_key: :store_id

        #
        # store has one image logo
        #
        include ::Supports::Imageable::Interfaces::HasAvatar

        #
        # methods delegation
        #
        delegate :name, to: :business_model, prefix: true

        #
        # define has addresses relations
        #
        include ::Supports::Addressable::Interfaces::HasAddresses

        #
        # define has many stocks
        #
        include ::Main::InventoryManagement::Interfaces::HasStocks
        include ::Main::InventoryManagement::Interfaces::HasStocks::StoreHelper
        cache_belongs_to :business_model, inverse_name: :stores
        has_many :variants, through: :stocks
        cache_has_many :stocks, inverse_name: :store
        cache_has_many :sales, inverse_name: :store
        cache_has_many :discounts, inverse_name: :store
        cache_has_many :discount_items, inverse_name: :store
        cache_has_many :discount_transactions, inverse_name: :store
        cache_has_many :returns, inverse_name: :store
        cache_has_many :refunds, inverse_name: :store
        cache_has_many :voids, inverse_name: :store
        cache_has_many :purchase_orders, inverse_name: :store
        cache_has_many :invoices, inverse_name: :store

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name, :short_name
          #attributes :branchs => "branchs.name"
        end

        #
        # use permalink as friendly id column
        #
        extend FriendlyId
        friendly_id :slug_candidates, use: :slugged, slug_column: :short_name

        #
        # add role super admin after create shop
        #
        after_create :add_default_role, if: Proc.new{ |store|  store.creator.present? }

        #
        # this accessor is preserved for object of ::Main::UsersManagement:Models::User class
        #
        attr_accessor :creator
        
        #
        # define validations
        #
        validates :name, presence: true
        #validates :short_name, presence: true, uniqueness: { scope: :deleted_at, allow_blank: true }

        class << self

          #
          # this method is used to filter records
          #
          def filter(params={})
            res= cached_collection.where(nil)
            if params[:branch_id].present?
              res = res.where(branch_id: params[:branch_id])
            end
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end

        end

        #
        # abbr from name
        #
        def abbr_name
          self.name.split(" ").map {|name| name[0].chr }.join.upcase 
        end

        protected

          #
          # this method is used to add default role to creator
          #
          def add_default_role
            raise ArgumentError("Object of ::Main::UsersManagement:Models::User is not provided") unless self.creator.respond_to?(:add_role)
            self.creator.add_role :super_admin, self
          end

          #
          # permalin slug candidates
          #
          def slug_candidates
            [ :abbr_name,[:abbr_name,:id] ]
          end

      end
    end
  end
end