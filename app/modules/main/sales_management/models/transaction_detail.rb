#
# schema
#create_table :main_sales_management_transaction_details do |t|
#
#  t.references :transaction, index: { name: "main_sales_management_transaction_details_transaction_id" }
#  t.references :variant, index: { name: "main_sales_management_transaction_details_variant_id" }
#  t.string :state
#  t.integer :quantity
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :cost, :default => 0.0, :precision => 15, :scale => 2
#  t.decimal :tax_rate, default: 0.0, precision: 8, :scale => 2
#  t.decimal :discount_amount, default: 0.0, precision: 15, scale: 2
#  t.text :notes
#  t.datetime :time
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class TransactionDetail < ::ApplicationRecord
        
        #
        # support batch insertion
        #
        include ::Supports::Batchable

        include ::Supports::Trackable::Interfaces::HasTracks
        track!

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # include stock_adjustable helpers
        #
        include ::Main::InventoryManagement::Helpers::StockAdjustable
        adjust_stock! depend_on_changes_of: { state: "completed" }#, when: :after_save

        #
        # @Override from ::Main::InventoryManagement::HelpersStockAdjustable
        #
        def stock_adjustment_params
          if self.variant.present? 
            { reference_type: self.class.name, reference_id: self.id, stock_id: fetch_variant.try(:stock, store).try(:id), state: "published", count: -(self.quantity), on: "count_on_store", stock_type: "sale"  } 
          else 
            {}
          end
        end

        protected :stock_adjustment_params

        #
        # relationships
        #
        belongs_to :transaksi, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :transaction_id#, counter_cache: :transaction_details_count
        belongs_to :variant, -> { with_deleted }, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id, optional: true#, counter_cache: :transaction_details_count
        has_many :discount_redemptions, class_name: "::Main::SalesManagement::Models::DiscountRedemption", as: :redeemable

        #
        # cache relationships
        #
        cache_belongs_to :transaksi, inverse_name: :transaction_details
        cache_belongs_to :variant, inverse_name: :transaction_details
        cache_has_many :discount_redemptions, inverse_name: :redeemable

        #
        # callbacks
        #
        before_validation :set_time
        before_validation :calculate_amount
        before_validation :calculate_cost
        before_save :calculate_discount
        before_save :calculate_tax_rate
        after_save :calculate_transaction_amount
        after_destroy :calculate_transaction_amount

        #
        # use AASM
        #
        include ::AASM
        aasm column: :state do
          state :in_progress, initial: true
          state :paid
          state :completed
          state :canceled
          event :pay do
            transitions from: :in_progress, to: :paid
          end
          event :cancel do 
            transitions from: :in_progress, to: :canceled
          end
          event :complete do 
            transitions from: :paid, to: :completed
          end
        end

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :transaction_number => "transaksi.number"
        end

        #
        # validations
        #
        validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: Proc.new { |trans|  trans.valid_quantity? } }
        validates :amount, presence: true, numericality: { greater_than: 0 }
        validates :transaksi, presence: true
        validates :name, presence: true, unless: :variant_id
        validates :variant, presence: true, unless: :name

        #
        # attr_accessors
        #
        attr_accessor :current_datetime, :discount_ids

        #
        # get store
        #
        def store
          fetch_transaksi.try(:fetch_store) || transaksi.try(:store)
        end

        #
        # check whether variant is present
        # calculate amount based on variant price - discount
        #
        def calculate_amount
          self.amount= if self.variant
                          self.variant.price * self.quantity
                        else
                          self.amount * self.quantity
                        end
        end

        def calculate_cost
          self.cost=  if self.variant
                        self.variant.cost * self.quantity
                      else
                        self.cost
                      end
        end

        #
        # valid quantity for transaction of store
        #
        def valid_quantity?
          if variant
            #
            # just return 0 if stock is not present
            # usually it can not be happening as stock is defined on inventory management before sales management is operated
            #
            variant.stock(transaksi.store).try(:count_on_store) || 0 
          else
            self.quantity#just return the same as quantity
          end
        end

        #
        # get list available_discounts
        #
        def available_discounts
          @available_discounts||= if persisted?
                                    discount_redemptions.map{|discount_redemption| discount_redemption.fetch_discount || discount_redemption.discount }.compact.uniq
                                  else
                                    if self.discount_ids.present?
                                       Array(self.discount_ids).flatten.uniq.compact.map do |discount_id|
                                        ::Main::SalesManagement::Models::Discount.fetch(discount_id) rescue nil 
                                      end
                                    else
                                      ::Main::SalesManagement::Models::DiscountTypes::Item.available_discounts(variant) || []
                                    end
                                  end
        end

        #
        # get total discount
        #
        def total_discount
          self.discount_amount||= calculate_discount    
        end

        #
        # calculate discount amount
        # if concurrent discount present with another unconcurrent discount disregard it
        # return discount amount of transaction
        #
        def calculate_discount
          self.discount_amount= available_discounts.compact.inject(0) do |sum, discount| 
            sum += discount.apply(discount_options)
          end
        end

        #
        # set tax rate
        #
        def calculate_tax_rate
          self.tax_rate= self.fetch_variant.try(:product).try(:tax_rate).try(:rate) || 0
        end

        #
        # define discount options
        #
        def discount_options
          { discount_object: self }
        end

        #
        # to check whether it has discount
        #
        def got_discount?
          fetch_discount_redemptions.present?
        end

        #
        # calculate total tax
        #
        def total_tax
          amount_after_discount * (self.tax_rate/100)
        end

        #
        # get amount after discount
        #
        def amount_after_discount
           self.amount - self.discount_amount
        end

        #
        # get amount after tax
        #
        def amount_after_tax
          amount_after_discount + total_tax
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:variant_id].present?
              res = res.where(variant_id: params[:variant_id])
            end
            if params[:transaction_id].present?
              res = res.where(transaction_id: params[:transaction_id])
            end
            if params[:common].present?
              res = res.search(params[:common])
            end
            if params[:min_quantity].present?
              res = res.where("quantity >= ?", params[:min_quantity])
            end
            if params[:max_quantity].present?
              res = res.where("quantity <= ?", params[:max_quantity])
            end
            if params[:min_amount].present?
              res = res.where("amount >= ?", params[:min_amount])
            end
            if params[:max_amount].present?
              res = res.where("amount <= ?", params[:max_amount])
            end
            if params[:from_time].present?
              from_time= DateTime.parse(params[:from_time]) rescue nil
              res = res.where("created_at >= ?", from_time) if from_time
            end
            if params[:to_time].present?
              to_time= DateTime.parse(params[:to_time]) rescue nil
              res = res.where("created_at <= ?", to_time) if to_time
            end
            res
          end

        end

        protected

          #
          # insert transaction.time if object#time value is blank
          #
          def set_time
            self.time||= self.fetch_transaksi.try(:time) || DateTime.now if self.respond_to?("time=")
          end

          #
          # calculate transaction amount prior to update
          #
          def calculate_transaction_amount
            self.fetch_transaksi.update_attribute(:amount, self.transaksi.calculate_amount)
            self.fetch_transaksi.update_attribute(:cost, self.transaksi.calculate_cost)
          end

          #
          # create discount redemptions if discount available or discount_ids presents
          # this method is called on after_create callback if object#state is `in_progress`
          #
          after_create :create_discount_redemptions, if: :in_progress?
          def create_discount_redemptions
            if available_discounts.compact.present?
              available_discounts.compact.each{|discount| discount.create_redemption(redeemable: self) }
            end
          end


      end
    end
  end
end
