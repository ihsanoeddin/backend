#
# schema
#create_table :main_sales_management_customers do |t|
#
#  t.string :name
#  t.string :type
#  t.string :number
#  t.string :phone_number
#  t.string :id_number
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class Customer < ::ApplicationRecord

        #
        # use search cop
        #

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        has_many :transactions, class_name: "::Main::SalesManagement::Models::Transaction", foreign_key: :customer_id

        cache_has_many :transactions, inverse_name: :customer

        has_many :addresses, as: :addressable, class_name: '::Supports::Addressable::Models::Address', :dependent => :destroy
        accepts_nested_attributes_for :addresses, allow_destroy: true, reject_if: :all_blank

        cache_has_many :addresses, inverse_name: :addressable

        validates :name, presence: true
        validates :number, presence: true, uniqueness: true
        validates :id_number, presence: true

        #
        # callbacks
        #
        before_validation :set_number

        #
        # include search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name, :number, :id_number, :phone_number
        end

        #
        # set number uniq
        #
        def set_number
          self.number ||= loop do
            random_str = "#{SecureRandom.random_number(10000000000)}"
            break random_str unless self.class.exists?(number: random_str)
          end  
        end

        protected :set_number

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common]
              res = res.search(params[:common])
            end
            res
          end

        end

      end
    end
  end
end
