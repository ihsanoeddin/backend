#
# schema
#create_table :main_sales_management_discounts do |t|
#
#  t.references :store, index: { name: "main_sales_management_discounts_store_id" }
#  t.string :code
#  t.text :description
#  t.date :valid_from
#  t.date :valid_until
#  t.integer :redemption_limit, default: 0
#  t.integer :discount_redemptions_count, default: 0
#  t.decimal :amount, default: 0
#  t.string :based_on
#  t.string :discount_orientation
#  t.decimal :discount_orientation_amount, :default => 0.0, :precision => 15, :scale => 2
#  t.string :type
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      module DiscountTypes
        class Item < ::Main::SalesManagement::Models::Discount

          #
          # include cacheable
          #
          include ::Supports::Cacheable::Cacheable

          #
          # relationships definitions
          #
          belongs_to :store, class_name: "::Main::StoresManagement::Models::Store", foreign_key: :store_id, optional: true
          has_many :discount_redemptions, class_name: "::Main::SalesManagement::Models::DiscountRedemption", foreign_key: :discount_id
          has_many :variant_discounts, class_name: "::Main::SalesManagement::Models::VariantDiscount", foreign_key: :discount_id
          has_many :variants, through: :variant_discounts

          #
          # cache relationships
          #
          #cache_belongs_to :store, :inverse_name => :discount_items
          #cache_has_many :variant_discounts, inverse_name: :discount

          validates :discount_orientation, inclusion: { in: %w{ amount quantity }, allow_blank: true }
          validates :discount_orientation_amount, presence: true, numericality: { greater_than_or_equal_to: 0 }, if: :amount_oriented?
          validates :discount_orientation_amount, presence: true, numericality: { greater_than_or_equal_to: 1 }, if: :quantity_oriented?

          #
          # check whether discount orientiation based on item
          #
          def amount_oriented?
            discount_orientation == 'amount'
          end

          #
          # check whether discount orientation based on quantity
          #
          def quantity_oriented?
            discount_orientation == 'quantity'
          end

          #
          # get cached variants
          # it will be cleared when variants_discounts inserted, updated, or deleted
          # refer to VariantDiscount#clear_variants_and_discount_cache
          #
          def cached_variants
            Rails.cache.fetch("#{::Apartment::Tenant.current}-#{self.class.name.demodulize.underscore}-#{self.id}-cached-variants", expires_in: 1.day) do
              variants.load
            end 
          end

          #
          # @Override from parent class
          # return amount of discounted amount
          # return zero if condition is not satisfied
          #
          def apply(options={})
            discount_object = options[:discount_object]
            raise ArgumentError unless discount_object.is_a?(::Main::SalesManagement::Models::TransactionDetail)
            discount_amount = BigDecimal(percentage_based? ? percentage_discount(discount_object.amount) : amount)
            #create_redemption(redeemable: discount_object, discount: self) do 
              if self.redeemable? #&& included_on_store?(discount_object.try(:transaction).try(:store_id))
                if amount_oriented? && (discount_object.amount >= discount_orientation_amount)
                  discount_amount= discount_amount 
                elsif quantity_oriented?
                  discount_amount = Integer(discount_object.quantity) >=  Integer(self.discount_orientation_amount) ? discount_amount : 0
                else
                  discount_amount= discount_amount
                end
              else
                discount_amount= 0
              end
            #end
            discount_amount
          end

          def assign_items(options={})
            options= {product_type_ids: [], product_ids: [], variant_ids: []}.merge!(options)
            variants_arr= []
            if options[:product_type_ids].present?
              Array(options[:product_type_ids]).flatten.uniq.compact.each do |product_type_id|
                product_ids = ::Main::ProductsManagement::Models::ProductType.fetch(product_type_id).fetch_products.map{|product| product.id}
                variants_arr = ::Main::ProductsManagement::Models::Variant.select(:id, :product_id).where(product_id: product_ids).pluck(&:id)
              end
            end
            if options[:product_ids].present?
              variants_arr = ::Main::ProductsManagement::Models::Variant.select(:id, :product_id).where(Array(options[:product_ids]).flatten.compact.uniq).pluck(&:id)
            end
            if options[:variant_ids].present?
              variants_arr= ::Main::ProductsManagement::Models::Variant.select(:id).find(params[:variant_ids]).pluck(&:id)
            end
            self.update variant_ids: (Array(self.variant_ids).flatten + variants_arr).compact.uniq
          end

          class << self

            #
            # @Overrde from parent class
            # get all valid discounts
            #
            def available_discounts(variant= nil)
              if variant.present?
                variant.cached_discounts.reject{|discount| !discount.redeemable? }
              else
                Rails.cache.fetch("#{cached_name}-#{name.demodulize.underscore}-available-discounts", expires_in: 1.day) do 
                  available.reject{|discount| !discount.redeemable? }
                end
              end
            end

          end

        end
      end
    end
  end
end
