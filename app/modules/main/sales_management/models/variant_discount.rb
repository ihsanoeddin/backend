#
# schema
#
#create_table :main_sales_management_variants_discounts do |t|
#
#  t.references :discount, index: { name: "main_sales_management_discount_variants_discount_id" }
#  t.references :variant, index: { name: "main_sales_management_discount_variants_variant_id" }
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#
#end
#

module Main
  module SalesManagement
    module Models
      class VariantDiscount < ::ApplicationRecord

        self.table_name= "main_sales_management_variants_discounts"

        #
        # support batch insertion
        #
        include ::Supports::Batchable

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        belongs_to :variant, class_name: "::Main::ProductsManagement::Models::Variant", foreign_key: :variant_id
        belongs_to :discount, class_name: "::Main::SalesManagement::Models::Discount", foreign_key: :discount_id

        #
        # cache relationships
        #
        cache_belongs_to :variant, inverse_name: :variant_discounts
        cache_belongs_to :discount, inverse_name: :variant_discounts

        #
        # validattions
        #
        validates :variant, presence: true
        validates :discount, presence: true
        validate :valid_discount

        def valid_discount
          if (try(:fetch_discount) || discount ).present?
            self.errors.add(:discount_id, "Invalid discount type") unless discount.type.to_s.demodulize.underscore == 'item'
          end
        end

        #
        # callbacks
        #
        after_save :clear_variants_and_discount_cache
        after_destroy :clear_variants_and_discount_cache

        #
        # include search cop
        #
        include SearchCop
        search_scope :search do
          attributes :variant => "variant.name"
          attributes :discount => "discount.name"
        end

        class << self

          def filter(params={})
            res = cached_collection
            if params[:variant_id].present?
              res = res.where(variant_id: params[:variant_id])
            end
            if params[:discount_id].present?
              res = res.where(discount_id: params[:discount_id])
            end
            res
          end

        end

        protected

          def clear_variants_and_discount_cache
             Rails.cache.delete("#{::Apartment::Tenant.current}-variant-#{self.variant_id}-cached-discounts")
             Rails.cache.delete("#{::Apartment::Tenant.current}-variant-#{self.discount_id}-cached-variants")
          end

      end
    end
  end
end
