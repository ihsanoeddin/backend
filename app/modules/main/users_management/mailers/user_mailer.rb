module Main
  module UsersManagement
    module Mailers
      class UserMailer < ActionMailer::Base
        
      default :from => 'Qashier <no-reply@qashier.com>'
      default template_path: "users"
      append_view_path Rails.root.join('app', 'modules', 'main', 'users_management', 'views', 'mailers')

      #
      # this method is used to replicate devise confirmation process
      # TODO : change all message text to Int.
      #
      def send_confirmation(user,company, options={})
        @token = user.fetch_confirmation_token
        @user= user
        @company= company
        @options= options
        mail to: company.email, subject: 'Confirmation Instruction'
      end
      
      end
    end
  end
end