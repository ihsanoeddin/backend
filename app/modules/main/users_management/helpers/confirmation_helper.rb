#
# this module is used to process super admin confirmation for company or merchant
#
module Main
  module UsersManagement
    module Helpers
      module ConfirmationHelper
        
        extend ::ActiveSupport::Concern

        #
        # send confirmation instruction when company first registered
        #
        def send_confirmation_notification(company, options={ version: 'v1' })
          #
          # this should be on background
          #
          ::Main::UsersManagement::Mailers::UserMailer.send_confirmation(self, company, options).deliver_later
        end

      end
    end
  end
end