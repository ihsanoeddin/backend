module Main
  module UsersManagement
    module Serializers
      class RoleSerializer < ::ActiveModel::Serializer
        
        attributes :name, :deleted_at, :created_at, :resource_type, :resource_id, :updated_at, :accesses

        def accesses
          ::ActiveModel::Serializer::CollectionSerializer.new(object.accesses, serializer: ::Main::UsersManagement::Serializers::AccessSerializer)
        end

        def resource_type
          object.resource_type.to_s.demodulize.underscore
        end

      end
    end
  end
end