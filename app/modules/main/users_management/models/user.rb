#
# schema
#
#create_table :main_users_management_users do |t|
### Database authenticatable
# t.string :username
# t.string :email,              null: false, default: ""
# t.string :encrypted_password, null: false, default: ""

## Recoverable
# t.string   :reset_password_token
# t.datetime :reset_password_sent_at

## Rememberable
# t.datetime :remember_created_at

## Trackable
# t.integer  :sign_in_count, default: 0, null: false
# t.datetime :current_sign_in_at
# t.datetime :last_sign_in_at
# t.string   :current_sign_in_ip
# t.string   :last_sign_in_ip

## Confirmable
# t.string   :confirmation_token
# t.datetime :confirmed_at
# t.datetime :confirmation_sent_at
# t.string   :unconfirmed_email # Only if using reconfirmable

# t.boolean :merchant, default: false

## Lockable
# t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
# t.string   :unlock_token # Only if unlock strategy is :email or :both
# t.datetime :locked_at

# t.datetime :deleted_at

# t.timestamps null: false
#end
#

module Main
  module UsersManagement
    module Models
      class User < ::ApplicationRecord

        #include ::Supports::Trackable::Interfaces::HasTracks
        #track!
 
        # devise modules definition
        # :confirmable, :lockable, :timeoutable and :omniauthable
        devise :database_authenticatable, 
               :registerable,
               :recoverable, 
               :rememberable, 
               :trackable, 
               :validatable, 
               :confirmable, :authentication_keys => [:login]

        #
        # include devise module helper
        #
        include ::Main::UsersManagement::Helpers::DeviseHelper

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        self.object_caches_suffix= ['roles']

        #
        # include roles helper
        # this module is used when saving with roles params
        #
        include ::Main::UsersManagement::Helpers::RolesHelper

        #
        # include confirmation helper module
        #
        include ::Main::UsersManagement::Helpers::ConfirmationHelper
        #cache_has_one :avatar, inverse_name: :imageable

        #
        # include has_avatar 
        # this means user object has one avatar object
        #
        include ::Supports::Imageable::Interfaces::HasAvatar

        #
        # scopes
        #
        scope :current_signing_in, -> { where.not(:current_sign_in_at => nil) }
        scope :current_signing_out, -> { where.not(:last_sign_in_at => nil) }


        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :username, :email
          attributes :roles => "roles.name"
        end

        attr_accessor :login

        # define rolify methods
        rolify role_join_table_name: :main_users_management_users_roles, role_cname: "::Main::UsersManagement::Models::Role"

        validates :username, :presence => true, uniqueness: { scope: :deleted_at }

        #
        # check whether the user is merchant
        #
        def is_merchant?
          self.merchant
        end

        #
        # @Override : From Devise module Validatable
        # change email requirement as optional
        #
        def email_required?
          false
        end

        #
        # get cached user roles
        #
        def cached_roles
          Rails.cache.fetch("#{self.cached_name}-#{self.id}-roles", expires_in: 1.month) do 
            self.roles
          end
        end

        class << self
          #
          # @Override
          # override database authentication key
          #
          def find_for_database_authentication(warden_conditions)
            conditions = warden_conditions.dup
            login = conditions.delete(:login)
            if login.present? 
              where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
            elsif conditions.has_key?(:username) || conditions.has_key?(:email)
              where(conditions.to_h).first
            end
          end

          #
          # 
          #
          def filter params={}
            res = cached_collection.where(nil)
            if params[:store_id].present?
              res = res.joins(:roles).where("main_users_management_roles.resource_id = ? 
                                             and main_users_management_roles.resource_type= 'Main::StoresManagement::Models::Store'", 
                                             params[:store_id])
            end
            if params[:role_id].present?
              res = res.joins(:roles).where("main_users_management_roles.id = ?", params[:role_id])
            end
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end

        end

      end
    end
  end
end