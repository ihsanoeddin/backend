#
# schema
#  create_table :main_users_management_accesses do |t|
#    t.string :name
#    t.text :context
#    t.text :key
#    t.text :categories
#    t.references :parent, index: { name: 'accesses_parent_id' }
#    t.boolean :active, default: true
#    t.datetime :deleted_at, index: true
#    t.timestamps
#  end
#

module Main
  module UsersManagement
    module Models
      class Access < ::ApplicationRecord

        #
        # include templates records
        #
        include ::Main::UsersManagement::Templates::Accesses

        #
        # include cacheable methods
        #
        include ::Supports::Cacheable::Cacheable

        #
        # serialize context and key as hash
        #
        serialize :key, Array
        serialize :context, Array
        serialize :categories, Array
        #
        # disable STI column
        #
        self.inheritance_column = nil

        #
        # define self relationship
        #

        belongs_to :parent, class_name: self.name, foreign_key: :parent_id
        has_many :childs, class_name: self.name, foreign_key: :parent_id
        accepts_nested_attributes_for :childs, allow_destroy: true, reject_if: :all_blank

        #
        # cache relationships
        #
        cache_belongs_to :parent, inverse_name: :childs
        cache_has_many :childs, inverse_name: :parent
        
        #
        # define roles relationship
        #
        has_and_belongs_to_many :roles, :join_table => :main_users_management_roles_accesses

        #
        #scopes
        #
        scope :actives, -> { where(active: true) }
        scope :with_category, ->(category) { where("categories like ?", "% #{category}\n%")  }

        #
        # search cop implementation for text searching
        #
        include SearchCop
        search_scope :search do
          attributes :name, :key
        end

        #
        # argument for cancan `can` method
        #
        def keys
          Array(keys).flatten + Array(context).flatten
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res= cached_collection.where(nil)
            res = res.search(params[:common]) if params[:common]
            res
          end

        end


      end
    end
  end
end