#
# schema
#create_table :main_accounting_management_journals_balances do |t|
#  t.references :journal, index: { name: "main_accounting_management_journals_balances_journal_id" }
#  t.references :balance, index: { name: "main_accounting_management_journals_balances_balance_id" }
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module Accounting
    module Models
      class JournalsBalance < ::ApplicationRecord
      
        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        belongs_to :journal, class_name: "::Main::Accounting::Models::Journal", foreign_key: :journal_id
        belongs_to :balance, class_name: "::Main::Accounting::Models::Balance", foreign_key: :balance_id

        #
        # cache relationships
        #
        cache_belongs_to :journal, inverse_name: :journals_balances
        cache_belongs_to :balance, inverse_name: :journals_balances
                 

      end
    end
  end
end