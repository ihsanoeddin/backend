#
# schema
#
#create_table :main_accounting_management_balances do |t|
#  t.references :account, index: { name: "main_accounting_management_balances_account_id" }
#  t.references :reference, polymorphic: true, index: { name: "main_accounting_management_balances_reference_type_and_id"}
#  t.decimal :amount, :default => 0.0, :precision => 15, :scale => 2 
#  t.string :type 
#  t.integer :store_id
#  t.text :notes
#  t.datetime :time
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#

module Main
  module Accounting
    module Models
      class Balance < ::ApplicationRecord

        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        belongs_to :account, class_name: "::Main::Accounting::Models::Account", foreign_key: :account_id
        belongs_to :group, class_name: self.name, foreign_key: :group_id
        belongs_to :reference, polymorphic: true
        has_many :journals_balances, class_name: "::Main::Accounting::Models::JournalsBalance", foreign_key: :balance_id, inverse_of: :balance
        has_many :journals, through: :journals_balances
        has_many :parts, class_name: self.name, foreign_key: :group_id, dependent: :destroy, inverse_of: :group

        #
        # allow nested attributes for
        #
        accepts_nested_attributes_for :parts, allow_destroy: true, reject_if: :all_blank 


        #
        # cache relatioships
        #
        cache_belongs_to :account, inverse_name: :balances
        cache_has_many :journals_balances, inverse_name: :balance
        cache_belongs_to :group, inverse_name: :parts
        cache_has_many :parts, inverse_name: :group

        #
        # validations
        #
        validates :amount, presence: true, numericality: true
        validates :balance_type, presence: true, inclusion: { in: %w{debt credit}, allow_blank: true }, on: :create
        validates :journal_identifing_name, presence: true, inclusion: { in: ::Main::Accounting::Models::Journal::STATIC_JOURNALS.keys.map{|sj| sj.to_s }, allow_blank: true }, unless: :journal_ids
        validates :account_identifing_name, inclusion: { in: ::Main::Accounting::Models::Account::STATIC_ACCOUNTS.keys.map{|sa| sa.to_s }, allow_blank: true }, unless: :account_id
        validates :time, date: true
        #
        # attr_accessor
        #
        attr_accessor :journal, :journal_identifing_name, :account_identifing_name, :balance_type

        before_validation :initialize_class, on: :create
        before_validation :initialize_time
        before_validation :determine_journal, if: :journal_identifing_name
        before_validation :determine_account, if: :account_identifing_name

        #
        # callbacks
        #
        after_create :assign_journal
        after_save :assign_store
        before_destroy :valid_to_destroy

        #
        # only non reference balance object can be destroyed
        #
        def valid_to_destroy
          if reference.present?
            errors.add(:reference, "can not be deleted")
            throw(:abort)
          end
        end

        #
        # assign store id to balances
        # - if reference#respond_to?(:store_id)
        # - override presented store_id with reference#store_id
        #
        def assign_store
          if reference.respond_to?(:store_id)
            self.update_attribute(:store_id, reference.store_id)
          end 
        end

        #
        # print journal names
        #
        def journals_list
          fetch_journals_balances.map{|jb| jb.try(:fetch_journal) }
        end

        #
        # determine journal_id based on journal_identifing_name 
        #
        def determine_journal
          self.journal= ::Main::Accounting::Models::Journal.send("get_#{self.journal_identifing_name}") if ::Main::Accounting::Models::Journal.respond_to?("get_#{self.journal_identifing_name}")
        end

        #
        # determine account_id based on account_identifing_name
        #
        def determine_account
          self.account= ::Main::Accounting::Models::Account.send("get_#{self.account_identifing_name}") if ::Main::Accounting::Models::Account.respond_to?("get_#{self.account_identifing_name}")
        end

        #
        # account_name
        #
        def account_name
          fetch_account.try(:name) || account.try(:name)
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res= cached_collection
            if params[:account_id].present?
              res = res.where(account_id: params[:account_id])
            end
            if params[:journal_id].present?
              res = res.joins(:journals).where(main_accounting_management_journals: { id: params[:journal_id] })
            end
            if params[:store_id].present?
              res = res.where(store_id: params[:store_id])
            end
            if params[:type].present?
              type= case params[:type]
                    when 'debt'
                      "Main::Accounting::Models::BalanceTypes::Debt"
                    when 'credit'
                      "Main::Accounting::Models::BalanceTypes::Credit"
                    end
              res = res.where(type: type)
            end
            res
          end

        end

        protected

          def initialize_class
            case self.balance_type
            when 'debt'
              self.becomes!(::Main::Accounting::Models::BalanceTypes::Debt)
            when 'credit'
              self.becomes!(::Main::Accounting::Models::BalanceTypes::Credit)
            end
          end

          def initialize_time
            self.time||= DateTime.now
          end

          #
          # assign journal if journal present
          #
          def assign_journal
            if self.journal.present?
              self.journals << self.journal
            end 
          end

      end
    end
  end
end