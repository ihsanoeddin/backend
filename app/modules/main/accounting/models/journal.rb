#
#create_table :main_accounting_management_journals do |t|
#  t.string :name
#  t.string :identifing_name
#  t.text :description
#  t.datetime :deleted_at, index: true
#  t.timestamps null: false
#end
#
#

module Main
  module Accounting
    module Models
      class Journal < ::ApplicationRecord

        
        STATIC_JOURNALS={ 
                          cash_revenue: { name: "Cash Revenue", identifing_name: 'cash_revenue', static: true }, 
                          inventory_expenditure: { name: "Inventory Expenditure", identifing_name: 'inventory_expenditure', static: true }, 
                          purchase: { name: "Purchase" , identifing_name: 'purchase', static: true},
                          return: { name: "Return", identifing_name: 'return', static: true },
                          refund: { name: "Refund", identifing_name: "refund", static: true },
                          void: { name: "Void", identifing_name: "void", static: true },
                          equity_shareholder: { name: "Equity ShareHolder", identifing_name: "equity_shareholder", static: true }
                        }


        #
        # include cacheable
        #
        include ::Supports::Cacheable::Cacheable

        #
        # define relationships
        #
        has_many :journals_balances, class_name: "::Main::Accounting::Models::JournalsBalance", foreign_key: :journal_id, inverse_of: :journal
        has_many :balances, through: :journals_balances

        #
        # cache relationships
        #
        cache_has_many :journals_balances, inverse_name: :journal

        #
        # callbacks
        #
        before_destroy :only_static

        #
        # use search cop
        #
        include SearchCop
        search_scope :search do
          attributes :name
        end

        class << self

          #
          # filter records
          #
          def filter(params={})
            res = cached_collection
            if params[:common].present?
              res = res.search(params[:common])
            end
            res
          end

          #
          # define static object helpers
          #
          def static_object_helpers
            STATIC_JOURNALS.each do |key, val|
              class_eval %{
                def self.get_#{key.to_s}
                  self.find_by_identifing_name("#{key.to_s}") || self.create(STATIC_JOURNALS["#{key.to_s}".to_sym])
                end
              }
            end
          end

        end

        #
        # declare static object helpers
        #
        static_object_helpers

        protected

          #
          # prevent static object deletion
          #
          def only_static
            if self.static
              erros.add(:name, "can not delete default object")
              throw(:abort)
            end
          end

      end
    end
  end
end