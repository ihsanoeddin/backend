module Main
  module Accounting
    module Interfaces
      module HasBalances
      
        extend ::ActiveSupport::Concern

        included do 
          has_many :balances, as: :reference, class_name: "Main::Accounting::Models::Balance"
           accepts_nested_attributes_for :balances, allow_destroy: true, reject_if: :all_blank
        end

        #
        # check whether changes of attribute(s) trigger balance insertion
        #
        def balance_insertion? options= self.balance_insertion_options
          fields= options[:depend_on_changes_of]
          adjust = false
          if fields.is_a?(String) or fields.is_a?(Symbol)
           fields= String(fields)
           if fields == "all"
             adjust= previous_changes.present?
           else
             adjust= previous_changes[fields].present?
           end
          elsif fields.is_a?(Array)
           fields.each do |field|
             adjust= previous_changes[field]
             break if adjust
           end
          elsif fields.is_a?(Hash)
            fields.each do |key,val|
              if previous_changes[key].present? or (send("#{key}_was") != send(key) )
                if val.is_a?(Array)
                  adjust= if previous_changes[key].present?
                            previous_changes[key].any?{|change| val.include?(change) }
                          else
                            val.include?(send(key))
                          end
                else
                  adjust= if previous_changes[key].present?
                              previous_changes[key].include?(val)
                          else
                            val == send(key)
                          end
                end
              end
              break if adjust
            end
          end
          adjust
        end

        def insert_balance!
          if balance_insertion_params.present? 
            ::Main::Accounting::Workers::BalanceInsertion.perform_async(balance_insertion_params.to_json)       
          end
        end

        def balance_insertion_params
          raise "Must be implemented"
        end

        module ClassMethods

          #
          # insert balance when?
          #
          def insert_balance! options={}
            options= { when: :after_commit, depend_on_changes_of: :all }.merge!(options)
            class_attribute :balance_insertion_options unless respond_to?(:balance_insertion_options)
            self.balance_insertion_options= options
            Array(options[:when]).flatten.compact.each do |callback|
              send(callback, :insert_balance!, if: :balance_insertion?)
            end
          end

        end

      end
    end
  end
end