#
# this class is used as base class for handling company registration confirmation
#
module Api
  module V1
    module Register
      class Base < ::Api::V1::Base
        
        #
        # mount registrations and confirmation api endpoints
        #
        mount BusinessFields
        mount Registrations
        mount Confirmations

      end
    end
  end
end