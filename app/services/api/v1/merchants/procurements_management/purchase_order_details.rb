#
# this class is used to handle purchase order details
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class PurchaseOrderDetails < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::PurchaseOrderDetail'
          use_resource!

          self.presenter_name= "Merchants::ProcurementsManagement::PurchaseOrderDetail"

          helpers do 
            
            def purchase_order_detail_params
              posts[:purchase_order_detail]||= posts
              posts.require(:purchase_order_detail).permit(:variant_id, :name, :quantity, :price)
            end

          end

          resources "merchants/procurements_management/purchase_orders/:purchase_order_id/details" do 

            desc "[GET] index all merchants purchase orders"
            get do
              details= resource_class_constant.filter(filter_params)
              presenter paginate(details)
            end

            desc "[GET] show a detail"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a detail"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a detail"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update purchase_order_detail_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a detail"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

          end

        end
      end
    end
  end
end