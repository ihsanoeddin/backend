#
# this class is used to handle suppliers
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class Suppliers < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::Supplier'
          use_resource!

          self.presenter_name= "Merchants::ProcurementsManagement::Supplier"

          helpers do 
            
            def supplier_params
              posts[:supplier]||= posts
              posts.require(:supplier).permit(:name, :code, :email, :phone_number, :mode, :max_purchase_orders_qty, :min_purchase_orders_qty,
                                              :addresses_attributes => [:address, :name, :city, :state, :country, :zip_code, :latitutde, :longitude, :_destroy, :id],
                                              :contacts_attributes => [:id, :_destroy, :name, :email, :phone_number], :suppliers_variants_ids => [])
            end

          end

          resources "merchants/procurements_management/suppliers" do 

            desc "[GET] index all merchants suppliers"
            get do
              suppliers= resource_class_constant.filter(filter_params)
              presenter paginate(suppliers)
            end

            desc "[GET] show a supplier"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a supplier"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a supplier"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update supplier_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a supplier"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end