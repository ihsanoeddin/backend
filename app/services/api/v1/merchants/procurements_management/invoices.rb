#
# this class is used to handle invoices
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProcurementsManagement
        class Invoices < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::Invoice'
          use_resource!

          self.presenter_name= "Merchants::ProcurementsManagement::Invoice"

          #
          # change upload attributes to uploaded class
          #
          before do 
            begin
              if params[:images_attributes].present? 
                if params[:images_attributes].is_a?(Array) or params[:images_attributes].is_a?(Hashie::Mash)
                  params[:images_attributes].each_with_index do |images_attributes, index|
                    image_params = images_attributes.is_a?(Array) ? images_attributes.last[:attachment] : images_attributes[:attachment]
                    if image_params.respond_to?(:keys) && (image_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                      params[:images_attributes][index][:attachment]= ActionDispatch::Http::UploadedFile.new(image_params)
                    end
                  end
                end
              end
            rescue => e
              Rails.logger.info(e.message)
              Rails.logger.info(e.backtrace)
            end
          end

          helpers do 

            def invoice_params
              posts[:invoice]||= posts
              posts.require(:invoice).permit(:force_accepted, :store_id, :supplier_delivery_order_number, :date, :notes, :tax_rate, :discount_rate,
                                             :details => [:id, :_destroy, :variant_id, :received_quantity, :expected_quantity, :price, :name, :tax_rate, :discount_rate], 
                                             :images_attributes => [:id, :_destroy, :remote_attachment_url, :attachment ])
            end

          end

          resources "merchants/procurements_management/invoices" do 

            desc "[GET] index all merchants invoices"
            get do
              purchase_orders= resource_class_constant.filter(filter_params)
              presenter paginate(purchase_orders)
            end
          
          end

          resources "merchants/procurements_management/purchase_orders/:purchase_order_id/invoices" do 

            desc "[GET] index all merchants invoices scoped by purchase order"
            get do
              invoices= resource_class_constant.filter(filter_params)
              presenter paginate(invoices)
            end

            desc "[GET] show a invoice"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a invoice"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a invoice"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update invoice_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a invoice"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a invoice"
            put ":id/accept", requirements: { id: /[0-9]*/ } do 
              if context_resource.accept!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] reject a invoice"
            put ":id/reject", requirements: { id: /[0-9]*/ } do 
              if context_resource.reject!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] pay a invoice"
            put ":id/pay", requirements: { id: /[0-9]*/ } do 
              if context_resource.pay!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

          end

        end
      end
    end
  end
end