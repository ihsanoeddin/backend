#
# this class is used to handle sales discount redemptions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Discounts
          class DiscountRedemptions < Base
            
            self.context_resource_class= '::Main::SalesManagement::Models::DiscountRedemption'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::DiscountRedemption"

            resources "merchants/sales_management/discount_redemptions" do 

              desc "[GET] index all merchants generated discount redemptions"
              get do
                discount_redemptions= resource_class_constant.filter(filter_params.merge!(redeemable_type: 'transaction'))
                presenter paginate(discount_redemptions)
              end

              desc "[GET] show a discount_redemption"
              get ":id", requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

            end
          end
        end
      end
    end
  end
end