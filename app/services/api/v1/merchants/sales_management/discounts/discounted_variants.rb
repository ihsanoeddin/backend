#
# this class is used to handle sales variants discounts
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Discounts
          class DiscountedVariants < Base
            
            self.context_resource_class= '::Main::SalesManagement::Models::VariantDiscount'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::VariantDiscount"

            helpers do 
              
              def variant_discount_params
                posts[:variant_discount]||= posts
                posts.require(:variant_discount).permit(:discount_id, :variant_id)
              end

              def discounted_variants_params
                posts[:discounted_variants]= posts
                posts.require(:discounted_variants).map do |p|
                 ActionController::Parameters.new(p.to_hash).permit(:discount_id, :variant_id, :id, :_destroy)
               end
              end

            end

            resources "merchants/sales_management/discounted_variants" do 

              desc "[GET] index all merchants created discounted variants"
              get do
                discounted_variants= resource_class_constant.filter(filter_params)
                presenter paginate(discounted_variants)
              end

              desc "[GET] show a discounted variant"
              get ":id", requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a discounted variant"
              post do 
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a discounted variant"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[POST] batch discounted variants"
              post "batch" do 
                results= resource_class_constant.batch(batch: discounted_variants_params, base_query: resource_class_constant.cached_collection)
                if not results.error_state
                   presenter results.batch, root: "discounted_variants"
                else
                  standard_validation_error details: results.errors
                end
              end              

            end
          end
        end
      end
    end
  end
end