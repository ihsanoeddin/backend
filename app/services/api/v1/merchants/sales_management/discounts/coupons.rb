#
# this class is used to handle sales discounts class
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Discounts
          class Coupons < Base
            
            self.context_resource_class= '::Main::SalesManagement::Models::Discount'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::Discount"

            helpers do 
              
              def discount_params
                posts[:discount]||= posts
                posts.require(:discount).permit(:name, :discount_type, :store_id, :code, :description, :valid_from, :valid_until, :store_id,
                                                :redemption_limit, :amount, :based_on, :discount_orientation, :discount_orientation_amount,
                                                :variant_ids => [])
              end

              def assign_items_params
                posts[:assign_items]= posts
                posts.require(:assign_items).permit(product_type_ids: [], product_ids: [], variant_ids: [])
              end

              def determine_discount_type_object(discount_type=nil, &block)
                discount_type||= posts[:discount_type]
                case discount_type
                when "transaction"
                  context_resource.becomes! ::Main::SalesManagement::Models::DiscountTypes::Transaction
                else
                  context_resource.becomes! ::Main::SalesManagement::Models::DiscountTypes::Item
                end
                yield
              end

              def resource_class_constant
                discount_type||= posts[:discount_type] if posts[:id].blank?
                case discount_type
                when "transaction"
                  ::Main::SalesManagement::Models::DiscountTypes::Transaction
                when 'item'
                  ::Main::SalesManagement::Models::DiscountTypes::Item
                else
                  ::Main::SalesManagement::Models::Discount
                end
              end

              def valid_discount_to_update &block
                if context_resource.started?
                  standard_validation_error options= {message: { started_at: ["Can not update discount if discount has already started"] }}
                else
                  yield
                end
              end

            end

            resources "merchants/sales_management/discounts" do 

              desc "[GET] index all merchants created discounts"
              get do
                discounts= resource_class_constant.filter(filter_params)
                presenter paginate(discounts)
              end

              desc "[GET] show a discount"
              get ":id", requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a discount"
              post do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] update a discount"
              put ":id", requirements: { id: /[0-9]*/ } do
                valid_discount_to_update do
                  if context_resource.update discount_params
                    presenter context_resource
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[PUT] expire a discount"
              put ":id/expire", requirements: { id: /[0-9]*/ } do 
                if context_resource.update(valid_until: Date.today)
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              #desc "[PUT] assign items to discount"
              #put ":id/items/assign", requirements: { id: /[0-9]*/ } do
              #  if context_resource.item_type
              #    context_resource.assign_items(assign_items_params)
              #    presenter context_resource
              #  else
              #
              #  end
              #end

              desc "[DELETE] update a discount"
              delete ":id", requirements: { id: /[0-9]*/ } do
                valid_discount_to_update do
                  if context_resource.update discount_params
                    presenter context_resource
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

            end            

          end
        end
      end
    end
  end
end