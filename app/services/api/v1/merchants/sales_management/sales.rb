#
# this class is used to handle sales base process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        class Sales < Base
          
          self.context_resource_class= '::Main::SalesManagement::Models::Sale'
          use_resource!

          self.presenter_name= "Merchants::SalesManagement::Sale"

          helpers do 
            
            def sale_params
              posts[:sale]||= posts
              posts.require(:sale).permit(:name, :started_at, :store_id)
            end

          end

          resources "merchants/sales_management/sales" do 

            desc "[GET] index all merchants sales"
            get do
              sales= resource_class_constant.filter(filter_params)
              presenter paginate(sales)
            end

            desc "[GET] show a sale"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a sale"
            post do
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a sale"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update sale_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] completed a sale"
            put ":id/complete", requirements: { id: /[0-9]*/ } do 
              if context_resource.complete!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a sale"
            delete ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end