#
# this class is used to handle sales transactions
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Transactions
          class Transaksi < ::Api::V1::Merchants::SalesManagement::Base

            self.context_resource_class= '::Main::SalesManagement::Models::Transaction'
            use_resource!

            self.presenter_name= "Merchants::SalesManagement::Transaction"

            helpers do 
              
              def transaction_params
                posts[:transaction]||= posts
                posts.require(:transaction).permit(:store_id, :sale_id, :customer_id, :notes, :number, :discount_ids, :exclude_ppn, :exclude_tax,
                                                    payments_attributes: [:id, :_destroy, :amount, :payment_type, :name, :details => [:name, :provider_name, :reference_code]],
                                                    details_attributes: [:id, :name, :_destroy, :variant_id, :quantity, :notes, :amount])
              end

            end

            #
            # get all merchant's transactions
            #
            resources "merchants/sales_management/transactions" do
              
              desc "[GET] index all merchants transactions"
              get do 
                transactions= resource_class_constant.filter(filter_params)
                presenter paginate(transactions)
              end

            end

            resources "merchants/sales_management/sales/:sale_id/transactions" do 

              desc "[GET] index all merchants transactions based on sales"
              get do
                transactions= resource_class_constant.filter(filter_params.merge!(sale_id: sale.id))
                presenter paginate(transactions)
              end

              desc "[GET] show a transaction"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a transaction"
              post do
                authorize_sale! do 
                  context_resource.sale_id= sale.id
                  if context_resource.save
                    presenter context_resource
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[PUT] update a transaction"
              put ":id", requirements: { id: /[0-9]*/ } do 
                authorize_transaction!(posts[:id]) do 
                  if context_resource.update transaction_params
                    presenter context_resource 
                  else
                    standard_validation_error(details: context_resource.errors)
                  end
                end
              end

              desc "[PUT] pay a transaction"
              put ":id/pay", requirements: { id: /[0-9]*/ } do 
                if context_resource.pay!
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] cancel a transaction"
              put ":id/cancel", requirements: { id: /[0-9]*/ } do 
                if context_resource.cancel!
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] completed a transaction"
              put ":id/complete", requirements: { id: /[0-9]*/ } do 
                if context_resource.complete!
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end

          end
        end
      end
    end
  end
end