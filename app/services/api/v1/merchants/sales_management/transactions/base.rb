#
# this class is used to handle sales transaction base class
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module SalesManagement
        module Transactions
          class Base < ::Api::V1::Merchants::SalesManagement::Base

            helpers do 

              #
              # get sale object
              #
              def sale(id=nil, silent=false)
                @sale ||= ::Main::SalesManagement::Models::Sale.fetch(id || posts[:sale_id])
                raise ::ActiveRecord::RecordNotFound if @sale.nil? and !silent
                @sale
              end

              def authorize_sale! id=nil, &block
                if sale(id).try(:started?)
                  yield
                else
                  standard_permission_denied_error
                end
              end

              #
              # get transaction object
              #
              def transaction(id= nil, silent=false)
                @transaction ||= ::Main::SalesManagement::Models::Transaction.fetch(id || posts[:transaction_id])
                raise ::ActiveRecord::RecordNotFound if @transaction.nil? and !silent
                @transaction
              end

              def authorize_transaction!(id=nil, &block)
                authorize_sale! do 
                  if transaction(id).try(:in_progress?)
                    yield
                  else
                    standard_permission_denied_error
                  end
                end
              end

            end
            
            mount Transaksi
            mount TransactionDetails
            mount Payments

          end
        end
      end
    end
  end
end