#
# this class is used to handle product management products
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module InventoryManagement
        class StockAdjustments < Base
          
          self.context_resource_class= '::Main::InventoryManagement::Models::StockAdjustment'
          use_resource!

          self.presenter_name= "Merchants::InventoryManagement::StockAdjustment"

          helpers do 

            def stock_adjustment_params
              posts[:stock_adjustment]||= posts
              posts.require(:stock_adjustment).permit(:count, :on, :store_id, :variant_id, :stock_id, :notes, :stock_type)
            end

          end

          resources "merchants/inventory_management/stock_adjustments" do 

            desc "[GET] index all stock adjustment"
            get do
              stock_adjustments= resource_class_constant.filter(filter_params)
              presenter paginate(stock_adjustments)
            end

            desc "[GET] show a stock_adjustment"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create a stock adjustment"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] publish a stock adjustment"
            put ":id/publish" do 
              if context_resource.publish!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end