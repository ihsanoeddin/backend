#
# this class is used to handle stock adjustments
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module InventoryManagement
        class Base < ::Api::V1::Merchants::Base
          
          mount StockAdjustments
          mount SaleStocks
          mount RejectedStocks
          mount Variants

        end
      end
    end
  end
end