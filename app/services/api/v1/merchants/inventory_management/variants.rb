#
# this class is used to handle product management products
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module InventoryManagement
        class Variants < Base
          
          self.context_resource_class= '::Main::ProductsManagement::Models::Variant'
          use_resource!

          self.presenter_name= "Merchants::InventoryManagement::Variant"

          resources "merchants/inventory_management/variants" do 

            desc "[GET] index all stock of all variants"
            get do
              variants= resource_class_constant.filter(filter_params)
              presenter paginate(variants)
            end

            desc "[GET] show a variant details"
            get ':id' do 
              presenter context_resource
            end

          end

        end
      end
    end
  end
end