#
# this class is used to handle product management products
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module InventoryManagement
        class RejectedStocks < Base
          
          self.context_resource_class= '::Main::InventoryManagement::Models::Stock'
          use_resource!

          self.presenter_name= "Merchants::InventoryManagement::StockStoreProduct"

          resources "merchants/inventory_management/rejected_stocks" do 

            desc "[GET] index all stock"
            get do
              stocks= resource_class_constant.filter(filter_params.merge!(type: "Main::InventoryManagement::Models::Types::RejectedStock"))
              presenter paginate(stocks)
            end

            desc "[GET] show a stock"
            get ':id' do 
              presenter context_resource
            end

          end

        end
      end
    end
  end
end