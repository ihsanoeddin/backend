#
# this class is used to operate return management function process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ReturnsManagement
        class Returns < Base
          
          self.context_resource_class= '::Main::ReturnsManagement::Models::Return'
          use_resource!

          self.presenter_name= "Merchants::ReturnsManagement::Return"

          helpers do 

            def return_params
              posts[:return]||= posts
              posts.require(:return).permit( :store_id, :transaction_id, :variant_id, :quantity, :amount, :exchange_variant_id, :exchange_quantity, :exchange_amount, :notes )
            end

          end

          resources "merchants/returns_management/returns" do 

            desc "[GET] index all returns"
            get do
              returns= resource_class_constant.filter(filter_params)
              presenter paginate(returns)
            end

            desc "[GET] show a return"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a return"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a return"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update return_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a return"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

            desc "[PUT] reject a return"
            put ":id/reject", requirements: { id: /[0-9]*/ } do 
              if context_resource.reject!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a return"
            delete ":id/accept", requirements: { id: /[0-9]*/ } do 
              if context_resource.accept!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end