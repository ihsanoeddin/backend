#
# this class is used to operate voids management function process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ReturnsManagement
        class Voids < Base
          
          self.context_resource_class= '::Main::ReturnsManagement::Models::Void'
          use_resource!

          self.presenter_name= "Merchants::ReturnsManagement::Void"

          helpers do 

            def void_params
              posts[:refund]||= posts
              posts.require(:refund).permit( :store_id, :transaction_id, :amount, :notes )
            end

          end

          resources "merchants/returns_management/voids" do 

            desc "[GET] index all voids"
            get do
              voids= resource_class_constant.filter(filter_params)
              presenter paginate(voids)
            end

            desc "[GET] show a void"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[POST] create a void"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a void"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.update void_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a void"
            put ":id", requirements: { id: /[0-9]*/ } do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

            desc "[PUT] reject a void"
            put ":id/reject", requirements: { id: /[0-9]*/ } do 
              if context_resource.reject!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] accept a void"
            delete ":id/accept", requirements: { id: /[0-9]*/ } do 
              if context_resource.accept!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end