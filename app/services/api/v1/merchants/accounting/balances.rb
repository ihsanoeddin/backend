#
# this class is to handle accounting account
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module Accounting
        class Balances < Base
          
          self.context_resource_class= '::Main::Accounting::Models::Balance'
          use_resource!

          self.presenter_name= "Merchants::Accounting::Balance"
          helpers do 

            def balance_params
              posts[:balance]||= posts
              posts.require(:balance).permit(:amount, :balance_type, :notes, :store_id, :time, :account_id, :journal_identifing_name, :account_identifing_name, :journal_ids=> [])
            end

            def journal
              @journal||= ::Main::Accounting::Models::Journal.fetch(params[:journal_id])
            end

          end

          resources "merchants/accounting/journals/:journal_id/balances" do 

            desc "[GET] index all balances"
            get do
              balances= resource_class_constant.filter(filter_params.merge!(journal_id: journal.id))
              presenter paginate(balances)
            end

            desc "[GET] show an balance"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create an balance"
            post do
              context_resource.journal_ids= [journal.id]
              if context_resource.save
                #journal.balances << context_resource
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update an balance"
            put ":id" do 
              if context_resource.update balance_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete an balance"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end            

          end

        end
      end
    end
  end
end