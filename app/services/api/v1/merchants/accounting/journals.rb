#
# this class is to handle accounting account
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module Accounting
        class Journals < Base
          
          self.context_resource_class= '::Main::Accounting::Models::Journal'
          use_resource!

          self.presenter_name= "Merchants::Accounting::Journal"
          helpers do 

            def journal_params
              posts[:journal]||= posts
              posts.require(:journal).permit(:name, :description)
            end

          end

          resources "merchants/accounting/journals" do 

            desc "[GET] index all journals"
            get do
              journals= resource_class_constant.filter(filter_params)
              presenter paginate(journals)
            end

            desc "[GET] show a journal"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create a journal"
            post do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a journal"
            put ":id" do 
              if context_resource.update journal_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a journal"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end            

          end

        end
      end
    end
  end
end