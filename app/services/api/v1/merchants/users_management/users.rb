#
# this class is used to handle users management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module UsersManagement
        class Users < Base
          
          self.context_resource_class= '::Main::UsersManagement::Models::User'
          use_resource!

          self.presenter_name= "Merchants::UsersManagement::User"

          #
          # change upload attributes to uploaded class
          #
          before do 
            if params[:avatar_attributes].present? && params[:avatar_attributes][:attachment].present?
              avatar_params = params[:avatar_attributes][:attachment]
              if avatar_params.respond_to?(:keys) && (avatar_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                params[:avatar_attributes][:attachment]= ActionDispatch::Http::UploadedFile.new(avatar_params)
              end
            end
          end

          helpers do 
            
            def user_params
              posts[:user]||= posts
              posts.require(:user).permit(:username, :email, :password, :password_confirmation, :role_ids => [], 
                                          avatar_attributes: [:_destroy, :id, :attachment], 
                                          roles_attributes: [:name, :resource_type, :resource_id])
            end

            def role_params
              posts[:roles]||= posts.delete(:roles)
              posts.require(:roles).permit(:name, :store_id)
            end

          end

          resources "merchants/users_management/users" do 

            desc "[GET] index all merchants users"
            get do
              users= resource_class_constant.filter(filter_params)
              presenter paginate(users)
            end

            desc "[GET] get new user data requirement"
            get 'new' do 
              presenter context_resource, root: "resource", 
                                          parameters: { 
                                                        roles: direct_present(Main::UsersManagement::Models::Role.cached_collection.reject{|role| role.resource_id.blank? }, with: ::Api::V1::Presenters::Merchants::UsersManagement::Role), 
                                                        accesses: direct_present(Main::UsersManagement::Models::Access.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::UsersManagement::Access)
                                                      }
            end

            desc "[POST] create a user"
            post do
              if context_resource.save
                context_resource.confirm
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[GET] get existing user"
            get ':id', requirements: { id: /[0-9]*/ } do 
              presenter context_resource
            end

            desc "[GET] get edit existing user data requirement"
            get ':id/edit' do 
              presenter context_resource, root: "resource", 
                                          parameters: { 
                                                        roles: direct_present(Main::UsersManagement::Models::Role.cached_collection.reject{|role| role.resource_id.blank? }, with: ::Api::V1::Presenters::Merchants::UsersManagement::Role), 
                                                        accesses: direct_present(Main::UsersManagement::Models::Access.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::UsersManagement::Access)
                                                      }
            end

            desc "[PUT] update a user"
            put ":id", requirements: { id: /[0-9]*/ } do
              if context_resource.update_without_password user_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a user"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end