#
# this class is used to handle accesses management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module UsersManagement
        class Accesses < Base
          
          self.context_resource_class= "::Main::UsersManagement::Models::Access"
          use_resource!

          self.presenter_name= "Merchants::UsersManagement::Access"

          resources "merchants/users_management/accesses" do 

            desc "[GET] index all merchants accesses"
            get do
              accesses= resource_class_constant.actives.filter(filter_params)
              presenter paginate(accesses)
            end

          end

        end
      end
    end
  end
end