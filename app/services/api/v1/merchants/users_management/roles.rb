#
# this class is used to handle roles management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module UsersManagement
        class Roles < Base
          
          self.context_resource_class= '::Main::UsersManagement::Models::Role'
          use_resource!

          self.presenter_name= "Merchants::UsersManagement::Role"

          helpers do 
            
            def role_params
              posts[:role]||= posts
              posts.require(:role).permit(:name, :resource_type, :resource_id, :user_ids => [], :access_ids => [])
            end

          end

          resources "merchants/users_management/roles" do 

            desc "[GET] index all merchants roles"
            get do
              roles= resource_class_constant.where.not(:resource_type => nil, resource_id: nil).search(filter_params[:common])
              presenter paginate(roles)
            end

            desc "[GET] get new role data requirement"
            get 'new' do 
              presenter context_resource, root: "resource", 
                                          parameters: { 
                                                        stores: direct_present(Main::StoresManagement::Models::Store.cached_collection, with: ::Api::V1::Presenters::Merchants::StoresManagement::Shop), 
                                                        accesses: direct_present(Main::UsersManagement::Models::Access.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::UsersManagement::Access)
                                                      }
            end

            desc "[GET] show a role"
            get ':id', requirements: { id: /[0-9]*/ } do
              presenter context_resource
            end

            desc "[POST] create a role"
            post do
              if context_resource.save(context: :merchant)
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[GET] get existing role update data requirement"
            get ':id/edit' do 
              presenter context_resource, root: "resource", 
                                          parameters: { 
                                                        stores: direct_present(Main::StoresManagement::Models::Store.cached_collection, with: ::Api::V1::Presenters::Merchants::StoresManagement::Shop), 
                                                        accesses: direct_present(Main::UsersManagement::Models::Access.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::UsersManagement::Access)
                                                      }
            end

            desc "[PUT] update a role"
            put ":id" do 
              context_resource.attributes= role_params
              if context_resource.save(:context => :merchant)
                context_resource.access_ids= role_params[:access_ids] unless role_params[:access_ids].nil?
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a role"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end