#
# this class is used as base class for users management process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module UsersManagement
        class Base < ::Api::V1::Merchants::Base
          
          #
          #
          #
          mount Users
          mount Roles
          mount Accesses
          
        end
      end
    end
  end
end