#
# this class is used to handle product management product properties
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class EntityProperties < Base
            
            self.context_resource_class= '::Main::ProductsManagement::Models::EntityProperty'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::EntityProperty"

            helpers do 
            
              def entity_property_params
                posts[:entity_property]||= posts
                posts.require(:entity_property).permit(:property_id, :value, :unit_value, :position)
              end

              def batch_entity_properties_params
                posts[:entity_properties]= posts
                posts.require(:entity_properties).map do |p|
                 ActionController::Parameters.new(p.to_hash).permit(:property_id, :value, :unit_value, :position, :id, :_destroy)
               end
              end

              def variant
                @variant||= ::Main::ProductsManagement::Models::Variant.fetch(posts[:variant_id])
              end

              def entity
                @entity||= case posts[:entity_type]
                          when "product"
                            posts[:product_id]= posts[:entity_id]
                            product
                          when "variant"
                            posts[:variant_id]= posts[:entity_id]
                            variant
                          else
                            raise Grape::Exceptions::MethodNotAllowed
                          end
              end

            end

            resources "merchants/products_management/products/:entity_type/:entity_id/entity_properties" do 

              desc "[GET] index all merchants product properties"
              get do
                entity_properties= resource_class_constant.filter(filter_params).where(entity: entity )
                presenter paginate(entity_properties)
              end

              desc "[GET] show a entity property"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[GET] get requirement data for a new entity property"
              get 'new' do 
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property)                                                          
                                                        }
              end

              desc "[POST] create a entity property"
              post do
                context_resource.entity= entity
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] get requirement data for an update entity property"
              get ':id/edit' do 
                presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property)                                                          
                                                        }
              end

              desc "[PUT] update a entity property"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update property_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a entity property"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[POST] batch entity properties"
              post "batch" do 
                results= resource_class_constant.batch(batch: batch_entity_properties_params, base_query: resource_class_constant.where(entity: entity))
                if not results.error_state
                   presenter results.batch, root: "entity_properties"
                else
                  standard_validation_error details: results.errors
                end
              end

            end

          end
        end
      end
    end
  end
end