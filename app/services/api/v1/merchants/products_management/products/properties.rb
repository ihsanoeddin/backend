#
# this class is used to handle product management properties
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class Properties < Base
            
            self.context_resource_class= '::Main::ProductsManagement::Models::Property'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::Property"

            helpers do 
            
              def property_params
                posts[:property]||= posts
                posts.require(:property).permit(:identifing_name, :display_name, :active, units: [])
              end

              def skip_authorization;Rails.env.development?;end

            end

            resources "merchants/products_management/products/properties" do 

              desc "[GET] index all merchants properties"
              get do
                properties= resource_class_constant.filter(filter_params)
                presenter paginate(properties)
              end

              desc "[GET] show a property"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a property"
              post do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] update a property"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update property_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a property"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end

          end
        end
      end
    end
  end
end