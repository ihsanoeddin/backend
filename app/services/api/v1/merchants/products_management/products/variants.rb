#
# this class is used to handle product management product items
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class Variants < Base
            
            self.context_resource_class= '::Main::ProductsManagement::Models::Variant'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::Variant"

            helpers do 
            
              def variant_params
                posts[:variant]||= posts
                posts.require(:variant).permit( :id, :_destroy, :image_group_id, :name, :sku, :cost, :price, :permalink, :keywords, :bundled, :short_description, :master, :featured, :active,
                                                :entity_properties_attributes => [:id ,:_destroy, :value, :unit_value, :position], variant_bundles_ids: [])
              end

              def batch_variants_params
                posts[:variants]= posts
                posts.require(:variants).map do |p|
                 ActionController::Parameters.new(p.to_hash).permit(:id, :_destroy, :image_group_id, :name, :bundled, :sku, :cost, :price, :permalink, :keywords, :short_description, :master, :featured, :active,
                                                                    :entity_properties_attributes => [:id ,:_destroy, :value, :unit_value, :position], variant_bundles_ids: [])
               end
              end

              def skip_authorization;Rails.env.development?;end

            end

            resources "merchants/products_management/products/variants" do 
              
              desc "[GET] index all merchants products variants"
              get do
                variants= resource_class_constant.filter(filter_params)
                presenter paginate(variants)
              end

                            desc "[GET] new variant requirements data"
              get 'new' do 
                 presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property),
                                                          image_groups: direct_present(product.fetch_image_groups, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ImageGroup)                                                          
                                                        }
              end              

              desc "[GET] show a variant"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a variant"
              post do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] edit variant requirements data"
              get ':id/edit', requirements: { id: /[0-9]*/ } do 
                 presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property),
                                                          image_groups: direct_present(product.fetch_image_groups, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ImageGroup)                                                          
                                                        }
              end

              desc "[PUT] update a product"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update variant_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a variant"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

            end

            resources "merchants/products_management/products/:product_id/variants" do 

              desc "[GET] index all merchants products variants"
              get do
                variants= resource_class_constant.filter(filter_params).where(product_id: product.id)
                presenter paginate(variants)
              end

              desc "[GET] new variant requirements data"
              get 'new' do 
                 presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property),
                                                          image_groups: direct_present(product.fetch_image_groups, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ImageGroup)                                                          
                                                        }
              end              

              desc "[GET] show a variant"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a variant"
              post do
                context_resource.product= product
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[GET] new variant requirements data"
              get ':id/edit', requirements: { id: /[0-9]*/ } do 
                 presenter context_resource, root: "resource",
                                            parameters: { 
                                                          properties: direct_present(::Main::ProductsManagement::Models::Property.cached_collection.actives, with: ::Api::V1::Presenters::Merchants::ProductsManagement::Property),
                                                          image_groups: direct_present(product.fetch_image_groups, with: ::Api::V1::Presenters::Merchants::ProductsManagement::ImageGroup)                                                          
                                                        }
              end

              desc "[PUT] update a product"
              put ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.update variant_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[DELETE] delete a variant"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[POST] batch variants"
              post "batch" do 
                results= resource_class_constant.batch(batch: variants_params, base_query: resource_class_constant.cached_collection.where(product_id: product.id))
                if not results.error_state
                   presenter results.batch, root: "variants"
                else
                  standard_validation_error details: results.errors
                end
              end

            end

          end
        end
      end
    end
  end
end