#
# this class is used to handle product management product items
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class Base < ::Api::V1::Merchants::ProductsManagement::Base
            
            helpers do 

              def product
                @product ||= ::Main::ProductsManagement::Models::Product.fetch(posts[:product_id])
                raise ::ActiveRecord::RecordNotFound if @product.nil?
                @product
              end

            end


            mount Items
            mount Services
            mount ImageGroups
            mount EntityProperties
            mount Properties
            mount Variants
            mount Images

          end
        end
      end
    end
  end
end