#
# this class is used to handle product management product images
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        module Products
          class Images < Base
            
            self.context_resource_class= '::Supports::Imageable::Models::Image'
            use_resource!

            self.presenter_name= "Merchants::ProductsManagement::Image"

            #
            # change upload attributes to uploaded class
            #
            before do 
              begin 
                att_params = params[:attachment]
                if att_params.respond_to?(:keys) && (att_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                  params[:attachment]= ActionDispatch::Http::UploadedFile.new(att_params)
                end

                if params[:images].is_a?(Array) or params[:images].is_a?(Hashie::Mash)
                  params[:images].each_with_index do |img_params, index|
                    att_params= img_params[:attachment]
                    if att_params.respond_to?(:keys) && (att_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                      params[:images][index][:attachment]= ActionDispatch::Http::UploadedFile.new(att_params)
                    end                    
                  end
                end
              rescue => e 
                Rails.logger.info(e.message)
                Rails.logger.info(e.backtrace)
              end
            end

            helpers do 
              
              def image_params
                posts[:image]||= posts
                posts.require(:image).permit(:attachment, :remote_attachment_url)
              end

              def images_params
                posts[:images_params]||= posts
                posts.require(:images).map do |p|
                 ActionController::Parameters.new(p.to_hash).permit(:id, :_destroy, :attachment, :remote_attachment_url)
               end
              end

              def image_group
                @image_group||= ::Supports::Imageable::Models::ImageGroup.fetch(params[:image_group_id])
                raise ::ActiveRecord::RecordNotFound unless @image_group
                @image_group
              end

              def imageable
                @imageable||= case posts[:imageable_type]
                                when "product"
                                  params[:product_id]= posts[:imageable_id]
                                  product
                                when "image_group"
                                  params[:image_group_id]= posts[:imageable_id]
                                  image_group
                                else
                                  raise Grape::Exceptions::MethodNotAllowed
                                end
              end

              def skip_authorization;Rails.env.development?;end

            end

            #
            # unscoped
            #
            resources "merchants/products_management/products/images" do

              desc "[GET] index all merchants image group images"
              get do
                images= resource_class_constant.filter(filter_params)
                presenter paginate(images)
              end

              #
              # @Override
              #
              def resource_class_query

              end              

            end
            #
            # scoped by imageable object
            #
            resources "merchants/products_management/products/:imageable_type/:imageable_id/images" do 

              desc "[GET] index all merchants image group images"
              get do
                filter_params["#{posts[:imageable_type].to_s}_id".to_sym]= imageable.id
                images= resource_class_constant.filter(filter_params)
                presenter paginate(images)
              end

              desc "[GET] show a image"
              get ':id', requirements: { id: /[0-9]*/ } do 
                presenter context_resource
              end

              desc "[POST] create a image"
              post do
                context_resource.imageable= imageable
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[PUT] update a image"
              put ":id", requirements: { id: /[0-9]*/ } do
                if context_resource.update image_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end


              desc "[DELETE] delete a image"
              delete ":id", requirements: { id: /[0-9]*/ } do 
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end

              desc "[POST] batch images"
              post "batch" do 
                results= resource_class_constant.batch(batch: images_params, base_query: resource_class_constant.cached_colletion.where(imageable_id: imageable.id, imageable_type: imageable.class.name))
                if not results.error_state
                   presenter results.batch, root: "images"
                else
                  standard_validation_error details: results.errors
                end
              end

            end

          end
        end
      end
    end
  end
end