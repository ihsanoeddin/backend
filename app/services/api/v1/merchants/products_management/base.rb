
module Api
  module V1
    module Merchants
      module ProductsManagement
        class Base < ::Api::V1::Merchants::Base
          
          mount ProductTypes
          mount TaxRates
          mount ::Api::V1::Merchants::ProductsManagement::Products::Base
          mount Imports

        end
      end
    end
  end
end