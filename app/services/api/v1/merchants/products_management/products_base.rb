#
# this class is used to handle product management products
# TODO : change all message text to Int.
#
module Api
  module V1
    module Merchants
      module ProductsManagement
        class ProductsBase < Base
          
          self.context_resource_class= '::Main::ProductsManagement::Models::Product'
          use_resource!

          self.presenter_name= "Merchants::ProductsManagement::Product"

          helpers do 
            
            def product_params
              posts[:product]||= posts
              posts.require(:product).permit(:name, :permalink, :keywords, :brand, :cost, 
                                                  :short_description, :price, :description, :featured, :product_type_id, :tax_rate_id,
                                                  :images_attributes => [:id, :attachment, :_destroy],
                                                  :variants_attributes => [ :id, :_destroy, :name, :sku, :cost, :price, :permalink, :keywords, :description, :master, :featured ])
            end

            def skip_authorization;true;end

          end

          resources "merchants/products_management/products/#{self.class.name.demodulize.underscore.pluralize}" do 

            desc "[GET] index all merchants products"
            get do
              products= resource_class_constant.filter(filter_params)
              presenter paginate(products)
            end

            desc "[GET] show a product"
            get ':id' do 
              presenter context_resource
            end

            desc "[POST] create a product"
            post do
              if context_resource.save
                context_resource.activate!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a product"
            put ":id" do 
              if context_resource.update product_type_params
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] activate a product"
            put ':id/activate' do 
              context_resource.activate!
              presenter context_resource
            end

            desc "[PUT] deactivate a product"
            put ':id/deactivate' do
              context_resource.deactivate!
              presenter context_resource
            end

            desc "[DELETE] delete a product"
            delete ":id" do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end