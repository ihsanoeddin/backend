module Api
  module V1
    class Base < Api::Base
      version "v1", using: :path

      include Resources

      #
      # protect endpoints with doorkeeper
      #
      before do
        doorkeeper_authorize! unless skip_authorization
      end

      helpers do 

        #
        # override cancan-grape current ability method
        #
        def current_ability
          Api::V1::Ability.new(current_user)
        end

        def switch_tenant(tenant_name= params[:tenant_name])
          ::Apartment::Tenant.switch!(tenant_name)
        end

        def skip_authorization;end

      end

      def current_ability
        Api::V1::Ability.new(current_user)
      end

      authorize_routes!

      mount Register::Base
      mount Merchants::Base
      mount Stores::Base
      mount CurrentUser
      mount Domains
      mount Sync::Base

    end
  end
end