#
# current company operations
#

module Api
  module V1
    class CurrentCompany < Api::V1::Base

      use_resource!
      self.presenter_name= "Merchants::CurrentCompany"

      #
      # change upload attributes to uploaded class
      #
      before do 
        if params[:logo].present?
          logo_params = params[:logo]
          if logo_params.respond_to?(:keys) && (logo_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
            params[:logo]= ActionDispatch::Http::UploadedFile.new(logo_params)
          end
        end
      end

      helpers do 
        
        def current_company_params
          posts[:current_company]||= posts
          posts.require(:current_company).permit(:name, :business_registration_number, :gst_registration_number, 
                                                 :email, :website, :phone_number, :currency, :time_zone, :owner_name,
                                                 :logo, :remote_logo_url,
                                                 :setting_attributes => [ :name, :options => [ :product_code => [:suffix, :prefix] ] ],
                                                 :addresses_attributes => [:address, :city, :province, :country, :latitude, :longitude])
        end

        def context_resource
          @context_resource||= ::Tenants::Merchants::Models::Company.fetch_by_domain(::Apartment::Tenant.current.underscore) 
        end

      end

      resources "current_company" do 

        desc "[GET] show current merchant company"
        get do 
          presenter context_resource
        end

        desc "[PUT] update current merchant company"
        put do
          if context_resource.update(current_company_params)
            presenter context_resource
          else
            standard_validation_error(details: context_resource.errors)
          end
        end

      end

    end
  end
end