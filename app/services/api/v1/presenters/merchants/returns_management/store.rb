module Api
  module V1
    module Presenters
      module Merchants
        module ReturnsManagement
          class Store < ::Grape::Entity
              expose :id
              expose :name
              expose :deleted_at
              expose :created_at
              expose :updated_at
          end
        end
      end
    end
  end
end