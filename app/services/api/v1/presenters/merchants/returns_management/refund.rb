module Api
  module V1
    module Presenters
      module Merchants
        module ReturnsManagement
          class Refund < ::Grape::Entity
            
            expose :id
            expose :state
            expose :transaction_id
            expose :transaction, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Transaction" do |refund, options|
              refund.fetch_transaksi || refund.transaksi
            end
            expose :store_id
            expose :store, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Store" do |refund, options|
              refund.fetch_store || refund.store
            end
            
            expose :variant_id
            expose :variant, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Variant" do |refund, options|
              refund.fetch_variant || refund.variant
            end
            expose :amount
            expose :quantity

            expose :notes
            expose :time
            expose :state

            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end