module Api
  module V1
    module Presenters
      module Merchants
        module ReturnsManagement
          class Return < ::Grape::Entity
            
            expose :id
            expose :state
            expose :transaction_id
            expose :transaction, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Transaction" do |retur, options|
              retur.fetch_transaksi || retur.transaksi
            end
            expose :store_id
            expose :store, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Store" do |retur, options|
              retur.fetch_store || retur.store
            end
            
            expose :variant_id
            expose :variant, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Variant" do |retur, options|
              retur.fetch_variant || retur.variant
            end
            expose :amount
            expose :quantity

            expose :exchange_variant_id
            expose :exchange_variant, with: "::Api::V1::Presenters::Merchants::ReturnsManagement::Variant" do |retur, options|
              retur.fetch_exchange_variant || retur.exchange_variant
            end
            expose :exchange_quantity
            expose :exchange_amount

            expose :notes
            expose :time
            expose :state

            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end