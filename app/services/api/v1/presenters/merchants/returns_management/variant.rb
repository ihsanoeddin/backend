module Api
  module V1
    module Presenters
      module Merchants
        module ReturnsManagement
          class Variant < ::Grape::Entity
              expose :id
              expose :product_id
              expose :sku
              expose :name
              expose :image_urls_thumbnail do |variant, options|
                variant.image_urls
              end
              expose :image_urls_medium do |variant, options|
                variant.image_urls :medium
              end 
              expose :deleted_at
              expose :created_at
              expose :updated_at
          end
        end
      end
    end
  end
end