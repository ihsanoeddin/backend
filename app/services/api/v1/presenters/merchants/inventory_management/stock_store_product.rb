module Api
  module V1
    module Presenters
      module Merchants
        module InventoryManagement
          class StockStoreProduct < ::Grape::Entity
              expose :id
              expose :variant, using: "Api::V1::Presenters::Merchants::InventoryManagement::SimpleVariant" do |stock, options|
                stock.fetch_variant
              end
              expose :quantity_available
              expose :low_stock do |stock, options|
                stock.low?
              end
              expose :out_of_stock do |stock, options|
                stock.out?
              end
              expose :type do |stock, options|
                stock.type.to_s.demodulize.underscore
              end
              expose :count_on_store
              expose :count_pending_to_customer
              expose :count_pending_from_supplier
              expose :store, using: "::Api::V1::Presenters::Merchants::InventoryManagement::Store" do |stock, options|
                stock.fetch_store
              end
          end
        end
      end
    end
  end
end