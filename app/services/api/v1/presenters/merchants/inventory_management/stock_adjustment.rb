module Api
  module V1
    module Presenters
      module Merchants
        module InventoryManagement
          class StockAdjustment < ::Grape::Entity
              expose :id
              expose :stock, using: "::Api::V1::Presenters::Merchants::InventoryManagement::StockStoreProduct" do |stk_adj, options|
                stk_adj.fetch_stock
              end
              expose :state
              expose :stock_type
              expose :on
              expose :notes
              expose :count
              expose :deleted_at
              expose :created_at
              expose :updated_at
          end
        end
      end
    end
  end
end