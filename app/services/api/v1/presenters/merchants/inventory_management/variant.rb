module Api
  module V1
    module Presenters
      module Merchants
        module InventoryManagement
          class Variant < ::Grape::Entity
              expose :id
              expose :product_id
              expose :sku
              expose :name
              expose :permalink
              expose :cost
              expose :price
              expose :price_after_tax
              expose :image_group_id
              expose :state
              expose :available_stocks
              expose :sale_stocks, using: "::Api::V1::Presenters::Merchants::InventoryManagement::Stock" do |variant, options|
                variant.fetch_sale_stocks
              end
              expose :rejected_stocks, using: "::Api::V1::Presenters::Merchants::InventoryManagement::Stock" do |variant, options|
                variant.fetch_rejected_stocks
              end
              expose :image_urls_thumbnail do |variant, options|
                variant.image_urls
              end
              expose :image_urls_medium do |variant, options|
                variant.image_urls :medium
              end 
              expose :bundled
              expose :deleted_at
              expose :created_at
              expose :updated_at
          end
        end
      end
    end
  end
end