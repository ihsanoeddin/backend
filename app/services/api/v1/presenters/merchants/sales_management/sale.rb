module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Sale < Grape::Entity

            expose :id 
            expose :store_id
            expose :store, with: "::Api::V1::Presenters::Merchants::SalesManagement::Store" do |sale, options|
              sale.fetch_store
            end
            expose :fund
            expose :fund_notes
            expose :name
            expose :state
            expose :transactions_count
            expose :started_at
            expose :finished_at
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end
