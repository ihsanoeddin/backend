module Api
  module V1
    module Presenters
      module Merchants
        module SalesManagement
          class Discount < Grape::Entity

            expose :id 
            expose :name
            expose :code
            expose :description
            expose :valid_from
            expose :valid_until
            expose :redemption_limit
            expose :discount_redemptions_count
            expose :based_on
            expose :amount
            expose :discount_orientation
            expose :discount_orientation_amount
            expose :type do |discount, options|
              discount.type.to_s.demodulize.underscore
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end
