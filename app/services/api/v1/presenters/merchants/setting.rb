module Api
  module V1
    module Presenters
      module Merchants
        class Setting < Grape::Entity

          expose :id
          expose :name
          expose :options
          expose :deleted_at
          expose :created_at
          expose :updated_at

        end
      end
    end
  end
end