module Api
  module V1
    module Presenters
      module Merchants
        module ProductsManagement
          class Property < ::Grape::Entity
            
            expose :id
            expose :identifing_name
            expose :display_name
            expose :units
            expose :active
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end
