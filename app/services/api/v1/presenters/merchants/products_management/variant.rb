module Api
  module V1
    module Presenters
      module Merchants
        module ProductsManagement
          class Variant < ::Grape::Entity
            
            expose :id
            expose :product_id
            expose :name
            expose :sku
            expose :permalink
            expose :cost
            expose :price
            expose :price_after_tax
            expose :image_group_id
            expose :state
            expose :keywords
            expose :short_description
            expose :master
            expose :featured
            expose :business_model_id
            expose :business_model_name do |variant, opt|
              variant.fetch_business_model.try(:name)
            end
            expose :entity_properties, using: "::Api::V1::Presenters::Merchants::ProductsManagement::EntityProperty" do |variant, opt|
              if variant.persisted?
                (variant.fetch_entity_properties + variant.fetch_product.fetch_entity_properties).uniq(&:unit_value)
              end
            end
            expose :image_urls_thumbnail do |variant, options|
              variant.image_urls
            end
            expose :image_urls_medium do |variant, options|
              variant.image_urls :medium
            end
            expose :bundled
            expose :bundled_variants, with: self.name do |variant, options|
              if variant.bundled
                variant.fetch_variants_bundles.map{|bund| bund.fetch_variant }
              else
                []
              end
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end