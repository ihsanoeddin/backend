module Api
  module V1
    module Presenters
      module Merchants
        module UsersManagement
          class User < ::Grape::Entity
            
            expose :id
            expose :username
            expose :email
            expose :merchant
            expose :deleted_at
            expose :created_at
            expose :updated_at
            expose :sign_in_count
            expose :current_sign_in_at
            expose :last_sign_in_at
            expose :current_sign_in_ip
            expose :last_sign_in_ip
            expose :roles, with: "::Api::V1::Presenters::Merchants::UsersManagement::Role"
            expose :avatar, with: "::Api::V1::Presenters::Merchants::UsersManagement::Avatar"

          end
        end
      end
    end
  end
end