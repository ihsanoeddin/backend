module Api
  module V1
    module Presenters
      module Merchants
        module UsersManagement
          class Role < ::Grape::Entity
            
            expose :id
            expose :name
            expose :resource_type do |role, options|
              role.resource_type.to_s.demodulize.underscore
            end
            expose :resource_id
            expose :resource_name do |role, options|
              role.resource.try(:name)
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at
            expose :accesses, with: "::Api::V1::Presenters::Merchants::UsersManagement::Access"

          end
        end
      end
    end
  end
end