module Api
  module V1
    module Presenters
      module Merchants
        module UsersManagement
          class Avatar < ::Grape::Entity
            
            expose :id
            expose :attachment_url do |avatar, options|
              "#{ENV["base_url"]}#{avatar.attachment_url}" if avatar.attachment_url.present?
            end
            expose :attachment_thumb_url do |avatar, options|
              "#{ENV["base_url"]}#{avatar.attachment.thumb.url}" if avatar.attachment.thumb.url.present?
            end
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end