module Api
  module V1
    module Presenters
      module Merchants
        class CurrentCompany < Grape::Entity

          expose :id
          expose :name
          expose :business_field_id
          expose :domain
          expose :owner_name
          expose :logo do |comp, options|
            "#{ENV["base_url"]}#{comp.logo.attachment_url}" if comp.logo.url.present?
          end
          expose :short_name
          expose :business_registration_number
          expose :email
          expose :phone_number
          expose :currency
          expose :time_zone
          expose :state
          expose :deleted_at
          expose :created_at
          expose :updated_at
          expose :addresses, using: "::Api::V1::Presenters::Merchants::Address"
          expose :setting, using: "::Api::V1::Presenters::Merchants::Setting"

        end
      end
    end
  end
end