module Api
  module V1
    module Presenters
      module Merchants
        module StoresManagement
          class BusinessModel < ::Grape::Entity
            
            expose :id
            expose :name
            expose :identifing_name
            expose :deleted_at
            expose :created_at
            expose :updated_at

          end
        end
      end
    end
  end
end