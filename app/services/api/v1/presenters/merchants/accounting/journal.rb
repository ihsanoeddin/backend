module Api
  module V1
    module Presenters
      module Merchants
        module Accounting
          class Journal < ::Grape::Entity
              
              expose :id
              expose :name
              expose :identifing_name
              expose :static
              expose :description
              expose :deleted_at
              expose :created_at
              expose :updated_at
          
          end
        end
      end
    end
  end
end