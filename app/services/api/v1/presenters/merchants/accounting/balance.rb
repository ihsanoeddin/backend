module Api
  module V1
    module Presenters
      module Merchants
        module Accounting
          class Balance < ::Grape::Entity
              
              expose :id
              expose :amount
              expose :type do |balance, options|
                balance.type.to_s.demodulize.underscore
              end
              expose :store_id
              expose :notes
              expose :time
              expose :account_id
              expose :account_name
              #expose :journals_list, with: "::Api::V1::Presenters::Merchants::Accounting::Journal"
              expose :group_id
              expose :deleted_at
              expose :created_at
              expose :updated_at
          
          end
        end
      end
    end
  end
end