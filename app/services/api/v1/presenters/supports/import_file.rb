module Api
  module V1
    module Presenters
      module Supports
        class ImportFile < ::Grape::Entity

          expose :id
          expose :attachment_url do |file, options|
            "#{ENV["base_url"]}#{file.attachment_url}" if file.attachment_url.present?
          end
          expose :deleted_at
          expose :created_at
          expose :updated_at

        end
      end
    end
  end
end
