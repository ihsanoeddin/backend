module Api
  module V1
    module Presenters
      module Supports
        class Importer < ::Grape::Entity

          expose :id
          expose :name
          expose :type do |importer, options|
            importer.type.to_s.demodulize.underscore
          end
          expose :state
          expose :results
          expose :started_at
          expose :finished_at
          expose :total_rows
          expose :current_row
          expose :file, with: "::Api::V1::Presenters::Supports::ImportFile" do |importer, options|
            importer.fetch_file
          end
          expose :deleted_at
          expose :created_at
          expose :updated_at

        end
      end
    end
  end
end
