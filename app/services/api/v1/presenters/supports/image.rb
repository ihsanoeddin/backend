module Api
  module V1
    module Presenters
      module Supports
        class Image < ::Grape::Entity

          expose :id
          expose :attachment_url do |image, options|
            "#{ENV["base_url"]}#{image.attachment_url}" if image.attachment_url.present?
          end
          expose :attachment_thumb_url do |image, options|
            "#{ENV["base_url"]}#{image.attachment.thumb.url}" if image.attachment.thumb.url.present?
          end
          expose :imageable_type do |image, options|
            image.imageable_type.to_s.demodulize.underscore
          end
          expose :imageable_id
          expose :deleted_at
          expose :created_at
          expose :updated_at

        end
      end
    end
  end
end
