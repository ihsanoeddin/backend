module Api
  module V1
    module Presenters
      module Syncs
        class Role < ::Api::V1::Presenters::Merchants::UsersManagement::Role

          expose :resource_status do |role, options|
            role.resource_status(options[:last_sync])
          end

        end
      end
    end
  end
end