#
# this class is used to handle purchase order invoices
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Inventory
        class Invoices < Base
          
          self.context_resource_class= '::Main::ProcurementsManagement::Models::Invoice'
          use_resource!

          self.presenter_name= "Stores::Invoice"

          #
          # change upload attributes to uploaded class
          #
          before do 
            begin
              if params[:images_attributes].present? 
                if params[:images_attributes].is_a?(Array) or params[:images_attributes].is_a?(Hashie::Mash)
                  params[:images_attributes].each_with_index do |images_attributes, index|
                    image_params = images_attributes.is_a?(Array) ? images_attributes.last[:attachment] : images_attributes[:attachment]
                    if image_params.respond_to?(:keys) && (image_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
                      params[:images_attributes][index][:attachment]= ActionDispatch::Http::UploadedFile.new(image_params)
                    end
                  end
                end
              end
            rescue => e
              Rails.logger.info(e.message)
              Rails.logger.info(e.backtrace)
            end
          end

          helpers do 

            def invoice_params
              posts[:invoice]||= posts
              posts.require(:invoice).permit(:force_accepted, :store_id, :supplier_delivery_order_number, :date, :notes, :tax_rate, :discount_rate,
                                             :details => [:id, :_destroy, :variant_id, :received_quantity, :expected_quantity, :price, :name, :tax_rate, :discount_rate], 
                                             :images_attributes => [:id, :_destroy, :remote_attachment_url, :attachment ] )
            end

            #
            # filter invoice by purchase order
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if got_resource.purchase_order_id != purchase_order.id
              else
                got_resource.store_id= current_store.id
                got_resource.purchase_order_id= purchase_order.id
              end
              got_resource
            end

          end

          resources "stores/:store_id/inventory/invoices" do 

            desc "[GET] index all stores invoices"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              invoices= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(invoices)
            end
          
          end

          resources "stores/:store_id/inventory/purchase_orders/:purchase_order_id/invoices" do 

            desc "[GET] index all merchants invoices scoped by purchase order"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              invoices= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(invoices)
            end

            desc "[GET] show a invoice"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a invoice"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              authorize_invoice_create(po_id=[posts[:purchase_order_id]]) do
                if context_resource.save
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] update a invoice"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:update, self.context_resource_class.constantize] do
              authorize_invoice_changes(id=posts[:id]) do
                if context_resource.update invoice_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[DELETE] delete a invoice"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do
              authorize_invoice_changes(id=posts[:id]) do
                if context_resource.destroy
                  presenter context_resource
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] accept a invoice"
            put ":id/accept", requirements: { id: /[0-9]*/ }, authorize: [:accept, self.context_resource_class.constantize] do 
              if context_resource.accept!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] reject a invoice"
            put ":id/reject", requirements: { id: /[0-9]*/ }, authorize: [:reject, self.context_resource_class.constantize] do 
              if context_resource.reject!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] pay a invoice"
            put ":id/pay", requirements: { id: /[0-9]*/ }, authorize: [:pay, self.context_resource_class.constantize] do 
              if context_resource.pay!
                presenter context_resource 
              else
                standard_validation_error(details: context_resource.errors)
              end
            end          

          end

        end
      end
    end
  end
end