#
# this class is used as base class for users stores cashiers namespace
#

module Api
  module V1
    module Stores
      module Inventory
        class Base < Api::V1::Stores::Base

          helpers do

            #
            # current inventory operator
            #
            def current_operator
              @current_operator ||= current_user
            end

            #
            # get purchase order
            #
            def purchase_order(id=nil, silent=false)
              @purchase_order ||= ::Main::ProcurementsManagement::Models::PurchaseOrder.fetch(id || posts[:purchase_order_id])
              raise ::ActiveRecord::RecordNotFound if (@purchase_order.nil? || !@purchase_order.store_id.eql?(current_store.id)) and !silent
              @purchase_order
            end 

            def authorize_purchase_order_changes(id=nil, &block)
              if purchase_order(id).try(:draft?)
                yield
              else
                standard_permission_denied_error
              end
            end

            #
            # get invoice
            #
            def invoice(id=nil, silent=false)
              @invoice ||= ::Main::ProcurementsManagement::Models::Invoice.fetch(id || posts[:invoice_id])
              raise ::ActiveRecord::RecordNotFound if (@invoice.nil? || !@invoice.store_id.eql?(current_store.id)) and !silent
              @invoice
            end

            #
            # check whether invoice can be created
            #
            def authorize_invoice_create po_id=nil, &block
              if purchase_order(po_id).try(:received?)
                yield
              else
                standard_permission_denied_error
              end
            end

            #
            # check whether invoice can be changed
            #
            def authorize_invoice_changes(id=nil, &block)
              authorize_purchase_order_changes do 
                if invoice(id).try(:draft?)
                  yield
                else
                  standard_permission_denied_error
                end
              end
            end

          end

          mount Contacts
          mount Suppliers
          mount SuppliersVariants
          mount PurchaseOrders
          mount PurchaseOrderDetails
          mount Invoices
          mount InvoiceDetails
          
        end
      end
    end
  end
end