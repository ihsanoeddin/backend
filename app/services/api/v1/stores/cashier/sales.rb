#
# this class is used to handle sales base process
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Cashier
        class Sales < Base
          
          self.context_resource_class= '::Main::SalesManagement::Models::Sale'
          use_resource!

          self.presenter_name= "Stores::Sale"

          helpers do 
            
            def sale_params
              posts[:sale]||= posts
              posts.require(:sale).permit(:name, :started_at, :fund, :fund_notes)
            end

            #
            # filter with current cashier
            # return 401 if context_resource#operator_id != current_cashier.id
            #
            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error unless got_resource.operator_id != current_cashier.id
              else
                got_resource.store = current_store
              end
              super
            end

          end

          resources "stores/:store_id/cashier/sales" do 

            desc "[GET] index current cashier sales"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              sales= resource_class_constant.filter(filter_params).where(operator_id: current_cashier.id)
              presenter paginate(sales)
            end

            desc "[GET] show a sale"
            get ':id', requirements: { id: /[0-9]*/ }, authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create a sale"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              context_resource.operator= current_cashier
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update a sale"
            put ":id", requirements: { id: /[0-9]*/ }, authorize: [:create, self.context_resource_class.constantize] do 
              authorize_sale!(id=posts[:id]) do 
                if context_resource.update sale_params
                  presenter context_resource 
                else
                  standard_validation_error(details: context_resource.errors)
                end
              end
            end

            desc "[PUT] completed a sale"
            put ":id/complete", requirements: { id: /[0-9]*/ }, authorize: [:complete, self.context_resource_class.constantize] do 
              if context_resource.complete!
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete a sale"
            delete ":id", requirements: { id: /[0-9]*/ }, authorize: [:delete, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

          end

        end
      end
    end
  end
end