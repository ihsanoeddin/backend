#
# this class is to handle accounting account
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Accounting
        class Balances < Base
          
          self.context_resource_class= '::Main::Accounting::Models::Balance'
          use_resource!

          self.presenter_name= "Stores::Balance"
          helpers do 

            def balance_params
              posts[:balance]||= posts
              posts.require(:balance).permit(:amount, :balance_type, :notes, :time, :account_id, :journal_identifing_name, :account_identifing_name, :journal_ids=> [])
            end

            def journal
              @journal||= ::Main::Accounting::Models::Journal.fetch(params[:journal_id])
            end

            def after_get_resource(got_resource)
              if got_resource.persisted?
                standard_not_found_error if (got_resource.store_id != current_store.id) or (!got_resource.journal_ids.include?(journal.id))
              else
                got_resource.store_id= current_store.id
              end
            end

          end

          resources "stores/accounting/balances" do 
            desc "[GET] index all balances"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              balances= resource_class_constant.filter(filter_params.merge!({store_id: current_store.id}))
              presenter paginate(balances)
            end            
          end

          resources "stores/accounting/journals/:journal_id/balances" do 

            desc "[GET] index all balances scoped by journal_id"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              balances= resource_class_constant.filter(filter_params.merge!(journal_id: journal.id, store_id: current_store.id))
              presenter paginate(balances)
            end

            desc "[GET] show an balance"
            get ':id', authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create an balance"
            post '', authorize: [:create, self.context_resource_class.constantize] do
              context_resource.journal_ids= [journal.id]
              if context_resource.save
                #journal.balances << context_resource
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update an balance"
            put ":id", authorize: [:update, self.context_resource_class.constantize] do 
              if context_resource.update balance_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete an balance"
            delete ":id", authorize: [:delete, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end            

          end

        end
      end
    end
  end
end