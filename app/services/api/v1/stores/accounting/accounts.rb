#
# this class is to handle accounting account
# TODO : change all message text to Int.
#
module Api
  module V1
    module Stores
      module Accounting
        class Accounts < Base
          
          self.context_resource_class= '::Main::Accounting::Models::Account'
          use_resource!

          self.presenter_name= "Stores::Account"

          helpers do 

            def account_params
              posts[:account]||= posts
              posts.require(:account).permit(:name, :group_id, :code, :description)
            end

          end

          resources "stores/accounting/accounts" do 

            desc "[GET] index all accounts"
            get '', authorize: [:read, self.context_resource_class.constantize] do
              accounts= resource_class_constant.filter(filter_params)
              presenter paginate(accounts)
            end

            desc "[GET] show an account"
            get ':id', authorize: [:read, self.context_resource_class.constantize] do 
              presenter context_resource
            end

            desc "[POST] create an account"
            post '', authorize: [:create, self.context_resource_class.constantize] do 
              if context_resource.save
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[PUT] update an account"
            put ":id", authorize: [:update, self.context_resource_class.constantize] do 
              if context_resource.update account_params
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end

            desc "[DELETE] delete an account"
            delete ":id", authorize: [:read, self.context_resource_class.constantize] do 
              if context_resource.destroy
                presenter context_resource
              else
                standard_validation_error(details: context_resource.errors)
              end
            end            

          end

        end
      end
    end
  end
end