#
# this class is used to map access for current user
# valid only for api version 1 namespace stores
# initialize on base class for specific namespace if you'd like
# inherit the class on specific namespace if you need specific authorization
#
module Api
  module V1
    module Stores
      class Ability < ::Api::V1::Ability

        #
        # override if you want
        #
        def specific_authorizations(current_user, options={})
          current_store = options[:current_store]
          if current_store
            roles = current_user.cached_roles.reject{|role| (role.resource_type != 'Main::StoresManagement::Models::Store') && (role.resource_id != current_store.id) }
            roles.each do |role|
              role.cached_accesses.each do |access|
                can access.keys
              end 
            end
          end
        end

      end
    end
  end
end