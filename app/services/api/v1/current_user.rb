#
# current user operations
#

module Api
  module V1
      class CurrentUser < Api::V1::Base

        use_resource!
        self.presenter_name= "CurrentUser"

        #
        # change upload attributes to uploaded class
        #
        before do 
          if params[:avatar_attributes].present? && params[:avatar_attributes][:attachment].present?
            avatar_params = params[:avatar_attributes][:attachment]
            if avatar_params.respond_to?(:keys) && (avatar_params.keys.uniq.sort == ["filename", "type", "name", "tempfile", "head"].sort)
              params[:avatar_attributes][:attachment]= ActionDispatch::Http::UploadedFile.new(avatar_params)
            end
          end
        end

        helpers do 
          
          def current_user_params
            posts[:current_user]||= posts
            posts.require(:current_user).permit(:username, :email, :password, :password_confirmation, :avatar_attributes => [:id, :_destroy, :attachment, :remote_attachment_url])
          end

          def context_resource
            current_resource_owner
          end

        end

        resources "current_user" do 

          desc "[GET] show current user"
          get do 
            presenter context_resource
          end

          desc "[PUT] update current current user"
          put do
            if context_resource.update_without_password current_user_params
              presenter context_resource 
            else
              standard_validation_error(details: context_resource.errors)
            end
          end

        end

      end
  end
end