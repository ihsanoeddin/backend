class ResourceOwnerCredential

  attr_accessor :user, :request

  def initialize(request)
    self.request= request
  end

  def domain
    if request.subdomains.present?
      request.subdomains.first.to_s
    else
      request.domain.to_s.split('.').first
    end
  end

  def authenticate!
    authenticated= safe_authentication
    if authenticated
      Apartment::Tenant.switch(domain.underscore)
      authenticated
    end
  end

  def safe_authentication
    Apartment::Tenant.switch(domain.underscore) do
      user = ::Main::UsersManagement::Models::User.find_for_database_authentication(:login => request.params[:username])
      if user && user.valid_for_authentication? { user.valid_password?(request.params[:password]) }
        return user.confirmed?? user : nil
      end
    end
  end

  def parse_tenant_name
    request_subdomain = subdomain(request.host)
  end

  def subdomain
    subdomains(request.host).first
  end

  def subdomains(host)
    #return [] unless named_host?(host)
    host.split('.')[0..-(1 + 1)]
  end

  class << self

    def authenticate!(request)
      new(request).authenticate!
    end

  end

end