Rolify.configure do |config|
  # By default ORM adapter is ActiveRecord. uncomment to use mongoid
  # config.use_mongoid

  # Dynamic shortcuts for User class (user.is_admin? like methods). Default is: false
  # config.use_dynamic_shortcuts
  #config.role_cname= "::Main::UsersManagement::Models::Role"
  #config.role_table_name= "main_users_management_roles"
end