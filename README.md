# README

Qashier backend with Ruby on Rails. Mutitenant app for POS frontend app.

Main :
* Ruby 2.4.1
* Ruby on Rails version > 5 

System dependencies :
* Nginx web server (deployment)
* Memcached server
* Redis server
* Mysql database Server

Configuration:
* Change config/database.yml appropriately
* Database setup with `rake db:create`, `rake db:migrate`, & `rake db:seed`

Sidekiq :
* run `sidekiq -C config/sidekiq.yml -d` on development 

Deployment instructions :
* Using capistrano3 `cap {environtment} deploy`

API Documentations
* <a target="_blank" href="http://docs.qashierv1.apiary.io">here</a>

